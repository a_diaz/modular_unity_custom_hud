using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

namespace CustomHUD {



	public class HUDEditorWindow : EditorWindow {

		# region structs

		/*
			We will be using some simple structs to gather in only one place
			all the creation parameters specific to each configurable part

		 */

		struct RectTransformParams {

			public int 		distance;
			public int 		position;
			public float 	tilt;
		}

		struct SubtitleParams {

			public HUDSubtitleTypes type;

			public int margins;
			public int height;

			public Font 	font;
			public string 	text;
			public Color 	textColor;
			public bool 	shadow;
			public Color 	shadowColor;
			public bool 	outline;
			public Color 	outlineColor;
		}

		struct WarningPanelParams {

			public HUDWarningLayouts 	layout;
			
			public Sprite 				backgroundSprite;
			public Sprite 				warningIcon;
			public Vector2 				backgroundSize;
			public Vector2 				iconSize;

			public Font 				font;
			public string 				text;
			public Color 				textColor;
			public bool 				shadow;
			public Color 				shadowColor;
			public bool 				outline;
			public Color 				outlineColor;
		}

		struct HintPanelParams {
			
			public HUDHintLayouts 	layout;
			public Font 			font;
			public Sprite 			sprite;
			public Vector2 			spriteSize;
			public string 			text;
			public HUDHintTypes 	type;
			public Color 			textColor;
			public bool 			shadow;
			public Color 			shadowColor;
			public bool 			outline;
			public Color 			outlineColor;
		}

		struct ProgressBarParams {

			public HUDProgressBarLayouts 	layout;
			public HUDProgressBarAlignment 	alignment;
			public HUDProgressBarTypes 		type;
			public float 	value;
			public float 	maxValue;
			public float 	minValue;

			public Sprite background;
			public Sprite fill;

			public float barHeight;
			public float barLength;

			public bool displayNumber;
			public bool useIntegerDisplay;

			public string floatFormatting;

			public Sprite textHolder;
			public Vector2 textHolderSize;

			public Font 	font;
			public Color 	textColor;
			public bool 	shadow;
			public Color 	shadowColor;
			public bool 	outline;
			public Color 	outlineColor;
		}

		struct ShowAndHideParams {

			public bool 	startShown;
			public bool 	animate;
			public bool 	fadeIn;
			public bool 	fadeOut;
			public bool 	slideIn;
			public bool 	slideOut;
			public float 	enterTime;
			public float 	exitTime;
		}

		# endregion

		private static HUDController _currentHUDController;

		# region editing attributes

		private HUDAnchorTypes _currentAnchorToSet;
		private HUDAnchorTypes _subtitleAnchorToSet;
		
		private HUDElementTypes _currentCreatingElement = HUDElementTypes.WarningPanel;

		private System.Type _associatedTypeToElement {

			get { 
			
				switch (_currentCreatingElement) {

				case HUDElementTypes.Subtitle : 	return typeof (HUDSubtitle);
				case HUDElementTypes.WarningPanel : return typeof (HUDWarningPanel);
				case HUDElementTypes.HintPanel : 	return typeof (HUDHintPanel);
				case HUDElementTypes.ProgressBar : 	return typeof (HUDProgressBar);
				default : return typeof (HUDElement);
				}
			}
		}

		private string _currentGameObjectName = "Warning Panel";

		private Texture2D _infoDistanceTexture = null;

		private Texture2D [] _infoHintLayoutsTextures = null;

		private Texture2D[] _infoProgressBarLayoutsTextures = null;
		private Texture2D[] _infoProgressBarAlignmentTextures = null;


		# region struct editing attributes

		private RectTransformParams _currentRectParams;

		private SubtitleParams _currentSubtitleParams;

		private WarningPanelParams _currentWarningPanelParams;

		private HintPanelParams _currentHintPanelParams;
		
		private ProgressBarParams _currentProgressBarParams;

		private ShowAndHideParams _currentShowAndHideParams;

		# endregion

		# endregion

		# region static methods
		
		/// <summary>
		/// Shows the Custom HUD Editor window. If there is not a HUDController in the scene, prompts the user to create one.
		/// </summary>
		[MenuItem ("Custom HUD/Edit HUD")]
		public static void ShowHUDEditor () {
			

			if (HUDController.hasSingletonInstanceSet) {
			
				HUDEditorWindow._currentHUDController = HUDController.GetInstance ();
				Debug.Log("Set instance!");

				HUDEditorWindow window = (HUDEditorWindow) EditorWindow.GetWindow(typeof(HUDEditorWindow), true);
			
				window.name = "Custom HUD Editor";
				window.titleContent = new GUIContent ("Custom HUD Editor");
				window.minSize = new Vector2 (HUDConst.DEFAULT_WINDOW_SIZE_X, HUDConst.DEFAULT_WINDOW_SIZE_Y); 

			}
			else {

				HUDController[] hudsInScene = GameObject.FindObjectsOfType<HUDController> ();
				
				if (hudsInScene.Length > 0) { // only if found one
					
					hudsInScene[0].EditorInitialize ();

					hudsInScene[0].EditorCacheAnchors ();
					hudsInScene[0].InitializeAnchorDictionaryWithCachedAnchors ();

					ShowHUDEditor (); // call again
					return;
				}
				else {

					// use a popup or so
					ShowHUDControllerDoesNotExistPopup ();
				}
			}
		}


		/// <summary>
		/// Shows the HUD controller does not exist popup.
		/// </summary>
		public static void ShowHUDControllerDoesNotExistPopup () {

			EditorWindow popup = EditorWindow.GetWindow(typeof(HUDDoesNotExistPopup), true);

			popup.name = "HUD not found";
			popup.titleContent = new GUIContent ("HUD not found");
			popup.minSize = new Vector2 (HUDConst.EDITOR_POPUP_SIZE_X, HUDConst.EDITOR_POPUP_SIZE_Y);
			popup.maxSize = new Vector2 (HUDConst.EDITOR_POPUP_SIZE_X, HUDConst.EDITOR_POPUP_SIZE_Y);
		}

		/// <summary>
		/// Opens the HUD Creator window in order to create a new HUDController before editing it.
		/// </summary>
		public static void CreateNewHUDBeforeEdit () {
		
			HUDCreatorWindow.ShowHUDCreator ();
		}

		# endregion

		# region unity default

		void OnEnable () {

			InitWindow ();
		}

		# endregion

		# region methods

		/// <summary>
		/// Sets some window attributes such as info images, and also default values obtained from the controller.
		/// </summary>
		private void InitWindow () {
		
			_infoDistanceTexture 				= HUDResources.distanceInfoImage;
			_infoHintLayoutsTextures 			= HUDResources.hintPanelLayoutInfoImages;
			_infoProgressBarAlignmentTextures 	= HUDResources.progressBarAlignmentInfoImages;
			_infoProgressBarLayoutsTextures 	= HUDResources.progressBarLayoutInfoImages;

			_currentRectParams = new RectTransformParams {
				
				distance = 10,
				position = 0,
				tilt = 0.0f
			};

			_currentSubtitleParams = new SubtitleParams {

				type = HUDSubtitleTypes.PrimarySubtitle,
				font = _currentHUDController.defaultFont,
				text = "Your speech goes here",

				margins 	= 100,
				height  	= 120,

				textColor = Color.white,
				outline = true,
				outlineColor = Color.black,
				shadow = false,
				shadowColor = Color.black

			};

			_currentWarningPanelParams = new WarningPanelParams {

				layout 		= HUDWarningLayouts.IconLeftTextRight,
				font		= _currentHUDController.defaultFont,

				backgroundSprite	= null,
				warningIcon		= null,
				backgroundSize	 	= new Vector2 (900.0f, 600.0f),
				iconSize			= new Vector2 (),

				text 		= "Warning!",
				textColor 	= Color.red,

				shadow 			= false,
				shadowColor 	= Color.black,
				outline 		= false,
				outlineColor 	= Color.black
			};

			_currentHintPanelParams = new HintPanelParams {
				
				layout 		= HUDHintLayouts.IconLeftTextRight,
				font 		= _currentHUDController.defaultFont,
				sprite 		= HUDResources.defaultHintSprite,
				spriteSize 	= new Vector2 (HUDResources.defaultHintSprite.texture.width, HUDResources.defaultHintSprite.texture.height),
				text 		= "Jump!",
				type 		= HUDHintTypes.ActionHint,
				
				textColor		 = Color.white,
				shadow 			= false,
				shadowColor 	= Color.black,
				outline			= false,
				outlineColor	= Color.black
			};

			_currentProgressBarParams = new ProgressBarParams {

				layout  = HUDProgressBarLayouts.NumberLeftBarRight,
				alignment = HUDProgressBarAlignment.Middle,
				type = HUDProgressBarTypes.HealthBar,
				value = 100.0f,
				maxValue = 100.0f,
				minValue = 0.0f,

				background = null,
				fill = null,

				barHeight = 60.0f,
				barLength = 500.0f,

				displayNumber = true,
				useIntegerDisplay = false,

				floatFormatting = "0.00",

				textHolder = null,
				textHolderSize = new Vector2 (100.0f, 100.0f),

				font = _currentHUDController.defaultFont,
				textColor = Color.white,
				shadow = false,
				shadowColor = Color.black,
				outline = false,
				outlineColor = Color.black
			};

			_currentShowAndHideParams = new ShowAndHideParams {
				
				startShown = true,
				animate = false,
				slideIn = false,
				slideOut = false,
				fadeIn = false,
				fadeOut = false,
				enterTime = 0.5f,
				exitTime = 0.5f
			};
		}

		# endregion

		void OnGUI () {
		
			GUILayout.Label ("Element of the HUD to be added: ", EditorStyles.miniLabel); 

			/*if (GUILayout.Button ("Show")) {
				show = !show;
			}*/

			// select the element to create
			PaintElementSelectionButtons ();

			EditorGUILayout.Separator ();

			GUILayout.Label ("Basic configuration", EditorStyles.boldLabel);
			PaintFirstCommonPart ();

			EditorGUILayout.Separator ();

			GUILayout.Label (_currentCreatingElement.ToString() + " configuration", EditorStyles.boldLabel);

			SwitchAndPaintSpecificPart ();

			EditorGUILayout.Separator ();



			PaintLastCommonPart ();


			EditorGUILayout.Space ();
			/*
			if (EditorGUILayout.BeginFadeGroup ((show) ? 1.0f : 0.0f) ){
			myString = EditorGUILayout.TextField ("Text Field", myString);
			
			groupEnabled = EditorGUILayout.BeginToggleGroup ("Optional Settings", groupEnabled);
			myBool = EditorGUILayout.Toggle ("Toggle", myBool);
			myFloat = EditorGUILayout.Slider ("Slider", myFloat, -3, 3);
			EditorGUILayout.EndToggleGroup ();

			EditorGUILayout.EndFadeGroup ();
			}
			*/
		}

		
		
		private void PaintElementSelectionButtons () {
			
			EditorGUILayout.BeginHorizontal ();

			GUI.enabled = _currentCreatingElement != HUDElementTypes.Subtitle;
			if (GUILayout.Button (new GUIContent("Subtitle", "Prompt text messages to the player, commonly following an audible speech."))) {
				
				_currentCreatingElement = HUDElementTypes.Subtitle;
				_currentGameObjectName = "Subtitle";
			}
			
			GUI.enabled = _currentCreatingElement != HUDElementTypes.WarningPanel;
			if (GUILayout.Button (new GUIContent("Warning Panel", "Prompt the player with an important message, like in an alert or warning. May have some visual effects."))) {
				
				_currentCreatingElement = HUDElementTypes.WarningPanel;
				_currentGameObjectName = "Warning Panel";
			}
			
			GUI.enabled = _currentCreatingElement != HUDElementTypes.HintPanel;
			if (GUILayout.Button (new GUIContent("Hint Panel", "Prompt the player with a suggestion or an advise, like pressing a button or jumping; or maybe to remind him/her something."))) {
				
				_currentCreatingElement = HUDElementTypes.HintPanel;
				_currentGameObjectName = "Hint Panel";
			}
			
			GUI.enabled = _currentCreatingElement != HUDElementTypes.ProgressBar;
			if (GUILayout.Button (new GUIContent("Progress Bar", "A progress/loading bar, normally used for displaying health, energy, stamina, mission completion, etc."))) {
				
				_currentCreatingElement = HUDElementTypes.ProgressBar;
				_currentGameObjectName = "Progress Bar";
			}
			
			GUI.enabled = true;
			EditorGUILayout.EndHorizontal ();
		}

		# region window parts

		/// <summary>
		/// Inserts a space of the specified pixels.
		/// </summary>
		/// <param name="pixels">Amount of pixels.</param>
		private void Space (float pixels) {
		
			GUILayout.Space (pixels);
		}

		/// <summary>
		/// Inserts a 10.0f pixel space.
		/// </summary>
		private void Space10 () {
		
			GUILayout.Space (10.0f);
		}

		/// <summary>
		/// Inserts a 110.0f pixel space.
		/// </summary>
		private void Space110 () {

			GUILayout.Space (110.0f);
		}

		private void PaintFirstCommonPart () {
		
			PaintBasicNameAndAnchor ();
			Space10 ();
			PaintSliderSection ();
		}

		private void PaintBasicNameAndAnchor () {
		
			EditorGUILayout.BeginHorizontal ();
			
			PaintNameSelectionInput ();
			
			Space10 ();
			
			PaintSelectAnchorDropDownMenu ();
			EditorGUILayout.EndHorizontal ();
		}

		private void PaintNameSelectionInput () {
		

			GUILayout.Label ("GameObject name:", EditorStyles.label, GUILayout.MinWidth (144), GUILayout.MaxWidth (144));
			_currentGameObjectName = GUILayout.TextField (_currentGameObjectName, 24, GUILayout.MinWidth (150),GUILayout.MaxWidth (150));

		}

		private void PaintSelectAnchorDropDownMenu () {
			
			GUILayout.Label ("Anchor to:", EditorStyles.label, GUILayout.MinWidth (80), GUILayout.MaxWidth (80));

			if (_currentCreatingElement != HUDElementTypes.Subtitle) {
			
				_currentAnchorToSet = (HUDAnchorTypes) EditorGUILayout.EnumPopup ( _currentAnchorToSet, GUILayout.MinWidth (80));
			}
			else {

				int choice = (_subtitleAnchorToSet == HUDAnchorTypes.Top) ? 0 : 1;

				choice = EditorGUILayout.Popup (choice, new string[] {"Bottom", "Top"},  GUILayout.MinWidth (80));

				if (choice == 0) { _subtitleAnchorToSet = _currentAnchorToSet = HUDAnchorTypes.Bottom; }
				else { _subtitleAnchorToSet = _currentAnchorToSet = HUDAnchorTypes.Top; }
			}
		}

		private void SwitchAndPaintSpecificPart () {
		
			switch (_currentCreatingElement) {
			
			case HUDElementTypes.Subtitle:
				PaintSubtitlePart ();
				break;
			case HUDElementTypes.WarningPanel:
				PaintWarningPanelPart ();
				break;
			case HUDElementTypes.HintPanel:
				PaintHintPanelPart ();
				break;
			case HUDElementTypes.ProgressBar:
				PaintProgressBarPart ();
				break;
			}
		}

		private void PaintLastCommonPart () {

			PaintShowAndHideSection ();

			// to the end of the vertical layout
			GUILayout.FlexibleSpace ();

			// the last thing is the button, at the bottom
			PaintCreateButtonSection ();
			
		}

		private void PaintSliderSection () {
		
			int maxRefResolution;
			int maxPositionToRefResolution;
			
			if (_currentAnchorToSet == HUDAnchorTypes.Left || _currentAnchorToSet == HUDAnchorTypes.Right || _currentAnchorToSet == HUDAnchorTypes.Center) {
				
				maxRefResolution = (int) _currentHUDController.referenceResolution.x;
				maxPositionToRefResolution = (int) _currentHUDController.referenceResolution.y;
			}
			else if (_currentAnchorToSet == HUDAnchorTypes.Top || _currentAnchorToSet == HUDAnchorTypes.Bottom){
				
				maxRefResolution = (int) _currentHUDController.referenceResolution.y;
				maxPositionToRefResolution = (int) _currentHUDController.referenceResolution.x;
			}
			else { 
				// something by default
				
				maxRefResolution = 2000;
				maxPositionToRefResolution = 1000;
			}

			maxRefResolution = maxRefResolution / 2;
			maxPositionToRefResolution = maxPositionToRefResolution / 2;

			EditorGUILayout.BeginHorizontal ();

			// one half three sliders // other half, indicative image

			EditorGUILayout.BeginVertical ();

			GUILayout.Label ("RectTransform basic parameters (relative to anchor):", EditorStyles.boldLabel,
			                 GUILayout.MinWidth (144));

			bool anchoringAtCenter = _currentAnchorToSet == HUDAnchorTypes.Center;

			if (anchoringAtCenter) {
			
				_currentRectParams.distance = EditorGUILayout.IntSlider (new GUIContent ("Horizontal", 
				                                                                      "The number of pixels that this HUDElement will be offseted horizontally from the center of the screen."),
				                                                      _currentRectParams.distance, 
				                                                      - maxRefResolution,
				                                                      maxRefResolution
				                                                      );

				_currentRectParams.position = EditorGUILayout.IntSlider (	new GUIContent ("Vertical", 
				                                                                       "The number of pixels that this HUDElement will be offseted vertically from the center of the screen."),
				                                                      _currentRectParams.position, 
				                                                      - maxPositionToRefResolution,
				                                                      maxPositionToRefResolution
				                                                      );

			}
			else {

				// for every other anchor // we do the normal thing
				_currentRectParams.distance = EditorGUILayout.IntSlider (new GUIContent ("Distance", 
				                                                                      "The number of pixels that this HUDElement will be offseted from the border of the screen towards the center."),
				                                                      _currentRectParams.distance, 
				                                                      0,
				                                                      maxRefResolution
				                                                      );


				if (_currentCreatingElement != HUDElementTypes.Subtitle) {

					// for every other element we have the offseted postion
					_currentRectParams.position = EditorGUILayout.IntSlider (	new GUIContent ("Position", 
					                                                                       "The number of pixels that this HUDElement will be displaced along the border of the screen."),
					                                                      _currentRectParams.position, 
					                                                      - maxPositionToRefResolution,
					                                                      maxPositionToRefResolution
					                                                      );
				}
				else {

					//for the subtitle, we have the margins instead
					_currentSubtitleParams.margins = EditorGUILayout.IntSlider (	new GUIContent ("Margins", 
					                                                                               "The number of pixels from the sides that the subtitle text will have as a margin."),
					                                                          	  _currentSubtitleParams.margins, 
					                                                              0,
					                                                              maxPositionToRefResolution
					                                                              );
				}
			}


			if (_currentCreatingElement != HUDElementTypes.Subtitle) {

				_currentRectParams.tilt = EditorGUILayout.Slider (	new GUIContent ("Tilt", 
				                                                                   "The angle that this HUDElement will be tilted, with center in the border of the screen."),
				                                                  _currentRectParams.tilt, 
				                                                  - 90.0f,
				                                                  90.0f
				                                                  );
			}
			else {

				_currentSubtitleParams.height = EditorGUILayout.IntSlider (	new GUIContent ("Height", 
				                                                                            "The number of pixels height that the new subtitle will be."),
				                                                           _currentSubtitleParams.height, 
				                                                           0,
				                                                           maxRefResolution
				                                                           );
			}

			GUILayout.Box (/*"Basic placement for the element with respect to the selected anchor. "
			               + */"You can make further adjustments in the RectTransform of the HUDElement created and of its children.", 
			               GUILayout.ExpandWidth(true),
			               GUILayout.ExpandHeight(true),
			               GUILayout.MinHeight (10));

			EditorGUILayout.EndVertical ();

			GUILayout.Label (_infoDistanceTexture);

			EditorGUILayout.EndHorizontal ();
			
		}

		private void PaintShowAndHideSection () {
		
			GUILayout.Label ("Display settings", EditorStyles.boldLabel);

			GUILayout.BeginHorizontal (); // global

			if (_associatedTypeToElement.GetInterface ("IHUDShowable") == typeof (IHUDShowable)) {

				GUILayout.BeginVertical ();
				_currentShowAndHideParams.startShown = EditorGUILayout.ToggleLeft (new GUIContent (" Start shown"), _currentShowAndHideParams.startShown, GUILayout.MinWidth (100), GUILayout.MaxWidth (100));
				_currentShowAndHideParams.animate = EditorGUILayout.ToggleLeft (new GUIContent (" Animate"), _currentShowAndHideParams.animate, GUILayout.MinWidth (100), GUILayout.MaxWidth (100));
				GUILayout.EndVertical ();

				Space10 ();
			}

			bool showEnterAndExitTime = false;

			if (_associatedTypeToElement.GetInterface ("IHUDFadeInOutAnimated") == typeof(IHUDFadeInOutAnimated)) {
			
				GUI.enabled = _currentShowAndHideParams.animate;
				GUILayout.BeginVertical ();
				_currentShowAndHideParams.fadeIn = EditorGUILayout.ToggleLeft (new GUIContent (" Fade in"), _currentShowAndHideParams.fadeIn, GUILayout.MinWidth (80), GUILayout.MaxWidth (80));
				_currentShowAndHideParams.fadeOut = EditorGUILayout.ToggleLeft (new GUIContent (" Fade out"), _currentShowAndHideParams.fadeOut, GUILayout.MinWidth (80), GUILayout.MaxWidth (80));
				GUILayout.EndVertical ();

				showEnterAndExitTime = true;
				Space10 ();
			}

			if (_associatedTypeToElement.GetInterface ("IHUDSlideInOutAnimated") == typeof(IHUDSlideInOutAnimated)) {
			
				GUILayout.BeginVertical ();
				_currentShowAndHideParams.slideIn = EditorGUILayout.ToggleLeft (new GUIContent (" Slide in"), _currentShowAndHideParams.slideIn, GUILayout.MinWidth (80), GUILayout.MaxWidth (80));
				_currentShowAndHideParams.slideOut = EditorGUILayout.ToggleLeft (new GUIContent (" Slide out"), _currentShowAndHideParams.slideOut, GUILayout.MinWidth (80), GUILayout.MaxWidth (80));
				GUILayout.EndVertical ();

				showEnterAndExitTime = true;
				Space10 ();
			}

			if (showEnterAndExitTime) {

				GUILayout.BeginVertical ();
				_currentShowAndHideParams.enterTime = EditorGUILayout.FloatField (new GUIContent ("Entrance duration"), _currentShowAndHideParams.enterTime, GUILayout.MinWidth (200), GUILayout.MaxWidth (200));
				_currentShowAndHideParams.exitTime = EditorGUILayout.FloatField (new GUIContent ("Exit duration"), _currentShowAndHideParams.exitTime, GUILayout.MinWidth (200), GUILayout.MaxWidth (200));
				GUILayout.EndVertical ();
			}

			GUILayout.FlexibleSpace ();
			

			GUILayout.EndHorizontal (); // global
			
			GUI.enabled = true;
			
//			GUILayout.FlexibleSpace ();


			// clamp the values
			_currentShowAndHideParams.enterTime = Mathf.Clamp (_currentShowAndHideParams.enterTime, 0.05f, int.MaxValue);
			_currentShowAndHideParams.exitTime = Mathf.Clamp (_currentShowAndHideParams.exitTime, 0.05f, int.MaxValue);
			
//			EditorGUILayout.EndToggleGroup ();

			
			/// 
		}

		private void PaintCreateButtonSection () {
			
			EditorGUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			
			if (GUILayout.Button (new GUIContent ("Create " + _currentCreatingElement.ToString ()), GUILayout.MinHeight (30.0f), GUILayout.Width (300.0f))) {
				
				CreateHUDElement ();
				
			}
			GUILayout.FlexibleSpace ();
			EditorGUILayout.EndHorizontal ();
		}

		private void PaintSubtitlePart () {
		
			EditorGUILayout.BeginHorizontal ();
			
			GUILayout.Label ("Subscribe as:", EditorStyles.label, GUILayout.MinWidth (144), GUILayout.MaxWidth (144));
			_currentSubtitleParams.type = (HUDSubtitleTypes) EditorGUILayout.EnumPopup (_currentSubtitleParams.type, GUILayout.MinWidth (150), GUILayout.MaxWidth (150));
			
			EditorGUILayout.EndHorizontal ();
			
			Space10 ();
			
			EditorGUILayout.BeginHorizontal ();
			
			GUILayout.Label ("Initial text:", EditorStyles.label, GUILayout.MinWidth (144), GUILayout.MaxWidth (144));
			
			_currentSubtitleParams.text = GUILayout.TextField (_currentSubtitleParams.text, 35, GUILayout.MinWidth (150),GUILayout.MaxWidth (150));
			
			Space10 ();
			
			GUILayout.Label ("Font:", EditorStyles.label, GUILayout.MinWidth (80), GUILayout.MaxWidth (80));
			_currentSubtitleParams.font = (Font) EditorGUILayout.ObjectField (_currentSubtitleParams.font, typeof(Font), false);
			
			EditorGUILayout.EndHorizontal ();
			
			EditorGUILayout.BeginHorizontal ();
			
			GUILayout.Label ("Text color:", EditorStyles.label, GUILayout.MinWidth (144), GUILayout.MaxWidth (144));
			_currentSubtitleParams.textColor = EditorGUILayout.ColorField (_currentSubtitleParams.textColor, GUILayout.MinWidth (150),GUILayout.MaxWidth (150));
			
			Space10 ();
			// outline and shadow
			
			EditorGUILayout.BeginHorizontal ();
			
			_currentSubtitleParams.shadow = EditorGUILayout.ToggleLeft (new GUIContent (" Shadow"), _currentSubtitleParams.shadow, GUILayout.MinWidth (80), GUILayout.MaxWidth (80));
			
			GUI.enabled = _currentSubtitleParams.shadow;
			_currentSubtitleParams.shadowColor = EditorGUILayout.ColorField (_currentSubtitleParams.shadowColor, GUILayout.MinWidth (50));
			GUI.enabled = true;
			
			Space10 ();
			
			_currentSubtitleParams.outline = EditorGUILayout.ToggleLeft (new GUIContent (" Outline"), _currentSubtitleParams.outline, GUILayout.MinWidth (80), GUILayout.MaxWidth (80));
			
			GUI.enabled = _currentSubtitleParams.outline;
			_currentSubtitleParams.outlineColor = EditorGUILayout.ColorField (_currentSubtitleParams.outlineColor, GUILayout.MinWidth (50));
			GUI.enabled = true;
			
			EditorGUILayout.EndHorizontal ();
			
			EditorGUILayout.EndHorizontal ();

		}


		private void PaintWarningPanelPart () {

			EditorGUILayout.BeginHorizontal ();
			
			GUILayout.Label ("Initial text:", EditorStyles.label, GUILayout.MinWidth (144), GUILayout.MaxWidth (144));
			
			_currentWarningPanelParams.text = GUILayout.TextField (_currentWarningPanelParams.text, 35, GUILayout.MinWidth (150),GUILayout.MaxWidth (150));
			
			Space10 ();
			
			GUILayout.Label ("Font:", EditorStyles.label, GUILayout.MinWidth (80), GUILayout.MaxWidth (80));
			_currentWarningPanelParams.font = (Font) EditorGUILayout.ObjectField (_currentWarningPanelParams.font, typeof(Font), false);
			
			EditorGUILayout.EndHorizontal ();
			
			EditorGUILayout.BeginHorizontal ();
			
			GUILayout.Label ("Text color:", EditorStyles.label, GUILayout.MinWidth (144), GUILayout.MaxWidth (144));
			_currentWarningPanelParams.textColor = EditorGUILayout.ColorField (_currentWarningPanelParams.textColor, GUILayout.MinWidth (150),GUILayout.MaxWidth (150));
			
			Space10 ();
			// outline and shadow
			
			EditorGUILayout.BeginHorizontal ();
			
			_currentWarningPanelParams.shadow = EditorGUILayout.ToggleLeft (new GUIContent (" Shadow"), _currentWarningPanelParams.shadow, GUILayout.MinWidth (80), GUILayout.MaxWidth (80));
			
			GUI.enabled = _currentWarningPanelParams.shadow;
			_currentWarningPanelParams.shadowColor = EditorGUILayout.ColorField (_currentWarningPanelParams.shadowColor, GUILayout.MinWidth (50));
			GUI.enabled = true;
			
			Space10 ();
			
			_currentWarningPanelParams.outline = EditorGUILayout.ToggleLeft (new GUIContent (" Outline"), _currentWarningPanelParams.outline, GUILayout.MinWidth (80), GUILayout.MaxWidth (80));
			
			GUI.enabled = _currentWarningPanelParams.outline;
			_currentWarningPanelParams.outlineColor = EditorGUILayout.ColorField (_currentWarningPanelParams.outlineColor, GUILayout.MinWidth (50));
			GUI.enabled = true;
			
			EditorGUILayout.EndHorizontal ();
			
			EditorGUILayout.EndHorizontal ();

			// now for the sprite and the orientation
			Space10 ();
			
			EditorGUILayout.BeginHorizontal ();
			
			EditorGUILayout.BeginVertical ();
			
			GUILayout.Label ("Initial sprite:", EditorStyles.label,
			                 GUILayout.MinWidth (144), GUILayout.MaxWidth (144),
			                 GUILayout.ExpandWidth (false));
			
			Space10 ();
			
			_currentWarningPanelParams.iconSize = EditorGUILayout.Vector2Field ("",_currentWarningPanelParams.iconSize,GUILayout.MaxWidth (100));
			
			EditorGUILayout.EndVertical ();

			Sprite tempSprite = _currentWarningPanelParams.warningIcon;

			_currentWarningPanelParams.warningIcon = (Sprite) EditorGUILayout.ObjectField (_currentWarningPanelParams.warningIcon, typeof(Sprite), false,
			                                                                       GUILayout.MinHeight (50), GUILayout.MaxHeight (50),
			                                                                       GUILayout.MinWidth (50), GUILayout.MaxWidth (50));
			
			
			if (_currentWarningPanelParams.warningIcon != null && tempSprite != _currentWarningPanelParams.warningIcon) {
				
				// if the sprite changed, change the resolution also
				_currentWarningPanelParams.iconSize = new Vector2 (_currentWarningPanelParams.warningIcon.texture.width, _currentWarningPanelParams.warningIcon.texture.height);
			}

			GUILayout.FlexibleSpace ();
			
			EditorGUILayout.BeginVertical ();
			
			GUILayout.Label ("Background (sliced):", EditorStyles.label,
			                 GUILayout.MinWidth (144), GUILayout.MaxWidth (144),
			                 GUILayout.ExpandWidth (false));
			
			Space10 ();
			
			_currentWarningPanelParams.backgroundSize = EditorGUILayout.Vector2Field ("",_currentWarningPanelParams.backgroundSize,GUILayout.MaxWidth (100));
			
			EditorGUILayout.EndVertical ();

			tempSprite = _currentWarningPanelParams.backgroundSprite;
			
			_currentWarningPanelParams.backgroundSprite = (Sprite) EditorGUILayout.ObjectField (_currentWarningPanelParams.backgroundSprite, typeof(Sprite), false,
			                                                                                 GUILayout.MinHeight (50), GUILayout.MaxHeight (50),
			                                                                                 GUILayout.MinWidth (50), GUILayout.MaxWidth (50));


			GUILayout.FlexibleSpace ();

			GUILayout.Label ("Layout:", EditorStyles.label,
			                 GUILayout.MinWidth (60), GUILayout.MaxWidth (80), GUILayout.ExpandWidth (false),
			                 GUILayout.ExpandHeight (true), GUILayout.MaxHeight (50));
			
			
			for (int i = 0; i < 2; i++) {
				
				GUI.enabled = ((int) _currentWarningPanelParams.layout) != i;
				
				if (GUILayout.Button (_infoHintLayoutsTextures[i], 
				                      /*GUILayout.MaxWidth (50),*/GUILayout.MinWidth (50), GUILayout.ExpandWidth (true),
				                      GUILayout.MaxHeight (50),GUILayout.MinHeight (50))) {
					
					_currentWarningPanelParams.layout = (HUDWarningLayouts) i;
				}
			}

			GUI.enabled = true;

			EditorGUILayout.EndHorizontal ();


		}

		private void PaintHintPanelPart () {

			// this for the text and font
			EditorGUILayout.BeginHorizontal ();

			GUILayout.Label ("Subscribe as:", EditorStyles.label, GUILayout.MinWidth (144), GUILayout.MaxWidth (144));
			_currentHintPanelParams.type = (HUDHintTypes) EditorGUILayout.EnumPopup (_currentHintPanelParams.type, GUILayout.MinWidth (150), GUILayout.MaxWidth (150));

			EditorGUILayout.EndHorizontal ();

			Space10 ();

			EditorGUILayout.BeginHorizontal ();

			GUILayout.Label ("Initial text:", EditorStyles.label, GUILayout.MinWidth (144), GUILayout.MaxWidth (144));
			
			_currentHintPanelParams.text = GUILayout.TextField (_currentHintPanelParams.text, 35, GUILayout.MinWidth (150),GUILayout.MaxWidth (150));

			Space10 ();

			GUILayout.Label ("Font:", EditorStyles.label, GUILayout.MinWidth (80), GUILayout.MaxWidth (80));
			_currentHintPanelParams.font = (Font) EditorGUILayout.ObjectField (_currentHintPanelParams.font, typeof(Font), false);

			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.BeginHorizontal ();
									
			GUILayout.Label ("Text color:", EditorStyles.label, GUILayout.MinWidth (144), GUILayout.MaxWidth (144));
			_currentHintPanelParams.textColor = EditorGUILayout.ColorField (_currentHintPanelParams.textColor, GUILayout.MinWidth (150),GUILayout.MaxWidth (150));

			Space10 ();
			// outline and shadow

			EditorGUILayout.BeginHorizontal ();
			
			_currentHintPanelParams.shadow = EditorGUILayout.ToggleLeft (new GUIContent (" Shadow"), _currentHintPanelParams.shadow, GUILayout.MinWidth (80), GUILayout.MaxWidth (80));
			
			GUI.enabled = _currentHintPanelParams.shadow;
			_currentHintPanelParams.shadowColor = EditorGUILayout.ColorField (_currentHintPanelParams.shadowColor, GUILayout.MinWidth (50));
			GUI.enabled = true;
			
			Space10 ();
			
			_currentHintPanelParams.outline = EditorGUILayout.ToggleLeft (new GUIContent (" Outline"), _currentHintPanelParams.outline, GUILayout.MinWidth (80), GUILayout.MaxWidth (80));
			
			GUI.enabled = _currentHintPanelParams.outline;
			_currentHintPanelParams.outlineColor = EditorGUILayout.ColorField (_currentHintPanelParams.outlineColor, GUILayout.MinWidth (50));
			GUI.enabled = true;
			
			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.EndHorizontal ();


			
			// now for the sprite and the orientation
			Space10 ();
			
			EditorGUILayout.BeginHorizontal ();

			EditorGUILayout.BeginVertical ();
			
			GUILayout.Label ("Initial sprite:", EditorStyles.label,
			                 GUILayout.MinWidth (144), GUILayout.MaxWidth (144),
			                 GUILayout.ExpandWidth (false));

			Space10 ();
			
			_currentHintPanelParams.spriteSize = EditorGUILayout.Vector2Field ("",_currentHintPanelParams.spriteSize,GUILayout.MaxWidth (100));

			EditorGUILayout.EndVertical ();

			Sprite tempSprite = _currentHintPanelParams.sprite;

			//float ratioTo50 = _currentHintPanelParams.sprite.texture.height / 50.0f;

			
			_currentHintPanelParams.sprite = (Sprite) EditorGUILayout.ObjectField (_currentHintPanelParams.sprite, typeof(Sprite), false,
			                                                                       GUILayout.MinHeight (50), GUILayout.MaxHeight (50),
			                                                                       GUILayout.MinWidth (50), GUILayout.MaxWidth (50));

			
			if (_currentHintPanelParams.sprite != null && tempSprite != _currentHintPanelParams.sprite) {
			
				// if the sprite changed, change the resolution also
				_currentHintPanelParams.spriteSize = new Vector2 (_currentHintPanelParams.sprite.texture.width, _currentHintPanelParams.sprite.texture.height);
			}
			// and the button things

			Space110 ();

			GUILayout.Label ("Layout:", EditorStyles.label,
			                 GUILayout.MinWidth (60), GUILayout.MaxWidth (80), GUILayout.ExpandWidth (false),
			                 GUILayout.ExpandHeight (true), GUILayout.MaxHeight (50));

			
			for (int i = 0; i < _infoHintLayoutsTextures.Length; i++) {
			
				GUI.enabled = ((int) _currentHintPanelParams.layout) != i;

				if (GUILayout.Button (_infoHintLayoutsTextures[i], 
				                      GUILayout.MaxWidth (50), GUILayout.MinWidth (50), GUILayout.ExpandWidth (false),
				                      GUILayout.MaxHeight (50),GUILayout.MinHeight (50))) {
					
					_currentHintPanelParams.layout = (HUDHintLayouts) i;
				}
			}

			GUI.enabled = true;
			GUILayout.FlexibleSpace ();
						
			EditorGUILayout.EndHorizontal ();
		}

		private void PaintProgressBarPart () {

			EditorGUILayout.BeginHorizontal ();
			
			GUILayout.Label ("Subscribe as:", EditorStyles.label, GUILayout.MinWidth (144), GUILayout.MaxWidth (144));
			_currentProgressBarParams.type = (HUDProgressBarTypes) EditorGUILayout.EnumPopup (_currentProgressBarParams.type, GUILayout.MinWidth (150), GUILayout.MaxWidth (150));

//			Space10 ();


//			GUILayout.FlexibleSpace ();

//			GUI.enabled = _currentProgressBarParams.displayNumber;
//			GUI.enabled = true;

			EditorGUILayout.EndHorizontal ();

			Space10 ();
			// basic settings like values

			EditorGUILayout.BeginHorizontal ();

			GUILayout.Label ("Minimum:", EditorStyles.label,
			                 GUILayout.MinWidth (144), GUILayout.MaxWidth (144),
			                 GUILayout.ExpandWidth (false));

			if (_currentProgressBarParams.useIntegerDisplay) {
			
				_currentProgressBarParams.minValue = (float) EditorGUILayout.IntField ("",(int) _currentProgressBarParams.minValue,GUILayout.MaxWidth (80));
			}
			else {

				_currentProgressBarParams.minValue = EditorGUILayout.FloatField ("",_currentProgressBarParams.minValue,GUILayout.MaxWidth (80));
			}

			Space (80.0f);
			_currentProgressBarParams.useIntegerDisplay = EditorGUILayout.ToggleLeft (" Integer steps", _currentProgressBarParams.useIntegerDisplay, GUILayout.MinWidth (100), GUILayout.MaxWidth (100));
			
			EditorGUILayout.EndHorizontal ();
			
//			Space (34.0f); Space10 ();
			EditorGUILayout.BeginHorizontal ();
			
			GUILayout.Label ("Maximum:", EditorStyles.label,
			                 GUILayout.MinWidth (144), GUILayout.MaxWidth (144),
			                 GUILayout.ExpandWidth (false));


			if (_currentProgressBarParams.useIntegerDisplay) {

				_currentProgressBarParams.maxValue = (float) EditorGUILayout.IntField ("",(int) _currentProgressBarParams.maxValue,GUILayout.MaxWidth (80));
			}
			else {

				_currentProgressBarParams.maxValue = EditorGUILayout.FloatField ("",_currentProgressBarParams.maxValue,GUILayout.MaxWidth (80));
			}

			_currentProgressBarParams.value = _currentProgressBarParams.maxValue;

			Space (80.0f);
			_currentProgressBarParams.useIntegerDisplay = !EditorGUILayout.ToggleLeft (" Float steps", !_currentProgressBarParams.useIntegerDisplay, GUILayout.MinWidth (100), GUILayout.MaxWidth (100));
			
			GUILayout.FlexibleSpace ();
			_currentProgressBarParams.displayNumber 	= EditorGUILayout.ToggleLeft ("Display value", _currentProgressBarParams.displayNumber, GUILayout.MinWidth (100), GUILayout.MaxWidth (100));

			EditorGUILayout.EndHorizontal ();

			Space10 ();


			// format, color, shadow, outline
			EditorGUILayout.BeginHorizontal ();
			
			GUI.enabled = _currentProgressBarParams.displayNumber && !_currentProgressBarParams.useIntegerDisplay;
			
			GUILayout.Label ("Float formatting:", EditorStyles.label, GUILayout.MinWidth (144), GUILayout.MaxWidth (144));
			
			_currentProgressBarParams.floatFormatting = GUILayout.TextField (_currentProgressBarParams.floatFormatting, 35, GUILayout.MinWidth (150),GUILayout.MaxWidth (150));
			
			GUI.enabled = _currentProgressBarParams.displayNumber;
			
			Space10 ();
			
			GUILayout.Label ("Display font:", EditorStyles.label, GUILayout.MinWidth (80), GUILayout.MaxWidth (80));
			_currentProgressBarParams.font = (Font) EditorGUILayout.ObjectField (_currentProgressBarParams.font, typeof(Font), false);
			
			EditorGUILayout.EndHorizontal ();
			
			EditorGUILayout.BeginHorizontal ();
			
			GUILayout.Label ("Text color:", EditorStyles.label, GUILayout.MinWidth (144), GUILayout.MaxWidth (144));
			_currentProgressBarParams.textColor = EditorGUILayout.ColorField (_currentProgressBarParams.textColor, GUILayout.MinWidth (150),GUILayout.MaxWidth (150));
			
			Space10 ();
			// outline and shadow
			
			EditorGUILayout.BeginHorizontal ();
			
			GUI.enabled = _currentProgressBarParams.displayNumber;
			_currentProgressBarParams.shadow = EditorGUILayout.ToggleLeft (new GUIContent (" Shadow"), _currentProgressBarParams.shadow, GUILayout.MinWidth (80), GUILayout.MaxWidth (80));
			
			GUI.enabled = _currentProgressBarParams.displayNumber && _currentProgressBarParams.shadow;
			_currentProgressBarParams.shadowColor = EditorGUILayout.ColorField (_currentProgressBarParams.shadowColor, GUILayout.MinWidth (50));
			GUI.enabled = true;
			
			Space10 ();
			
			GUI.enabled = _currentProgressBarParams.displayNumber;
			_currentProgressBarParams.outline = EditorGUILayout.ToggleLeft (new GUIContent (" Outline"), _currentProgressBarParams.outline, GUILayout.MinWidth (80), GUILayout.MaxWidth (80));
			
			GUI.enabled = _currentProgressBarParams.displayNumber && _currentProgressBarParams.outline;
			_currentProgressBarParams.outlineColor = EditorGUILayout.ColorField (_currentProgressBarParams.outlineColor, GUILayout.MinWidth (50));
			GUI.enabled = true;
			
			EditorGUILayout.EndHorizontal ();
			EditorGUILayout.EndHorizontal ();
			
			GUI.enabled = true;


			Space10 ();
			// sprites

			EditorGUILayout.BeginHorizontal ();
			
			EditorGUILayout.BeginVertical ();
			
			GUILayout.Label ("Bar fill (sliced):", EditorStyles.label,
			                 GUILayout.MinWidth (144), GUILayout.MaxWidth (144),
			                 GUILayout.ExpandWidth (false));
			
			Space10 ();

			EditorGUILayout.BeginHorizontal ();
			
			GUILayout.Label ("Height:", EditorStyles.label,
			                 GUILayout.MinWidth (60), GUILayout.MaxWidth (60),
			                 GUILayout.ExpandWidth (false));

			_currentProgressBarParams.barHeight = EditorGUILayout.FloatField ("",_currentProgressBarParams.barHeight,GUILayout.MaxWidth (50));
			EditorGUILayout.EndHorizontal ();
			
			EditorGUILayout.EndVertical ();
			
			Sprite tempSprite = _currentProgressBarParams.fill;
			
			_currentProgressBarParams.fill = (Sprite) EditorGUILayout.ObjectField (_currentProgressBarParams.fill, typeof(Sprite), false,
			                                                                                 GUILayout.MinHeight (50), GUILayout.MaxHeight (50),
			                                                                                 GUILayout.MinWidth (50), GUILayout.MaxWidth (50));
			
			
			if (_currentProgressBarParams.fill != null && tempSprite != _currentProgressBarParams.fill) {
				
				// if the sprite changed, change the resolution also
				_currentProgressBarParams.barHeight = _currentProgressBarParams.fill.texture.height;
			}
			
			GUILayout.FlexibleSpace ();
			
			EditorGUILayout.BeginVertical ();
			
			GUILayout.Label ("Bar background (sliced):", EditorStyles.label,
			                 GUILayout.MinWidth (144), GUILayout.MaxWidth (144),
			                 GUILayout.ExpandWidth (false));
			
			Space10 ();

			EditorGUILayout.BeginHorizontal ();

			GUILayout.Label ("Lenght:", EditorStyles.label,
			                 GUILayout.MinWidth (60), GUILayout.MaxWidth (60),
			                 GUILayout.ExpandWidth (false));

			_currentProgressBarParams.barLength = EditorGUILayout.FloatField ("",_currentProgressBarParams.barLength,GUILayout.MaxWidth (50));

			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.EndVertical ();

			_currentProgressBarParams.background = (Sprite) EditorGUILayout.ObjectField (_currentProgressBarParams.background, typeof(Sprite), false,
			                                                                                    GUILayout.MinHeight (50), GUILayout.MaxHeight (50),
			                                                                                    GUILayout.MinWidth (50), GUILayout.MaxWidth (50));
			
			
			GUILayout.FlexibleSpace ();

			EditorGUILayout.BeginVertical ();
			
			GUILayout.Label ("Number holder img:", EditorStyles.label,
			                 GUILayout.MinWidth (144), GUILayout.MaxWidth (144),
			                 GUILayout.ExpandWidth (false));
			
			Space10 ();

			GUI.enabled = _currentProgressBarParams.displayNumber;
						
			_currentProgressBarParams.textHolderSize = EditorGUILayout.Vector2Field ("",_currentProgressBarParams.textHolderSize,GUILayout.MaxWidth (100));
			
			EditorGUILayout.EndVertical ();
			tempSprite = _currentProgressBarParams.textHolder;
			
			_currentProgressBarParams.textHolder = (Sprite) EditorGUILayout.ObjectField (_currentProgressBarParams.textHolder, typeof(Sprite), false,
			                                                                                    GUILayout.MinHeight (50), GUILayout.MaxHeight (50),
			                                                                                    GUILayout.MinWidth (50), GUILayout.MaxWidth (50));
			
			if (_currentProgressBarParams.textHolder != null && tempSprite != _currentProgressBarParams.textHolder) {
				
				// if the sprite changed, change the resolution also
				_currentProgressBarParams.textHolderSize = new Vector2(_currentProgressBarParams.textHolder.texture.width, _currentProgressBarParams.textHolder.texture.height);
			}
			GUI.enabled = true;

			EditorGUILayout.EndHorizontal ();

			Space10 ();

			EditorGUILayout.BeginHorizontal ();
			
			GUILayout.Label ("Layout:", EditorStyles.label,
			                 GUILayout.MinWidth (144), GUILayout.MaxWidth (144),
			                 GUILayout.ExpandWidth (false));


			for (int i = 0; i < _infoProgressBarLayoutsTextures.Length; i++) {
				
				GUI.enabled = ((int) _currentProgressBarParams.layout) != i;
				
				if (GUILayout.Button (_infoProgressBarLayoutsTextures[i], 
				                      GUILayout.MaxWidth (64),GUILayout.MinWidth (64), GUILayout.ExpandWidth (false),
				                      GUILayout.MaxHeight (50),GUILayout.MinHeight (50))) {
					
					_currentProgressBarParams.layout = (HUDProgressBarLayouts) i;
				}
			}

			GUI.enabled = true;

			Space (30.0f);
			GUILayout.Label ("Alignment:", EditorStyles.label,
			                 GUILayout.MinWidth (80), GUILayout.MaxWidth (80),
			                 GUILayout.ExpandWidth (false));
			
			
			for (int i = 0; i < _infoProgressBarAlignmentTextures.Length; i++) {
				
				GUI.enabled = ((int) _currentProgressBarParams.alignment) != i;
				
				if (GUILayout.Button (_infoProgressBarAlignmentTextures[i], 
				                      GUILayout.MaxWidth (64),GUILayout.MinWidth (64), GUILayout.ExpandWidth (false),
				                      GUILayout.MaxHeight (50),GUILayout.MinHeight (50))) {
					
					_currentProgressBarParams.alignment = (HUDProgressBarAlignment) i;
				}
			}
			
			GUI.enabled = true;

			EditorGUILayout.EndHorizontal ();
			
			
		}

		# endregion

		# region methods
	
		/// <summary>
		/// Creates the HUDElement. Called when the Create button is clicked.
		/// </summary>
		private void CreateHUDElement () {
		
			GameObject obj = HUDElementCreatorUtility.InstantiateHUDElement (_currentCreatingElement);

			try {

				obj.GetComponent<Transform> ().SetParent (_currentHUDController.anchors[_currentAnchorToSet].GetComponent<Transform> ());	
			}
			catch (NullReferenceException ex) {
			
				Debug.Log ("Caught " + ex.GetType ().ToString () + " because the anchors of the HUD weren't cached in the Dictionary. Corrected it." );

				_currentHUDController.EditorCacheAnchors ();
				_currentHUDController.InitializeAnchorDictionaryWithCachedAnchors ();
			
				obj.GetComponent<Transform> ().SetParent (_currentHUDController.anchors[_currentAnchorToSet].GetComponent<Transform> ());
			}
			catch (KeyNotFoundException ex) {
				
				Debug.Log ("Caught " + ex.GetType ().ToString () + " because the anchors of the HUD weren't cached in the Dictionary. Corrected it." );

				_currentHUDController.InitializeAnchorDictionaryWithCachedAnchors ();
				
				obj.GetComponent<Transform> ().SetParent (_currentHUDController.anchors[_currentAnchorToSet].GetComponent<Transform> ());
			}

			// the name of the object
			obj.name = _currentGameObjectName;
			

			// spectific arrangements depedning on the element type // layouts, sprites, etc
			SetUpSpecificParts (obj);
			
			// I don't know why I need this but works
			obj.transform.SetLocalScaleProportional (1.0f);

			// for the subtitle, it is already done in the SpecificPart
			if (_currentCreatingElement != HUDElementTypes.Subtitle) {
			
				// arrange the element according to its anchor
				obj.GetComponent<HUDElement> ().SetUpForAnchor (_currentHUDController.anchors[_currentAnchorToSet]);
				SetUpRectTransformValues (obj);
			}

			if (obj.GetComponent (typeof(IHUDShowable)) != null) {

				((IHUDShowable) obj.GetComponent (typeof(IHUDShowable))).startShown = _currentShowAndHideParams.startShown;
			}

			if (obj.GetComponent (typeof(IHUDFadeInOutAnimated)) != null) {
			
				SetUpFadeAnimations ((IHUDFadeInOutAnimated) obj.GetComponent (typeof (IHUDFadeInOutAnimated)));
			}

			if (obj.GetComponent (typeof(IHUDSlideInOutAnimated)) != null) {
			
				SetUpSlideAnimations ((IHUDSlideInOutAnimated) obj.GetComponent (typeof (IHUDSlideInOutAnimated)));
			}
		}

		/// <summary>
		/// Sets up the specific parts of a given HUDElement, depending on which one we're creating.
		/// </summary>
		/// <param name="obj">The newly instantiated GameObject, as a clone of the prefab corresponding to the given HUDElement.</param>
		private void SetUpSpecificParts (GameObject obj) {
		
			switch (_currentCreatingElement) {
			case HUDElementTypes.Subtitle:
				SetUpSubtitleParts (obj);
				break;
			case HUDElementTypes.WarningPanel:
				SetUpWarningPanelParts (obj);
				break;
			case HUDElementTypes.HintPanel:
				SetUpHintPanelParts (obj);
				break;
			case HUDElementTypes.ProgressBar:
				SetUpProgressBarParts (obj);
				break;
			}
		}


		private void SetUpSubtitleParts (GameObject obj) {
		
			HUDSubtitle sub = obj.GetComponent<HUDSubtitle> ();

			sub.SetRole (_currentSubtitleParams.type);

			sub.LayoutToAnchor (_currentHUDController.anchors [_currentAnchorToSet]);
			sub.SetHeight (_currentSubtitleParams.height);
			sub.SetMargins (_currentSubtitleParams.margins);
			sub.SetDistance (_currentRectParams.distance);
			sub.font = _currentSubtitleParams.font;

			sub.text = _currentSubtitleParams.text;
			sub.textColor = _currentSubtitleParams.textColor;

			if (_currentSubtitleParams.outline) {
			
				sub.SetTextOutline (_currentSubtitleParams.outlineColor);
			}

			if (_currentSubtitleParams.shadow) {
			
				sub.SetTextShadow (_currentSubtitleParams.shadowColor);
			}

		}

		/// <summary>
		/// Sets up the specific hint panel parts.
		/// </summary>
		/// <param name="obj">Object.</param>
		private void SetUpHintPanelParts (GameObject obj) {
		
			HUDHintPanel hint = obj.GetComponent<HUDHintPanel> ();

			hint.SetRole (_currentHintPanelParams.type);

			hint.AnchorLayoutFrom (_currentAnchorToSet);
			hint.SetValues (_currentHintPanelParams.text, _currentHintPanelParams.sprite);
			
			if (_currentHintPanelParams.sprite == null) {
				
				hint.DisableIcon ();
			}
			
			hint.SetResolution (_currentHintPanelParams.spriteSize);
			hint.SetFont (_currentHintPanelParams.font);
			hint.SetTextColor (_currentHintPanelParams.textColor);
			hint.LayoutContentAs (_currentHintPanelParams.layout);

			if (_currentHintPanelParams.shadow) {

				hint.SetTextShadow (_currentHintPanelParams.shadowColor);
			}

			if (_currentHintPanelParams.outline) {
				
				hint.SetTextOutline (_currentHintPanelParams.outlineColor);
			}
			
			if ( (_currentAnchorToSet == HUDAnchorTypes.Left || _currentAnchorToSet == HUDAnchorTypes.Right)
			    && (_currentHintPanelParams.layout == HUDHintLayouts.IconAboveTextBelow || _currentHintPanelParams.layout == HUDHintLayouts.TextAboveIconBelow)) {
				
				Debug.Log ("Forcing align from center");
				
				hint.AlignFromCenter ();
			}
			else {
				
				Debug.Log ("Aligning normally at: " + _currentAnchorToSet.ToString ());
				
				hint.AlignFrom (_currentAnchorToSet);
			}

		}

		public void SetUpWarningPanelParts (GameObject obj) {
		
			HUDWarningPanel warning = obj.GetComponent<HUDWarningPanel> ();

			warning.SetValues (_currentWarningPanelParams.text, _currentWarningPanelParams.backgroundSprite, _currentWarningPanelParams.warningIcon);
			warning.SetSize (_currentWarningPanelParams.backgroundSize);
			warning.SetIconSize (_currentWarningPanelParams.iconSize);
			warning.SetFont (_currentWarningPanelParams.font);
			warning.SetTextColor (_currentWarningPanelParams.textColor);

			warning.AnchorLayoutFrom (_currentAnchorToSet);

			if (_currentWarningPanelParams.shadow) {
				
				warning.SetTextShadow (_currentWarningPanelParams.shadowColor);
			}
			
			if (_currentWarningPanelParams.outline) {
				
				warning.SetTextOutline (_currentWarningPanelParams.outlineColor);
			}

			if (_currentWarningPanelParams.layout == HUDWarningLayouts.IconLeftTextRight) {
			
				warning.SetImageFirst ();
			}
			else {

				warning.SetTextFirst ();
			}
		}

		public void SetUpProgressBarParts (GameObject obj) {
		
			HUDProgressBar bar = obj.GetComponent<HUDProgressBar> ();

			bar.AnchorLayoutFrom (_currentAnchorToSet);
			bar.LayoutContentAs (_currentProgressBarParams.layout);
			bar.AlignContentTo (_currentProgressBarParams.alignment);
			bar.SetRole (_currentProgressBarParams.type);

			bar.useIntegerSteps = _currentProgressBarParams.useIntegerDisplay;
			bar.toStringFloatFormat = _currentProgressBarParams.floatFormatting;

			// we make use of this distinction now in order to have the displayed value correctly
			if (bar.useIntegerSteps) {
			
				bar.intMaxValue = (int) _currentProgressBarParams.maxValue;
				bar.intMinValue = (int) _currentProgressBarParams.minValue;
				bar.intValue 	=  (int) _currentProgressBarParams.value;
			}
			else {
			
				bar.maxValue 	= _currentProgressBarParams.maxValue;
				bar.minValue 	= _currentProgressBarParams.minValue;
				bar.value 		= _currentProgressBarParams.value;
			}


			bar.fillSprite = _currentProgressBarParams.fill;
			bar.backSprite = _currentProgressBarParams.background;

			bar.barHeight = _currentProgressBarParams.barHeight;
			bar.barLenght = _currentProgressBarParams.barLength;


			LayoutElement canvasLayout = bar.valueHolderCanvas.GetComponent<LayoutElement> ();
			canvasLayout. minWidth 			= _currentProgressBarParams.textHolderSize.x;
			canvasLayout. preferredWidth 	= _currentProgressBarParams.textHolderSize.x;
			canvasLayout. minHeight 		= _currentProgressBarParams.textHolderSize.y;
			canvasLayout. preferredHeight 	= _currentProgressBarParams.textHolderSize.y;

			bar.valueHolderImage.sprite = _currentProgressBarParams.textHolder;

			// if no holder sprite was set, we set the maximum height of the canvas to the same height
			// of the bar, so the text doesn't get bigger than that maximum
			if (_currentProgressBarParams.textHolder == null) {
			
				bar.valueHolderImage.enabled = false;

				bar.valueHolderCanvas.GetComponent<LayoutElement> ().minHeight 
					= bar.valueHolderCanvas.GetComponent<LayoutElement> ().preferredHeight 
						= _currentProgressBarParams.barHeight;

			}
			else {

				bar.valueHolderImage.enabled = true;
				
			}

			bar.valueText.font 	= _currentProgressBarParams.font;
			bar.valueText.color = _currentProgressBarParams.textColor;

			if (_currentProgressBarParams.shadow) {

				bar.SetTextShadow (_currentProgressBarParams.shadowColor);
			}
			if (_currentProgressBarParams.outline) {

				bar.SetTextOutline (_currentProgressBarParams.outlineColor);
			}

			bar.SetLenght (_currentProgressBarParams.barLength);
			
			bar.valueHolderCanvas.gameObject.SetActive (_currentProgressBarParams.displayNumber);
		}
		
		/// <summary>
		/// Sets up rect transform values according to the input.
		/// </summary>
		/// <param name="obj">Object.</param>
		private void SetUpRectTransformValues (GameObject obj) {
			
			RectTransform r = obj.GetComponent<RectTransform> ();
			
			if (_currentAnchorToSet == HUDAnchorTypes.Top || _currentAnchorToSet == HUDAnchorTypes.Bottom) {
				
				r.SetLocalPos (_currentRectParams.position, (_currentAnchorToSet == HUDAnchorTypes.Top) ? - _currentRectParams.distance : _currentRectParams.distance, 0.0f);
			}
			else if (_currentAnchorToSet == HUDAnchorTypes.Left || _currentAnchorToSet == HUDAnchorTypes.Right || _currentAnchorToSet == HUDAnchorTypes.Center) {
				
				r.SetLocalPos ( (_currentAnchorToSet == HUDAnchorTypes.Right) ? - _currentRectParams.distance : _currentRectParams.distance, _currentRectParams.position , 0.0f);
			}
			
			r.SetLocalEulerAngles (0.0f, 0.0f, _currentRectParams.tilt);
			
		}

		private void SetUpFadeAnimations (IHUDFadeInOutAnimated elem) {
		
			elem.startShown = _currentShowAndHideParams.startShown;
			elem.animate = _currentShowAndHideParams.animate;

			elem.fadeIn = _currentShowAndHideParams.fadeIn;
			elem.fadeOut = _currentShowAndHideParams.fadeOut;

			elem.enterTime = _currentShowAndHideParams.enterTime;
			elem.exitTime = _currentShowAndHideParams.exitTime;
		}

		private void SetUpSlideAnimations (IHUDSlideInOutAnimated elem) {
		
			elem.startShown = _currentShowAndHideParams.startShown;
			elem.animate = _currentShowAndHideParams.animate;

			elem.slideIn = _currentShowAndHideParams.slideIn;
			elem.slideOut = _currentShowAndHideParams.slideOut;

			elem.enterTime = _currentShowAndHideParams.enterTime;
			elem.exitTime = _currentShowAndHideParams.exitTime;
		}

		# endregion

		/// <summary>
		/// Called when the window is closed.
		/// </summary>
		void OnDestroy () {

			Debug.Log ("Custom HUD Creator window closed.");
		}
	}

	/// <summary>
	/// Popup for notifying that there is not a HUDController in the scene and promptin the developer to create one.
	/// </summary>
	public class HUDDoesNotExistPopup : EditorWindow {
		
		void OnGUI () {
			
			EditorGUILayout.BeginVertical ();
			
			GUILayout.FlexibleSpace ();
			
			EditorGUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			
			EditorGUILayout.BeginVertical ();
			
			GUILayout.Label ("A HUDController couldn't be found in this scene.", EditorStyles.label);
			GUILayout.Label ("Do you wish to create a new one?", EditorStyles.boldLabel);
			
			EditorGUILayout.EndVertical ();
			GUILayout.FlexibleSpace ();
			
			EditorGUILayout.EndHorizontal ();
			
			GUILayout.FlexibleSpace ();
			
			EditorGUILayout.BeginHorizontal ();
			
			GUILayout.FlexibleSpace ();
			
			if (GUILayout.Button (new GUIContent ("Create new HUD",
			                                      "Open the HUD Creator to create and initialize a HUDController that can be edited afterwards."),
			                      GUILayout.MinHeight (30.0f),
			                      GUILayout.Width (150.0f)) ) {
				
				HUDEditorWindow.CreateNewHUDBeforeEdit ();
				ClosePopup ();
			}
			
			if (GUILayout.Button (new GUIContent ("Close",
			                                      "Keep the HUD as it is and dismiss this warning."),
			                      GUILayout.MinHeight (30.0f),
			                      GUILayout.Width (150.0f)) ) {
				
				ClosePopup ();
			}
			
			GUILayout.FlexibleSpace ();
			
			EditorGUILayout.EndHorizontal ();
			EditorGUILayout.EndVertical ();
			
		}
		
		/// <summary>
		/// Closes the popup.
		/// </summary>
		private void ClosePopup () {
			
			this.Close ();
		}
	}
}