﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Collections;

namespace CustomHUD {

	
	public class HUDCreatorWindow : EditorWindow {

		public const int RESOLUTION_X = 500;
		public const int RESOLUTION_Y = 200;

		const int DEFAULT_REFERENCE_RESOLUTION_X = 1920;
		const int DEFAULT_REFERENCE_RESOLUTION_Y = 1080;

		public static HUDAnchorTypes[] anchorArray = {	HUDAnchorTypes.Top,
			HUDAnchorTypes.Bottom, 
			HUDAnchorTypes.Left, 
			HUDAnchorTypes.Right,
			HUDAnchorTypes.Center
		};

		# region static methods


		[MenuItem ("Custom HUD/Create new HUD")]
		public static void ShowHUDCreator () {

			if (!HUDController.hasSingletonInstanceSet) {

				// even though there is no static Instance set, we'll search for it anyway and assign it if found
				HUDController[] hudsInScene = GameObject.FindObjectsOfType<HUDController> ();

				if (hudsInScene.Length > 0) { // only if found one

					hudsInScene[0].EditorInitialize ();

					ShowHUDCreator (); // call again
					return;
				}

				// show the window				
				EditorWindow window = EditorWindow.GetWindow(typeof(HUDCreatorWindow), true);

				window.name = "HUD Creator";
				window.titleContent = new GUIContent ("HUD Creator");
				window.minSize = new Vector2 (HUDConst.DEFAULT_WINDOW_SIZE_X, HUDConst.DEFAULT_WINDOW_SIZE_Y);

			}
			else {

				ShowAlreadyExistingHUDPopup ();
			}
		}

		[MenuItem ("Custom HUD/Create default HUD")]
		public static void CreateDefaultHUD () {
		
			if (!HUDController.hasSingletonInstanceSet ) {
			
				new HUDCreatorWindow ().CreateNewHUDController ();
			}
		}
		
		/// <summary>
		/// Shows a popup indicating that there exists a HUDController in the scene and prompts the user with options.
		/// </summary>
		private static void ShowAlreadyExistingHUDPopup () {
			
			// window for notifying that a controller already exists
			EditorWindow popup = EditorWindow.GetWindow(typeof(HUDAlreadyExistsPopup), true);

			popup.name = "Already existing HUD";
			popup.titleContent = new GUIContent ("Already existing HUD");
			popup.minSize = new Vector2 (HUDConst.EDITOR_POPUP_SIZE_X, HUDConst.EDITOR_POPUP_SIZE_Y);
			popup.maxSize = new Vector2 (HUDConst.EDITOR_POPUP_SIZE_X, HUDConst.EDITOR_POPUP_SIZE_Y);
		}
		
		/// <summary>
		/// Deletes the existing HUDController, if any, and proceeds with the creation of a new one.
		/// </summary>
		public static void ReplaceExistingHUDController () {
			
			HUDController.Instance.EditorDestroy ();
			ShowHUDCreator ();
		}

		# endregion

		# region attributes

		private Vector2 _tempReferenceResolution = new Vector2 (DEFAULT_REFERENCE_RESOLUTION_X, DEFAULT_REFERENCE_RESOLUTION_Y);
		private CanvasScaler.ScaleMode _tempScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
		private float _tempMatchWidthOrHeight = 1.0f;
		private string _tempHudName = "Custom HUD Controller";
	
		private Font _tempDefaultFont = null;

		private Texture2D _infoTexture = null;
		
		# endregion

		# region methods

		/// <summary>
		/// Creates the new HUD controller.
		/// </summary>
		/// <returns><c>true</c>, if new HUD controller was created, <c>false</c> otherwise.</returns>
		private bool CreateNewHUDController () {

			bool success = true; // whether the new hud controller has been created correctly or not

			HUDController newHud = new GameObject (_tempHudName,
			                                       typeof(HUDController),
			                                       typeof(Canvas), 
			                                       typeof(CanvasScaler)
			                                       ) .GetComponent<HUDController> ();

			newHud.GetComponent<Canvas> ().renderMode = RenderMode.ScreenSpaceOverlay;
			
			newHud.GetComponent<CanvasScaler> ().uiScaleMode 			= _tempScaleMode;
			newHud.GetComponent<CanvasScaler> ().matchWidthOrHeight 	= _tempMatchWidthOrHeight;
			newHud.GetComponent<CanvasScaler> ().referenceResolution 	= _tempReferenceResolution;

			newHud.SetDefaultFont (_tempDefaultFont);

			newHud.EditorInitialize ();
			
			foreach (HUDAnchorTypes type in anchorArray) {
				
				GameObject obj = new GameObject("Anchor " + type.ToString(), typeof(HUDAnchor), typeof(Canvas));
				
				obj.GetComponent<Transform> ().SetParent (newHud.GetComponent<Transform> ());

				obj.GetComponent<HUDAnchor> ().EditorInitialize (type, newHud);
			}

			newHud.EditorCacheAnchors ();
			newHud.InitializeAnchorDictionaryWithCachedAnchors ();

			return success;
		}

		/// <summary>
		/// Inits some values in this window.
		/// </summary>
		public void InitWindow () {
		
			_tempDefaultFont = HUDResources.defaultFont;
			_infoTexture = HUDResources.canvasScalerInfoImage;
		}

		/// <summary>
		/// Closes the HUD Creator window.
		/// </summary>
		private void CloseWindow () {
		
			this.Close ();
		}


		# endregion

		private void OnEnable () {
			
			InitWindow ();
		}

		private void OnDestroy () {

			_tempDefaultFont = null;
			DestroyImmediate (_tempDefaultFont);
		}


		# region window paint

		void OnGUI () {

			EditorGUILayout.BeginVertical ();

			EditorGUILayout.Separator ();
			
			PaintFirstPart ();

			EditorGUILayout.Separator ();

			PaintCanvasRelatedPart ();

			PaintFinalPart ();
			EditorGUILayout.EndVertical ();
		}

		private Color selectedColor = Color.black;

		/// <summary>
		/// Paints the first part.
		/// </summary>
		private void PaintFirstPart () {
		
			GUILayout.Label ("Basic settings", EditorStyles.boldLabel);
			EditorGUILayout.Separator ();

			EditorGUILayout.BeginHorizontal ();

			GUILayout.Label ("Name for the HUD GameObject:", EditorStyles.label, GUILayout.MinWidth (HUDConst.MIN_LABEL_WIDTH));

			// _tempHudName holds the name that will be given to the HUD controller GameObject
			_tempHudName = GUILayout.TextField(_tempHudName, 50, GUILayout.MinWidth(HUDConst.MIN_INPUT_WIDTH));




			GUILayout.FlexibleSpace ();

			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.BeginHorizontal ();

			GUILayout.Label ("Default font for HUD elements:", EditorStyles.label, GUILayout.MinWidth (HUDConst.MIN_LABEL_WIDTH));

			_tempDefaultFont = (Font) EditorGUILayout.ObjectField (new GUIContent("", "Drag here a Font from your project to make it the default font that the elements of this HUD will use. When creating them, you can set individual fonts for them instead."),
			                                                       _tempDefaultFont, typeof(Font), false, GUILayout.MinWidth (HUDConst.MIN_INPUT_WIDTH));
		
			GUILayout.FlexibleSpace ();
			EditorGUILayout.EndHorizontal ();
		}

		/// <summary>
		/// Paints the canvas related part.
		/// </summary>
		private void PaintCanvasRelatedPart () {
		
			GUILayout.Label ("Scaling settings", EditorStyles.boldLabel);
			EditorGUILayout.Separator ();

			EditorGUILayout.BeginHorizontal ();
			
			GUILayout.Box ("Set the reference resolution to be equal to which you have used to design your interfaces.\n" 
			               + "The default scaling settings keep the interface elements undeformed and at the same distance from the edges of the screen if it becomes wider. You can make further adjustments in the CanvasScaler component of the HUD GameObject.",
			               GUILayout.Width(280), 
			               GUILayout.ExpandWidth(true),
			               GUILayout.MinHeight (100));

			GUILayout.Label (_infoTexture,
			               GUILayout.Width (300),
			               GUILayout.ExpandWidth(false),
			               GUILayout.MinHeight (100));



			EditorGUILayout.EndHorizontal ();
			

			EditorGUILayout.Separator ();

			
			EditorGUILayout.BeginHorizontal ();
			
			GUILayout.Label ("Reference resolution:", EditorStyles.label, GUILayout.MinWidth (HUDConst.MIN_LABEL_WIDTH));
			
			_tempReferenceResolution = EditorGUILayout.Vector2Field ("", _tempReferenceResolution, GUILayout.MaxWidth (150));
			
			GUILayout.FlexibleSpace ();
			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.BeginHorizontal ();

			GUILayout.Label ("UI scale mode:", EditorStyles.label, GUILayout.MinWidth (HUDConst.MIN_LABEL_WIDTH));

			_tempScaleMode = (CanvasScaler.ScaleMode) EditorGUILayout.EnumPopup (_tempScaleMode,GUILayout.MinWidth(HUDConst.MIN_INPUT_WIDTH));

			GUILayout.FlexibleSpace ();

			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.BeginHorizontal ();

			GUILayout.Label ("Match width/height:", EditorStyles.label, GUILayout.MinWidth (HUDConst.MIN_LABEL_WIDTH));

			_tempMatchWidthOrHeight = EditorGUILayout.Slider (_tempMatchWidthOrHeight, 0.0f, 1.0f, GUILayout.MinWidth (HUDConst.MIN_INPUT_WIDTH));

			GUILayout.FlexibleSpace ();

			EditorGUILayout.EndHorizontal ();

		}

		/// <summary>
		/// Paints the final part of the GUI of this window.
		/// </summary>
		private void PaintFinalPart () {

			GUILayout.FlexibleSpace ();
			
			EditorGUILayout.BeginHorizontal ();
			
			GUILayout.FlexibleSpace ();
			
			if (GUILayout.Button (new GUIContent ("Create HUD", "Instantiates a complete HUD Controller with the specified characteristics."), GUILayout.MinHeight (30.0f), GUILayout.Width (300.0f))) {
				
				bool success = CreateNewHUDController ();

				if (success) {

					CloseWindow ();
				}
			}
			GUILayout.FlexibleSpace ();
			
			EditorGUILayout.EndHorizontal ();
		}

		# endregion
	}

	/// <summary>
	/// Popup for notifying that there is a HUD instantiated already and ask if they want a new one or keep it.
	/// </summary>
	public class HUDAlreadyExistsPopup : EditorWindow {

		void OnGUI () {

			EditorGUILayout.BeginVertical ();

			GUILayout.FlexibleSpace ();

			EditorGUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			
			EditorGUILayout.BeginVertical ();
			
			GUILayout.Label ("An existing HUDController has been found in this scene.", EditorStyles.label);
			GUILayout.Label ("Do you wish edit it or replace it with a new one?", EditorStyles.boldLabel);

			EditorGUILayout.EndVertical ();
			GUILayout.FlexibleSpace ();
			
			EditorGUILayout.EndHorizontal ();
			
			GUILayout.FlexibleSpace ();

			EditorGUILayout.BeginHorizontal ();

			GUILayout.FlexibleSpace ();

			if (GUILayout.Button (new GUIContent ("Edit", "Open the HUD Editor to add elements to the currently existing HUDController."), GUILayout.MinHeight (30.0f), GUILayout.Width (100.0f)) ) {
				
				Debug.Log ("Create me!");
			}

			if (GUILayout.Button (new GUIContent ("Replace", "Remove the currently existing HUDController and open the HUD Creator to create a new one."), GUILayout.MinHeight (30.0f), GUILayout.Width (100.0f)) ) {

				HUDCreatorWindow.ReplaceExistingHUDController ();
				ClosePopup ();
			}

			if (GUILayout.Button (new GUIContent ("Close", "Keep the HUD as it is and dismiss this warning."), GUILayout.MinHeight (30.0f), GUILayout.Width (100.0f)) ) {
				
				ClosePopup ();
			}

			GUILayout.FlexibleSpace ();

			EditorGUILayout.EndHorizontal ();
			EditorGUILayout.EndVertical ();
			
		}

		/// <summary>
		/// Closes the popup.
		/// </summary>
		private void ClosePopup () {
			
			this.Close ();
		}
	}
}
