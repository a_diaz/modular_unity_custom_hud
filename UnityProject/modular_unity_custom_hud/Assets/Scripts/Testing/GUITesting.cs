﻿using UnityEngine;
using System.Collections;

public class GUITesting : MonoBehaviour {

	string sentence = "The button has not been clicked.";

	void OnGUI () {
	

		if (GUI.Button (new Rect (10, 10, 250, 40), "Click me!")) {

			sentence = "The button has been clicked.";
		}

		GUI.Label (new Rect (10, 30, 250, 20), sentence);
	}
}
