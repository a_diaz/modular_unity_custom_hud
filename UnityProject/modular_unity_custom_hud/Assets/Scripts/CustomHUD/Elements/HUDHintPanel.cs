﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace CustomHUD {

	/// <summary>
	/// A HUDElement that is used to display some short hint, like a suggestion of pressing a key or performing a precise action.
	/// </summary>
	public class HUDHintPanel : HUDElement, IHUDHintPanel, IHUDFadeInOutAnimated, IHUDSlideInOutAnimated {

		# region enum

		# endregion
		# region attributes and properties
		
		/// <summary>
		/// The type of hint that this <see cref="CustomHUD.HUDHintPanel"/> will be suscribed as to the HUDController.
		/// </summary>
		[Tooltip ("The type of hint that this HUDHintPanel will be suscribed as to the HUDControlled.")]
		[SerializeField]
		private HUDHintTypes _subscribeAs;

		public HUDHintTypes subscribeAs {

			get { return _subscribeAs; }
		}

		/// <summary>
		/// The GameObject that holds the HorizontalOrVerticalLayout that will arrange the Image and the Text.
		/// </summary>
		[Tooltip("The GameObject that holds the HorizontalOrVerticalLayout that will arrange the Image and the Text.")]
		public GameObject hintPanelLayout;

		/// <summary>
		/// The current HintLayouts layout that the UI elements of this <see cref="CustomHUD.HUDHintPanel"/> are set to follow.
		/// </summary>
		private HUDHintLayouts _currentLayout;

		/// <summary>
		/// Gets or sets the current HintLayouts layout that the UI elements of this <see cref="CustomHUD.HUDHintPanel"/> are set to follow.
		/// </summary>
		/// <value>The current layout.</value>
		public HUDHintLayouts currentLayout {
			
			get { return _currentLayout; }
			set { _currentLayout = value; }
		}

		/*
			UI Elements
		 */

		/// <summary>
		/// The UI Text that holds the text for the hint.
		/// </summary>
		[Tooltip ("The UI Text that holds the text for the hint.")]
		public Text hintUIText;

		/// <summary>
		/// The UI Image that holds the icon of the hint.
		/// </summary>
		[Tooltip ("The UI Image that holds the icon of the hint.")]
		public Image hintUIImage;
		
		/// <summary>
		/// Gets or sets the text placed in the UI Text of this <see cref="CustomHUD.HUDHintPanel"/>, representing the content of the hint.
		/// </summary>
		/// <value>The hint string.</value>
		public string text {
			
			get { return hintUIText.text; }
			set { hintUIText.text = value; }
		}

		/// <summary>
		/// Gets or sets the Color of the text of the hint.
		/// </summary>
		/// <value>The color of the text.</value>
		public Color textColor {

			get { return hintUIText.color; }
			set { hintUIText.color = value; }
		}

		/// <summary>
		/// Gets or sets the Sprite of the UI Image that serves as icon of this <see cref="CustomHUD.HUDHintPanel"/>.
		/// </summary>
		/// <value>The hint icon.</value>
		public Sprite icon {
			
			get { return hintUIImage.sprite; } 
			set { hintUIImage.sprite = value; } 
		}

		/// <summary>
		/// Gets or sets the font of the UI Text of this <see cref="CustomHUD.HUDHintPanel"/>.
		/// </summary>
		/// <value>The font.</value>
		public Font font {
			
			get { return hintUIText.font; }
			set	{ hintUIText.font = value; }
		}

		/// <summary>
		/// Whether this <see cref="CustomHUD.HUDHintPanel"/> is currently shown on screen.
		/// </summary>
		private bool _isCurrentlyShown;

		/// <summary>
		/// Gets a value indicating whether this <see cref="CustomHUD.HUDHintPanel"/> is shown.
		/// </summary>
		/// <value><c>true</c> if is shown; otherwise, <c>false</c>.</value>
		public bool isCurrentlyShown {

			get { return _isCurrentlyShown; } 
		}

		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDHintPanel"/> should start displayed on screen or not.
		/// </summary>
		[SerializeField]
		private bool _startShown;

		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDHintPanel"/> should start displayed on screen or not.
		/// </summary>
		[SerializeField]
		public bool startShown {
			
			get { return _startShown; }
			set { _startShown = value; }
		}

		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDHintPanel"/> will have some animations when being displayed.
		/// </summary>
		[SerializeField]
		private bool _animate;

		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDHintPanel"/> will have some animations when being displayed.
		/// </summary>
		[SerializeField]
		public bool animate {
			
			get { return _animate; }
			set { _animate = value; }
		}

		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDHintPanel"/> should have a fade-in effect when being shown or not, if <see cref="CustomHUD.HUDHintPanel.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private bool _fadeIn;

		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDHintPanel"/> should have a fade-in effect when being shown or not, if <see cref="CustomHUD.HUDHintPanel.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		public bool fadeIn {
			
			get { return _fadeIn; }
			set { _fadeIn = value; }
		}
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDHintPanel"/> should have a fade-out effect when being hidden or not, if <see cref="CustomHUD.HUDHintPanel.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private bool _fadeOut;

		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDHintPanel"/> should have a fade-out effect when being hidden or not, if <see cref="CustomHUD.HUDHintPanel.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		public bool fadeOut {
			
			get { return _fadeOut; }
			set { _fadeOut = value; }
		}

		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDHintPanel"/> should have a slide-in effect when being shown or not, if <see cref="CustomHUD.HUDHintPanel.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private bool _slideIn;

		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDHintPanel"/> should have a slide-in effect when being shown or not, if <see cref="CustomHUD.HUDHintPanel.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		public bool slideIn {
			
			get { return _slideIn; }
			set { _slideIn = value; }
		}

		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDHintPanel"/> should have a slide-out effect when being hidden or not, if <see cref="CustomHUD.HUDHintPanel.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private bool _slideOut;

		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDHintPanel"/> should have a slide-out effect when being hidden or not, if <see cref="CustomHUD.HUDHintPanel.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		public bool slideOut {
			
			get { return _slideOut; }
			set { _slideOut = value; }
		}

		/// <summary>
		/// The it takes for the fade/hide-in animations of this <see cref="CustomHUD.HUDHintPanel"/> to be completed, if <see cref="CustomHUD.HUDHintPanel.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private float _enterTime;
		
		/// <summary>
		/// The it takes for the fade/hide-in animations of this <see cref="CustomHUD.HUDHintPanel"/> to be completed, if <see cref="CustomHUD.HUDHintPanel.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		public float enterTime {
			
			get { return _enterTime; }
			set { _enterTime = value; }
		}

		/// <summary>
		/// The it takes for the fade/hide-out animations of this <see cref="CustomHUD.HUDHintPanel"/> to be completed, if <see cref="CustomHUD.HUDHintPanel.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private float _exitTime;

		/// <summary>
		[SerializeField]
		/// The it takes for the fade/hide-out animations of this <see cref="CustomHUD.HUDHintPanel"/> to be completed, if <see cref="CustomHUD.HUDHintPanel.animate"/> is set to true.
		/// </summary>
		public float exitTime {
			
			get { return _exitTime; }
			set { _exitTime = value; }
		}

		# endregion

		# region unity default

		new void Awake () {

			base.Awake ();
			_type = HUDElementTypes.HintPanel;
		}

		void Start () {

		}

		# endregion

		# region methods
		/// <summary>
		/// Subscribes this <see cref="CustomHUD.HUDHintPanel"/> to the <see cref="CustomHUD.HUDController"/>  in the scene.
		/// </summary>
		protected override void SubscribeToController () {
		
			Debug.Log ("Hint panel subscribing to controller");
			_controller.SubscribeAs (this);
			CheckStartShown ();
		}
		
		/// <summary>
		/// Checks if this <see cref="CustomHUD.HUDHintPanel"/> should start shown or not, and shows or hides it accordingly.
		/// </summary>
		private void CheckStartShown () {
			
			if (!_startShown) {
				
				hintUIImage	.CrossFadeAlpha (0.0f, 0.0f, false);
				hintUIText	.CrossFadeAlpha (0.0f, 0.0f, false);
				
				Disable ();
			}
			else {
				
			}
			
			_isCurrentlyShown = _startShown;
			gameObject.SetActive (_startShown);
		}

		/// <summary>
		/// Enables the GameObject holding this <see cref="CustomHUD.HUDHintPanel"/>.
		/// </summary>
		public void Enable () {

			this.gameObject.SetActive (true);
			
			_isCurrentlyShown = true;
		}

		/// <summary>
		/// Disables the GameObject holding this <see cref="CustomHUD.HUDHintPanel"/>.
		/// </summary>
		public void Disable () {
		
			this.gameObject.SetActive (false);
			
			_isCurrentlyShown = false;
		}

		/// <summary>
		/// Sets the role of this <see cref="CustomHUD.HUDHintPanel"/>, as which it will be subscribed to the <see cref="CustomHUD.HUDController"/>.
		/// </summary>
		/// <param name="role">Role.</param>
		public void SetRole (HUDHintTypes role) {
		
			_subscribeAs = role;
		}


		/// <summary>
		/// Sets the values of this <see cref="CustomHUD.HUDHintPanel"/>. These values can be empty.
		/// </summary>
		/// <param name="hintText">Hint text.</param>
		/// <param name="hintIcon">Hint icon.</param>
		public void SetValues (string hintText, Sprite hintIcon) {
		
			hintUIText.text = hintText;
			hintUIImage.sprite = hintIcon;
		}

		/// <summary>
		/// Sets the icon of this <see cref="CustomHUD.HUDHintPanel"/>.
		/// </summary>
		/// <param name="newIcon">The new icon for the hint.</param>
		public void SetIcon (Sprite newIcon) {
		
			icon = newIcon;
		}

		/// <summary>
		/// Sets the text of this <see cref="CustomHUD.HUDHintPanel"/>.
		/// </summary>
		/// <param name="newText">The new text for the hint.</param>
		public void SetText (string newText) {
		
			text = newText;
		}

		/// <summary>
		/// Sets the font of the text of this <see cref="CustomHUD.HUDHintPanel"/>.
		/// </summary>
		/// <param name="font">Font.</param>
		public void SetFont (Font font) {
		
			hintUIText.font = font;
		}

		/// <summary>
		/// Sets the size of the font of the text of this <see cref="CustomHUD.HUDHintPanel"/>.
		/// </summary>
		/// <param name="size">Size.</param>
		public void SetFontSize (int size) {
		
			hintUIText.fontSize = size;
		}

		/// <summary>
		/// Sets the color of the UI Text of this HUDHintPanel to a given new Color.
		/// </summary>
		/// <param name="newColor">The new Color.</param>
		public void SetTextColor (Color newColor) {
		
			hintUIText.color = newColor;
		}

		/// <summary>
		/// Disables the icon Image of this <see cref="CustomHUD.HUDHintPanel"/>, setting it inactive.
		/// </summary>
		public void DisableIcon () {
		
			hintUIImage.gameObject.SetActive (false);
		}

		/// <summary>
		/// Enables the icon Image of this <see cref="CustomHUD.HUDHintPanel"/>, setting it active.
		/// </summary>
		public void EnableIcon () {

			hintUIImage.gameObject.SetActive (true);
		}

		/// <summary>
		/// Disables the Text of this <see cref="CustomHUD.HUDHintPanel"/>, setting it inactive.
		/// </summary>
		public void DisableText () {
		
			hintUIText.gameObject.SetActive (false);
		}

		/// <summary>
		/// Enables the Text of this <see cref="CustomHUD.HUDHintPanel"/>, setting it active.
		/// </summary>
		public void EnableText () {
		
			hintUIText.gameObject.SetActive (true);
		}

		/// <summary>
		/// Adds a Shadow effect component to the UI Text of this <see cref="CustomHUD.HUDHintPanel"/> of the given Color.
		/// </summary>
		/// <param name="color">Color.</param>
		public void AddTextShadow (Color color) {
		
			hintUIText.gameObject.AddComponent<Shadow> ().effectColor = color;
		}

		/// <summary>
		/// Sets the color of the Shadow effect component of the UI Text of this <see cref="CustomHUD.HUDHintPanel"/> of the given Color. Creates it if the component doesn't exist.
		/// </summary>
		/// <param name="color">Color.</param>
		public void SetTextShadow (Color color) {
		
		
			if (hintUIText.GetComponent<Shadow> () != null) {
			
				hintUIText.GetComponent<Shadow> ().effectColor = color;
			}
			else {

				AddTextShadow (color);
			}
		}

		/// <summary>
		/// Removes the Shadow effect component of the UI Text of this <see cref="CustomHUD.HUDHintPanel"/>.
		/// </summary>
		public void RemoveTextShadow () {
		
			DestroyImmediate (hintUIText.GetComponent<Shadow> ());
		}

		/// <summary>
		/// Adds a Outline effect component to the UI Text of this <see cref="CustomHUD.HUDHintPanel"/> of the given Color.
		/// </summary>
		/// <param name="color">Color.</param>
		public void AddTextOutline (Color color) {
			
			hintUIText.gameObject.AddComponent<Outline> ().effectColor = color;
		}
		
		/// <summary>
		/// Sets the color of the Outline effect component of the UI Text of this <see cref="CustomHUD.HUDHintPanel"/> of the given Color. Creates it if the component doesn't exist.
		/// </summary>
		/// <param name="color">Color.</param>
		public void SetTextOutline (Color color) {
			
			
			if (hintUIText.GetComponent<Outline> () != null) {
				
				hintUIText.GetComponent<Outline> ().effectColor = color;
			}
			else {
				
				AddTextOutline (color);
			}
		}
		
		/// <summary>
		/// Removes the Outline effect component of the UI Text of this <see cref="CustomHUD.HUDHintPanel"/>.
		/// </summary>
		public void RemoveTextOutline () {
			
			DestroyImmediate (hintUIText.GetComponent<Outline> ());
		}

		/// <summary>
		/// Sets the dimensions of the RectTransforms of the GameObjects according to a given value.
		/// </summary>
		/// <param name="res">Res.</param>
		public void SetResolution (Vector2 res) {
		
			LayoutElement elemIcon = hintUIImage.GetComponent<LayoutElement> ();
			LayoutElement elemText = hintUIText.GetComponent<LayoutElement> ();

			elemIcon.minWidth = elemIcon.preferredWidth = res.x;
			elemIcon.minHeight = elemIcon.preferredHeight = elemText.preferredHeight = res.y;

			hintUIText.resizeTextMaxSize = (int) res.y;
		}

		/// <summary>
		/// Anchors the layout of the HUDHintPanel from a given place.
		/// </summary>
		/// <param name="from">From.</param>
		public void AnchorLayoutFrom ( HUDAnchorTypes from ) {
		
			switch (from) {
			
			case HUDAnchorTypes.Top:
				AnchorLayoutFromTop ();
				break;
			case HUDAnchorTypes.Bottom:
				AnchorLayoutFromBottom ();
				break;
			case HUDAnchorTypes.Left:
				AnchorLayoutFromLeft ();
				break;
			case HUDAnchorTypes.Right:
				AnchorLayoutFromRight ();
				break;
			case HUDAnchorTypes.Center:
				AnchorLayoutFromCenter ();
				break;
			}
		}

		/// <summary>
		/// Sets up the layout RectTransform from the top.
		/// </summary>
		public void AnchorLayoutFromTop () {
			
			SetUpLayoutRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MAX_VALUE),
			                                        new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MAX_VALUE),
			                                        new Vector2 (HUDConst.PIVOT_X_FOR_TOP_ANCHOR, HUDConst.PIVOT_Y_FOR_TOP_ANCHOR)
			                                        );
		}
		
		/// <summary>
		/// Sets up the layout RectTransform from the bottom.
		/// </summary>
		public void AnchorLayoutFromBottom () {
			
			SetUpLayoutRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MIN_VALUE),
			                                        new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MIN_VALUE),
			                                        new Vector2 (HUDConst.PIVOT_X_FOR_BOTTOM_ANCHOR, HUDConst.PIVOT_Y_FOR_BOTTOM_ANCHOR)
			                                        );
		}
		
		/// <summary>
		/// Sets up the layout RectTransform from the left.
		/// </summary>
		public void AnchorLayoutFromLeft () {
			
			SetUpLayoutRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MIN_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                                        new Vector2 (HUDConst.ANCHOR_MIN_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                                        new Vector2 (HUDConst.PIVOT_X_FOR_LEFT_ANCHOR, HUDConst.PIVOT_Y_FOR_LEFT_ANCHOR)
			                                        );
		}
		
		/// <summary>
		/// Sets up the layout RectTransform from the right.
		/// </summary>
		public void AnchorLayoutFromRight () {
			
			SetUpLayoutRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MAX_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                                        new Vector2 (HUDConst.ANCHOR_MAX_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                                        new Vector2 (HUDConst.PIVOT_X_FOR_RIGHT_ANCHOR, HUDConst.PIVOT_Y_FOR_RIGHT_ANCHOR)
			                                        );
		}
		
		/// <summary>
		/// Sets up the layout RectTransform from the center.
		/// </summary>
		public void AnchorLayoutFromCenter () {
			
			SetUpLayoutRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                                        new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                                        new Vector2 (HUDConst.PIVOT_X_FOR_CENTER_ANCHOR, HUDConst.PIVOT_Y_FOR_CENTER_ANCHOR)
			                                        );
		}

		/// <summary>
		/// Sets up the layout rect tranform with given values.
		/// </summary>
		/// <param name="anchorMin">Anchor minimum.</param>
		/// <param name="anchorMax">Anchor max.</param>
		/// <param name="pivot">Pivot.</param>
		private void SetUpLayoutRectTranformWithGivenValues (Vector2 anchorMin, Vector2 anchorMax, Vector2 pivot) {
			
			RectTransform r = hintPanelLayout.GetComponent<RectTransform> ();
			
			r.anchorMin = anchorMin;
			r.anchorMax = anchorMax;
			r.pivot = pivot;
			
			// by default at zero // it will be modified as needed
			r.anchoredPosition = Vector3.zero;
		}

		/// <summary>
		/// Aligns the content of the HintPanel from a given side.
		/// </summary>
		/// <param name="side">Side.</param>
		public void AlignFrom (HUDAnchorTypes side) {
		
			switch (side) {
			
			case HUDAnchorTypes.Right:
				AlignFromRight ();
				break;
			case HUDAnchorTypes.Left:
				AlignFromLeft ();
				break;
			case HUDAnchorTypes.Top:
				AlignFromTop ();
				break;
			case HUDAnchorTypes.Bottom:
				AlignFromBottom ();
				break;
			case HUDAnchorTypes.Center:
				AlignFromCenter ();
				break;
			}
		}

		/// <summary>
		/// Aligns the layout from the right. Makes it a horizontal layout if needed.
		/// </summary>
		public void AlignFromRight () {

			hintPanelLayout.GetComponent<HorizontalOrVerticalLayoutGroup> ().childAlignment = TextAnchor.MiddleRight;
		}

		/// <summary>
		/// Aligns the layout from the left. Makes it a horizontal layout if needed.
		/// </summary>
		public void AlignFromLeft () {
		
			hintPanelLayout.GetComponent<HorizontalOrVerticalLayoutGroup> ().childAlignment = TextAnchor.MiddleLeft;
		}

		/// <summary>
		/// Aligns the layout from the top. Makes it a vertical layout if needed.
		/// </summary>
		public void AlignFromTop () {
		
			hintPanelLayout.GetComponent<HorizontalOrVerticalLayoutGroup> ().childAlignment = TextAnchor.UpperCenter;
		}

		/// <summary>
		/// Aligns the layout from the bottom. Makes it a vertical layout if needed.
		/// </summary>
		public void AlignFromBottom () {
		
			hintPanelLayout.GetComponent<HorizontalOrVerticalLayoutGroup> ().childAlignment = TextAnchor.LowerCenter;
		}

		/// <summary>
		/// Alings the layout from the center.
		/// </summary>
		public void AlignFromCenter () {
		
			hintPanelLayout.GetComponent<HorizontalOrVerticalLayoutGroup> ().childAlignment = TextAnchor.MiddleCenter;
		}

		/// <summary>
		/// Layouts the content as.
		/// </summary>
		/// <param name="layout">Layout.</param>
		public void LayoutContentAs (HUDHintLayouts layout) {
		
			_currentLayout = layout;

			switch (_currentLayout) {
			
			case HUDHintLayouts.IconLeftTextRight:
				MakeHorizontalLayout();
				SetImageFirst ();
				hintUIText.alignment = TextAnchor.MiddleLeft;
				break;
			case HUDHintLayouts.TextLeftIconRight:
				MakeHorizontalLayout();
				SetTextFirst ();
				hintUIText.alignment = TextAnchor.MiddleRight;
				break;
			case HUDHintLayouts.IconAboveTextBelow:
				MakeVerticalLayout ();
				SetImageFirst ();
				hintUIText.alignment = TextAnchor.MiddleCenter;
				break;
			case HUDHintLayouts.TextAboveIconBelow:
				MakeVerticalLayout ();
				SetTextFirst ();
				hintUIText.alignment = TextAnchor.MiddleCenter;
				break;

			}
		}

		/// <summary>
		/// Makes the layout an horizontal one, replacing it if it was a vertical one.
		/// </summary>
		private void MakeHorizontalLayout () {
		
			if ((hintPanelLayout.GetComponent<HorizontalOrVerticalLayoutGroup> ()).GetType() == typeof(VerticalLayoutGroup)) {
				
				DestroyImmediate (hintPanelLayout.GetComponent<HorizontalOrVerticalLayoutGroup> ());
				hintPanelLayout.AddComponent<HorizontalLayoutGroup> ();

				hintPanelLayout.GetComponent<HorizontalLayoutGroup> ().childForceExpandWidth = false;
				hintPanelLayout.GetComponent<HorizontalLayoutGroup> ().childForceExpandHeight = false;
				hintPanelLayout.GetComponent<HorizontalLayoutGroup> ().spacing = 15;
				
			}
		}

		/// <summary>
		/// Makes the layout a vertical one, replacing it if it was a horizontal one.
		/// </summary>
		private void MakeVerticalLayout () {
		
			if ((hintPanelLayout.GetComponent<HorizontalOrVerticalLayoutGroup> ()).GetType() == typeof(HorizontalLayoutGroup)) {
				
				DestroyImmediate (hintPanelLayout.GetComponent<HorizontalOrVerticalLayoutGroup> ());
				hintPanelLayout.AddComponent<VerticalLayoutGroup> ();
				
				hintPanelLayout.GetComponent<VerticalLayoutGroup> ().childForceExpandWidth = false;
				hintPanelLayout.GetComponent<VerticalLayoutGroup> ().childForceExpandHeight = false;
				hintPanelLayout.GetComponent<VerticalLayoutGroup> ().spacing = 15;
				
			}
		}

		/// <summary>
		/// Sets the spacing between the image and the text of the hint.
		/// </summary>
		/// <param name="spacing">Spacing.</param>
		public void SetSpacing (float spacing) {
		
			hintPanelLayout.GetComponent<HorizontalOrVerticalLayoutGroup> ().spacing = spacing;
		}

		/// <summary>
		/// Sets the image as the first child of the layout.
		/// </summary>
		public void SetImageFirst () {

			hintUIImage.GetComponent<Transform> ().SetAsFirstSibling ();
		}

		/// <summary>
		/// Sets the text as the first child of the layout.
		/// </summary>
		public void SetTextFirst () {

			hintUIText.GetComponent<Transform> ().SetAsFirstSibling ();
		}

		# endregion

		# region show

		/// <summary>
		/// Shows this <see cref="CustomHUD.HUDHintPanel"/> on screen.
		/// </summary>
		public void Show () {

			Enable ();

			if (_animate) {
			
				if (_fadeIn) {

					hintUIImage.canvasRenderer.SetAlpha (0.0f);
					hintUIText.canvasRenderer.SetAlpha (0.0f);

					hintUIImage.CrossFadeAlpha (0.0f, 0.0f, false);
					hintUIText.CrossFadeAlpha (0.0f, 0.0f, false);

					hintUIImage.CrossFadeAlpha (1.0f, enterTime, false);
					hintUIText.CrossFadeAlpha (1.0f, enterTime, false);
				}

				if (_slideIn) {
					GetComponent<Animator> ().speed = 1.0f / _enterTime;
					
					switch (_anchor.type) {

					case HUDAnchorTypes.Left: GetComponent<Animator> ().SetTrigger (HUDConst.SLIDE_LEFT_IN_TRIGGER); break;
					case HUDAnchorTypes.Right: GetComponent<Animator> ().SetTrigger (HUDConst.SLIDE_RIGHT_IN_TRIGGER); break;
					case HUDAnchorTypes.Top: GetComponent<Animator> ().SetTrigger (HUDConst.SLIDE_TOP_IN_TRIGGER); break;
					case HUDAnchorTypes.Bottom: GetComponent<Animator> ().SetTrigger (HUDConst.SLIDE_BOTTOM_IN_TRIGGER); break;
					}

				}
			}
		}

		/// <summary>
		/// Hides this <see cref="CustomHUD.HUDHintPanel"/> on screen.
		/// </summary>
		public void Hide () {

			
			if (_animate) {
				
				if (_fadeOut) {

					hintUIImage.canvasRenderer.SetAlpha (1.0f);
					hintUIText.canvasRenderer.SetAlpha (1.0f);
					hintUIImage.CrossFadeAlpha (0.0f, _exitTime, false);
					hintUIText.CrossFadeAlpha (0.0f, _exitTime, false);
				}

				if (_slideOut) {

					GetComponent<Animator> ().speed = 1.0f / _exitTime;
					GetComponent<Animator> ().SetTrigger (HUDConst.SLIDE_OUT_TRIGGER);
				}

				if (_fadeOut || _slideOut) {

					Invoke ("Disable", _exitTime);
				}
			}
			else {

				Disable ();
			}

			_isCurrentlyShown = false;
		}

		/// <summary>
		/// Shows this <see cref="CustomHUD.HUDHintPanel"/> on the screen and automatically hides it after a given amount of time has passed.
		/// </summary>
		/// <param name="secondsToHide">The time in seconds between showing and hiding this <see cref="CustomHUD.HUDHintPanel"/>.</param>
		public void ShowAndHide (float secondsToHide) {

			Show ();
			Invoke ("Hide", secondsToHide + _enterTime);
		}

		# endregion
	}

}