﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace CustomHUD {

	public class HUDWarningPanel : HUDElement, IHUDWarningPanel, IHUDFadeInOutAnimated {
		
		# region attributes and properties
				
		/// <summary>
		/// The GameObject that holds the HorizontalLayout that will arrange the inner Image and the Text.
		/// </summary>
		[Tooltip("The GameObject that holds the HorizontalOrVerticalLayout that will arrange the Image and the Text.")]
		public GameObject warningPanelLayout;
				
		/*
			UI Elements
		 */

		/// <summary>
		/// The UI Image that holds the other UI elements of the warning, acting as a background.
		/// </summary>
		[Tooltip ("The UI Image that holds the other UI elements of the warning, acting as a background.")]
		public Image warningUIBackground;
		
		/// <summary>
		/// The UI Text that holds the text for the warning.
		/// </summary>
		[Tooltip ("The UI Text that holds the text for the warning.")]
		public Text warningUIText;
		
		/// <summary>
		/// The UI Image that holds the icon of the warning.
		/// </summary>
		[Tooltip ("The UI Image that holds the icon of the warning.")]
		public Image warningUIImage;

		public Sprite background {

			get { return warningUIBackground.sprite; }
			set { warningUIBackground.sprite = value; }
		}

		/// <summary>
		/// Gets or sets the text placed in the UI Text of this <see cref="CustomHUD.HUDWarningPanel"/>, representing the content of the hint.
		/// </summary>
		/// <value>The hint string.</value>
		public string text {
			
			get { return warningUIText.text; }
			set { warningUIText.text = value; }
		}
		
		/// <summary>
		/// Gets or sets the Color of the text of the warning.
		/// </summary>
		/// <value>The color of the text.</value>
		public Color textColor {
			
			get { return warningUIText.color; }
			set { warningUIText.color = value; }
		}
		
		/// <summary>
		/// Gets or sets the Sprite of the UI Image that serves as icon of this <see cref="CustomHUD.HUDWarningPanel"/>.
		/// </summary>
		/// <value>The hint icon.</value>
		public Sprite icon {
			
			get { return warningUIImage.sprite; } 
			set { warningUIImage.sprite = value; } 
		}
		
		/// <summary>
		/// Gets or sets the font of the UI Text of this <see cref="CustomHUD.HUDWarningPanel"/>.
		/// </summary>
		/// <value>The font.</value>
		public Font font {
			
			get { return warningUIText.font; }
			set	{ warningUIText.font = value; }
		}

		/// <summary>
		/// The size of the RectTransform of the GameObject holding both the layout and the background of this <see cref="CustomHUD.HUDWarningPanel"/>.
		/// </summary>
		/// <value>The size.</value>
		public Vector2 size {

			get { return warningUIBackground.GetComponent<RectTransform> ().sizeDelta; }
			set { warningUIBackground.GetComponent<RectTransform> ().sizeDelta = value; }
		}
		
		/// <summary>
		/// Whether this <see cref="CustomHUD.HUDWarningPanel"/> is currently shown on screen.
		/// </summary>
		private bool _isCurrentlyShown;
		
		/// <summary>
		/// Gets a value indicating whether this <see cref="CustomHUD.HUDWarningPanel"/> is shown.
		/// </summary>
		/// <value><c>true</c> if is shown; otherwise, <c>false</c>.</value>
		public bool isCurrentlyShown {
			
			get { return _isCurrentlyShown; } 
		}
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDWarningPanel"/> should start displayed on screen or not.
		/// </summary>
		[SerializeField]
		private bool _startShown;
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDWarningPanel"/> should start displayed on screen or not.
		/// </summary>
		[SerializeField]
		public bool startShown {
			
			get { return _startShown; }
			set { _startShown = value; }
		}
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDWarningPanel"/> will have some animations when being displayed.
		/// </summary>
		[SerializeField]
		private bool _animate;
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDWarningPanel"/> will have some animations when being displayed.
		/// </summary>
		[SerializeField]
		public bool animate {
			
			get { return _animate; }
			set { _animate = value; }
		}
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDWarningPanel"/> should have a fade-in effect when being shown or not, if <see cref="CustomHUD.HUDWarningPanel.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private bool _fadeIn;
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDWarningPanel"/> should have a fade-in effect when being shown or not, if <see cref="CustomHUD.HUDWarningPanel.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		public bool fadeIn {
			
			get { return _fadeIn; }
			set { _fadeIn = value; }
		}
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDWarningPanel"/> should have a fade-out effect when being hidden or not, if <see cref="CustomHUD.HUDWarningPanel.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private bool _fadeOut;
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDWarningPanel"/> should have a fade-out effect when being hidden or not, if <see cref="CustomHUD.HUDWarningPanel.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		public bool fadeOut {
			
			get { return _fadeOut; }
			set { _fadeOut = value; }
		}
		
		/// <summary>
		/// The it takes for the fade/hide-in animations of this <see cref="CustomHUD.HUDWarningPanel"/> to be completed, if <see cref="CustomHUD.HUDWarningPanel.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private float _enterTime;
		
		/// <summary>
		/// The it takes for the fade/hide-in animations of this <see cref="CustomHUD.HUDWarningPanel"/> to be completed, if <see cref="CustomHUD.HUDWarningPanel.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		public float enterTime {
			
			get { return _enterTime; }
			set { _enterTime = value; }
		}
		
		/// <summary>
		/// The it takes for the fade/hide-out animations of this <see cref="CustomHUD.HUDWarningPanel"/> to be completed, if <see cref="CustomHUD.HUDWarningPanel.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private float _exitTime;
		
		/// <summary>
		[SerializeField]
		/// The it takes for the fade/hide-out animations of this <see cref="CustomHUD.HUDWarningPanel"/> to be completed, if <see cref="CustomHUD.HUDWarningPanel.animate"/> is set to true.
		/// </summary>
		public float exitTime {
			
			get { return _exitTime; }
			set { _exitTime = value; }
		}
	
		# endregion

		new void Awake () {

			base.Awake ();
			_type = HUDElementTypes.WarningPanel;
		}

		void Start () {

		}


		# region methods

		/// <summary>
		/// Subscribes this <see cref="CustomHUD.HUDWarningPanel"/> to the <see cref="CustomHUD.HUDController in the scene."/>
		/// </summary>
		protected override void SubscribeToController () {
			
			_controller.SubscribeAs (this);
			CheckStartShown ();
		}

		/// <summary>
		/// Checks if this <see cref="CustomHUD.HUDWarningPanel"/> should start shown or not, and shows or hides it accordingly.
		/// </summary>
		private void CheckStartShown () {
		
			if (!_startShown) {
				
				warningUIBackground	.CrossFadeAlpha (0.0f, 0.0f, false);
				warningUIImage		.CrossFadeAlpha (0.0f, 0.0f, false);
				warningUIText		.CrossFadeAlpha (0.0f, 0.0f, false);

				Disable ();
			}
			else {

			}

			_isCurrentlyShown = _startShown;
			gameObject.SetActive (_startShown);
		}

		
		/// <summary>
		/// Enables the GameObject holding this <see cref="CustomHUD.HUDWarningPanel"/>.
		/// </summary>
		public void Enable () {
			
			this.gameObject.SetActive (true);
			
			_isCurrentlyShown = true;
		}
		
		/// <summary>
		/// Disables the GameObject holding this <see cref="CustomHUD.HUDWarningPanel"/>.
		/// </summary>
		public void Disable () {
			
			this.gameObject.SetActive (false);
			
			_isCurrentlyShown = false;
		}

		/// <summary>
		/// Sets the values of this <see cref="CustomHUD.HUDWarningPanel"/>. These values can be empty.
		/// </summary>
		/// <param name="warningText">Warning text.</param>
		/// <param name="warningBackground">Warning background sprite.</param>
		/// <param name="warningIcon">Warning icon sprite.</param>
		public void SetValues (string warningText, Sprite warningBackground, Sprite warningIcon) {
			
			warningUIText.text = warningText;
			warningUIBackground.sprite = warningBackground;
			warningUIImage.sprite = warningIcon;

			// add a check for null
		}

		/// <summary>
		/// Sets the background of this <see cref="CustomHUD.HUDWarningPanel"/>.
		/// </summary>
		/// <param name="newBackground">New background.</param>
		public void SetBackground (Sprite newBackground) {
		
			background = newBackground;
		}

		/// <summary>
		/// The size of the RectTransform of the GameObject holding both the layout and the background of this <see cref="CustomHUD.HUDWarningPanel"/>.
		/// </summary>
		/// <param name="size">Size.</param>
		public void SetSize (Vector2 size) {
		
			this.size = size;
		} 

		/// <summary>
		/// The size of the RectTransform of the GameObject holding both the layout and the background of this <see cref="CustomHUD.HUDWarningPanel"/>.
		/// </summary>
		/// <param name="sizeX">Size x.</param>
		/// <param name="sizeY">Size y.</param>
		public void SetSize (float sizeX, float sizeY) {
		
			SetSize (new Vector2 (sizeX, sizeY));
		}

		/// <summary>
		/// Sets the icon of this <see cref="CustomHUD.HUDWarningPanel"/>.
		/// </summary>
		/// <param name="newIcon">The new icon for the warning.</param>
		public void SetIcon (Sprite newIcon) {
			
			icon = newIcon;
		}

		/// <summary>
		/// Sets the size of the warning icon by adjusting the preferred width and height of the LayoutElement component of this <see cref="CustomHUD.HUDWarningPanel"/>.
		/// </summary>
		/// <param name="size">Size.</param>
		public void SetIconSize (Vector2 size) {
		
			warningUIImage.GetComponent<LayoutElement> ().preferredWidth = size.x;
			warningUIImage.GetComponent<LayoutElement> ().preferredHeight = size.y;
		}
		
		/// <summary>
		/// Sets the text of this <see cref="CustomHUD.HUDWarningPanel"/>.
		/// </summary>
		/// <param name="newText">The new text for the warning.</param>
		public void SetText (string newText) {
			
			text = newText;
		}
		
		/// <summary>
		/// Sets the font of the text of this <see cref="CustomHUD.HUDWarningPanel"/>.
		/// </summary>
		/// <param name="font">Font.</param>
		public void SetFont (Font font) {
			
			warningUIText.font = font;
		}
		
		/// <summary>
		/// Sets the size of the font of the text of this <see cref="CustomHUD.HUDWarningPanel"/>.
		/// </summary>
		/// <param name="size">Size.</param>
		public void SetFontSize (int size) {
			
			warningUIText.fontSize = size;
		}
		
		/// <summary>
		/// Sets the color of the UI Text of this HUDWarningPanel to a given new Color.
		/// </summary>
		/// <param name="newColor">The new Color.</param>
		public void SetTextColor (Color newColor) {
			
			warningUIText.color = newColor;
		}
		
		/// <summary>
		/// Disables the icon Image of this <see cref="CustomHUD.HUDWarningPanel"/>, setting it inactive.
		/// </summary>
		public void DisableIcon () {
			
			warningUIImage.gameObject.SetActive (false);
		}
		
		/// <summary>
		/// Enables the icon Image of this <see cref="CustomHUD.HUDWarningPanel"/>, setting it active.
		/// </summary>
		public void EnableIcon () {
			
			warningUIImage.gameObject.SetActive (true);
		}
		
		/// <summary>
		/// Disables the Text of this <see cref="CustomHUD.HUDWarningPanel"/>, setting it inactive.
		/// </summary>
		public void DisableText () {
			
			warningUIText.gameObject.SetActive (false);
		}
		
		/// <summary>
		/// Enables the Text of this <see cref="CustomHUD.HUDWarningPanel"/>, setting it active.
		/// </summary>
		public void EnableText () {
			
			warningUIText.gameObject.SetActive (true);
		}

		/// <summary>
		/// Sets the spacing between the image and the text of the hint.
		/// </summary>
		/// <param name="spacing">Spacing.</param>
		public void SetSpacing (float spacing) {
			
			warningPanelLayout.GetComponent<HorizontalOrVerticalLayoutGroup> ().spacing = spacing;
		}
		
		/// <summary>
		/// Sets the image as the first child of the layout.
		/// </summary>
		public void SetImageFirst () {
			
			warningUIImage.GetComponent<Transform> ().SetAsFirstSibling ();
		}
		
		/// <summary>
		/// Sets the text as the first child of the layout.
		/// </summary>
		public void SetTextFirst () {
		
			warningUIText.GetComponent<Transform> ().SetAsFirstSibling ();
		}

		/// <summary>
		/// Adds a Shadow effect component to the UI Text of this <see cref="CustomHUD.HUDWarningPanel"/> of the given Color.
		/// </summary>
		/// <param name="color">Color.</param>
		public void AddTextShadow (Color color) {
			
			warningUIText.gameObject.AddComponent<Shadow> ().effectColor = color;
		}
		
		/// <summary>
		/// Sets the color of the Shadow effect component of the UI Text of this <see cref="CustomHUD.HUDWarningPanel"/> of the given Color. Creates it if the component doesn't exist.
		/// </summary>
		/// <param name="color">Color.</param>
		public void SetTextShadow (Color color) {
			
			
			if (warningUIText.GetComponent<Shadow> () != null) {
				
				warningUIText.GetComponent<Shadow> ().effectColor = color;
			}
			else {
				
				AddTextShadow (color);
			}
		}
		
		/// <summary>
		/// Removes the Shadow effect component of the UI Text of this <see cref="CustomHUD.HUDWarningPanel"/>.
		/// </summary>
		public void RemoveTextShadow () {
			
			DestroyImmediate (warningUIText.GetComponent<Shadow> ());
		}
		
		/// <summary>
		/// Adds a Outline effect component to the UI Text of this <see cref="CustomHUD.HUDWarningPanel"/> of the given Color.
		/// </summary>
		/// <param name="color">Color.</param>
		public void AddTextOutline (Color color) {
			
			warningUIText.gameObject.AddComponent<Outline> ().effectColor = color;
		}
		
		/// <summary>
		/// Sets the color of the Outline effect component of the UI Text of this <see cref="CustomHUD.HUDWarningPanel"/> of the given Color. Creates it if the component doesn't exist.
		/// </summary>
		/// <param name="color">Color.</param>
		public void SetTextOutline (Color color) {
			
			
			if (warningUIText.GetComponent<Outline> () != null) {
				
				warningUIText.GetComponent<Outline> ().effectColor = color;
			}
			else {
				
				AddTextOutline (color);
			}
		}
		
		/// <summary>
		/// Removes the Outline effect component of the UI Text of this <see cref="CustomHUD.HUDWarningPanel"/>.
		/// </summary>
		public void RemoveTextOutline () {
			
			DestroyImmediate (warningUIText.GetComponent<Outline> ());
		}

		/// <summary>
		/// Anchors the layout of the HUDHintPanel from a given place.
		/// </summary>
		/// <param name="from">From.</param>
		public void AnchorLayoutFrom ( HUDAnchorTypes from ) {
			
			switch (from) {
				
			case HUDAnchorTypes.Top:
				AnchorLayoutFromTop ();
				break;
			case HUDAnchorTypes.Bottom:
				AnchorLayoutFromBottom ();
				break;
			case HUDAnchorTypes.Left:
				AnchorLayoutFromLeft ();
				break;
			case HUDAnchorTypes.Right:
				AnchorLayoutFromRight ();
				break;
			case HUDAnchorTypes.Center:
				AnchorLayoutFromCenter ();
				break;
			}
		}
		
		/// <summary>
		/// Sets up the layout RectTransform from the top.
		/// </summary>
		public void AnchorLayoutFromTop () {
			
			SetUpLayoutRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MAX_VALUE),
			                                        new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MAX_VALUE),
			                                        new Vector2 (HUDConst.PIVOT_X_FOR_TOP_ANCHOR, HUDConst.PIVOT_Y_FOR_TOP_ANCHOR)
			                                        );
		}
		
		/// <summary>
		/// Sets up the layout RectTransform from the bottom.
		/// </summary>
		public void AnchorLayoutFromBottom () {
			
			SetUpLayoutRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MIN_VALUE),
			                                        new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MIN_VALUE),
			                                        new Vector2 (HUDConst.PIVOT_X_FOR_BOTTOM_ANCHOR, HUDConst.PIVOT_Y_FOR_BOTTOM_ANCHOR)
			                                        );
		}
		
		/// <summary>
		/// Sets up the layout RectTransform from the left.
		/// </summary>
		public void AnchorLayoutFromLeft () {
			
			SetUpLayoutRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MIN_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                                        new Vector2 (HUDConst.ANCHOR_MIN_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                                        new Vector2 (HUDConst.PIVOT_X_FOR_LEFT_ANCHOR, HUDConst.PIVOT_Y_FOR_LEFT_ANCHOR)
			                                        );
		}
		
		/// <summary>
		/// Sets up the layout RectTransform from the right.
		/// </summary>
		public void AnchorLayoutFromRight () {
			
			SetUpLayoutRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MAX_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                                        new Vector2 (HUDConst.ANCHOR_MAX_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                                        new Vector2 (HUDConst.PIVOT_X_FOR_RIGHT_ANCHOR, HUDConst.PIVOT_Y_FOR_RIGHT_ANCHOR)
			                                        );
		}
		
		/// <summary>
		/// Sets up the layout RectTransform from the center.
		/// </summary>
		public void AnchorLayoutFromCenter () {
			
			SetUpLayoutRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                                        new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                                        new Vector2 (HUDConst.PIVOT_X_FOR_CENTER_ANCHOR, HUDConst.PIVOT_Y_FOR_CENTER_ANCHOR)
			                                        );
		}
		
		/// <summary>
		/// Sets up the layout rect tranform with given values.
		/// </summary>
		/// <param name="anchorMin">Anchor minimum.</param>
		/// <param name="anchorMax">Anchor max.</param>
		/// <param name="pivot">Pivot.</param>
		private void SetUpLayoutRectTranformWithGivenValues (Vector2 anchorMin, Vector2 anchorMax, Vector2 pivot) {
			
			RectTransform r = warningPanelLayout.GetComponent<RectTransform> ();
			
			r.anchorMin = anchorMin;
			r.anchorMax = anchorMax;
			r.pivot = pivot;
			
			// by default at zero // it will be modified as needed
			r.anchoredPosition = Vector3.zero;
		}

		# endregion


		# region show
		
		/// <summary>
		/// Shows this <see cref="CustomHUD.HUDWarningPanel"/> on screen.
		/// </summary>
		public void Show () {
			
			Enable ();
			
			if (_animate) {
				
				if (_fadeIn) {

					warningUIBackground	.CrossFadeAlpha (0.0f, 0.0f, false);
					warningUIImage		.CrossFadeAlpha (0.0f, 0.0f, false);
					warningUIText		.CrossFadeAlpha (0.0f, 0.0f, false);

					warningUIBackground	.CrossFadeAlpha (1.0f, _enterTime, false);
					warningUIImage		.CrossFadeAlpha (1.0f, _enterTime, false);
					warningUIText		.CrossFadeAlpha (1.0f, _enterTime, false);
				}
			}
		}
		
		/// <summary>
		/// Hides this <see cref="CustomHUD.HUDWarningPanel"/> on screen.
		/// </summary>
		public void Hide () {
			
			
			if (_animate) {
				
				if (_fadeOut) {
					
					warningUIBackground	.CrossFadeAlpha (0.0f, _exitTime, false);
					warningUIImage		.CrossFadeAlpha (0.0f, _exitTime, false);
					warningUIText		.CrossFadeAlpha (0.0f, _exitTime, false);

					Invoke ("Disable", _exitTime);
				}
			}
			else {
				
				Disable ();
			}
			
			_isCurrentlyShown = false;
		}
		
		/// <summary>
		/// Shows this <see cref="CustomHUD.HUDWarningPanel"/> on the screen and automatically hides it after a given amount of time has passed.
		/// </summary>
		/// <param name="secondsToHide">The time in seconds between showing and hiding this <see cref="CustomHUD.HUDWarningPanel"/>.</param>
		public void ShowAndHide (float secondsToHide) {
			
			Show ();
			Invoke ("Hide", secondsToHide + _enterTime);
		}
		
		# endregion
	}






}
