﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace CustomHUD {

	public class HUDProgressBar : HUDElement, IHUDProgressBar, IHUDFadeInOutAnimated, IHUDSlideInOutAnimated {

		# region attributes and properties

		/// <summary>
		/// The type of progress bar (i.e. the function it performs) that this <see cref="CustomHUD.HUDProgressBar"/> will be subscribed as to the HUDController.
		/// </summary>
		public HUDProgressBarTypes subscribeAs {

			get { return _subscribeAs; }
		}

		/// <summary>
		/// The type of progress bar (i.e. the function it performs) that this <see cref="CustomHUD.HUDProgressBar"/> will be subscribed as to the HUDController.
		/// </summary>
		[SerializeField]
		public HUDProgressBarTypes _subscribeAs;

		/// <summary>
		/// Gets or sets the current value to display by the progress bar. Clamped between minValue and maxValue.
		/// </summary>
		/// <value>The current value.</value>
		public float value {
			
			get{ return progressUISlider.value; }
			set{ SetValue (value); }
		}

		/// <summary>
		/// Gets or sets the integer value to display by the progress bar. Clamped between minValue and maxValue.
		/// </summary>
		/// <value>The int value.</value>
		public int intValue {

			get { return (int) value; }
			set { SetIntValue (value); }
		}

		/// <summary>
		/// Gets or sets the maximum value the progress bar can represent. At this value, the bar will be completely full.
		/// </summary>
		/// <value>The max value.</value>
		public float maxValue {
			
			get{ return progressUISlider.maxValue; }
			set{ SetMaxValue (value); }
		}
		/// <summary>
		/// Gets or sets the integer maximum value the progress bar can represent. At this value, the bar will be completely full.
		/// </summary>
		/// <value>The int max value.</value>
		public int intMaxValue {

			get { return (int) maxValue; }
			set { SetMaxValue (value); }
		}

		/// <summary>
		/// Gets or sets the minimum value this progress bar can represent. At this value, the bar will be completely empty.
		/// </summary>
		/// <value>The minimum value.</value>
		public float minValue {
			
			get{ return progressUISlider.minValue; }
			set{ SetMinValue (value); }
		}

		/// <summary>
		/// Gets or sets the integer minimum value this progress bar can represent. At this value, the bar will be completely empty.
		/// </summary>
		/// <value>The int minimum value.</value>
		public int intMinValue {

			get { return (int) minValue; }
			set { SetIntMinValue (value); }
		}
		
		/// <summary>
		/// Gets or sets the current value percentually clamped by [0.0f, 1.0f].
		/// </summary>
		/// <value>The value percent.</value>
		public float normalizedValue {
			
			get{ return 1.0f; }
			set{ Debug.Log (value); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="CustomHUD.HUDProgressBar"/> use integer steps.
		/// </summary>
		/// <value><c>true</c> if use integer steps; otherwise, <c>false</c>.</value>
		public bool useIntegerSteps {

			get { return progressUISlider.wholeNumbers; }
			set { progressUISlider.wholeNumbers = value; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="CustomHUD.HUDProgressBar"/> use float steps.
		/// </summary>
		/// <value><c>true</c> if use float steps; otherwise, <c>false</c>.</value>
		public bool useFloatSteps {

			get { return !useIntegerSteps; }
			set { useIntegerSteps = !value; }
		}

		/// <summary>
		/// The ToString float format for the value display text.
		/// </summary>
		private string _toStringFloatFormat = "0.00";

		/// <summary>
		/// Gets or sets the ToString float format for the value display text.
		/// </summary>
		/// <value>To string float format.</value>
		public string toStringFloatFormat {

			get { return _toStringFloatFormat; }
			set { _toStringFloatFormat = value; }
		}

		# endregion

		# region show attributes

		
		/// <summary>
		/// Whether this <see cref="CustomHUD.HUDProgressBar"/> is currently shown on screen.
		/// </summary>
		private bool _isCurrentlyShown;
		
		/// <summary>
		/// Gets a value indicating whether this <see cref="CustomHUD.HUDProgressBar"/> is shown.
		/// </summary>
		/// <value><c>true</c> if is shown; otherwise, <c>false</c>.</value>
		public bool isCurrentlyShown {
			
			get { return _isCurrentlyShown; } 
		}
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDProgressBar"/> should start displayed on screen or not.
		/// </summary>
		[SerializeField]
		private bool _startShown;
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDProgressBar"/> should start displayed on screen or not.
		/// </summary>
		[SerializeField]
		public bool startShown {
			
			get { return _startShown; }
			set { _startShown = value; }
		}
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDProgressBar"/> will have some animations when being displayed.
		/// </summary>
		[SerializeField]
		private bool _animate;
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDProgressBar"/> will have some animations when being displayed.
		/// </summary>
		[SerializeField]
		public bool animate {
			
			get { return _animate; }
			set { _animate = value; }
		}
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDProgressBar"/> should have a fade-in effect when being shown or not, if <see cref="CustomHUD.HUDProgressBar.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private bool _fadeIn;
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDProgressBar"/> should have a fade-in effect when being shown or not, if <see cref="CustomHUD.HUDProgressBar.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		public bool fadeIn {
			
			get { return _fadeIn; }
			set { _fadeIn = value; }
		}
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDProgressBar"/> should have a fade-out effect when being hidden or not, if <see cref="CustomHUD.HUDProgressBar.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private bool _fadeOut;
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDProgressBar"/> should have a fade-out effect when being hidden or not, if <see cref="CustomHUD.HUDProgressBar.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		public bool fadeOut {
			
			get { return _fadeOut; }
			set { _fadeOut = value; }
		}
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDProgressBar"/> should have a slide-in effect when being shown or not, if <see cref="CustomHUD.HUDProgressBar.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private bool _slideIn;
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDProgressBar"/> should have a slide-in effect when being shown or not, if <see cref="CustomHUD.HUDProgressBar.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		public bool slideIn {
			
			get { return _slideIn; }
			set { _slideIn = value; }
		}
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDProgressBar"/> should have a slide-out effect when being hidden or not, if <see cref="CustomHUD.HUDProgressBar.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private bool _slideOut;
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDProgressBar"/> should have a slide-out effect when being hidden or not, if <see cref="CustomHUD.HUDProgressBar.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		public bool slideOut {
			
			get { return _slideOut; }
			set { _slideOut = value; }
		}
		
		/// <summary>
		/// The it takes for the fade/hide-in animations of this <see cref="CustomHUD.HUDProgressBar"/> to be completed, if <see cref="CustomHUD.HUDProgressBar.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private float _enterTime;
		
		/// <summary>
		/// The it takes for the fade/hide-in animations of this <see cref="CustomHUD.HUDProgressBar"/> to be completed, if <see cref="CustomHUD.HUDProgressBar.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		public float enterTime {
			
			get { return _enterTime; }
			set { _enterTime = value; }
		}
		
		/// <summary>
		/// The it takes for the fade/hide-out animations of this <see cref="CustomHUD.HUDProgressBar"/> to be completed, if <see cref="CustomHUD.HUDProgressBar.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private float _exitTime;
		
		/// <summary>
		[SerializeField]
		/// The it takes for the fade/hide-out animations of this <see cref="CustomHUD.HUDProgressBar"/> to be completed, if <see cref="CustomHUD.HUDProgressBar.animate"/> is set to true.
		/// </summary>
		public float exitTime {
			
			get { return _exitTime; }
			set { _exitTime = value; }
		}

		# endregion

		# region UI elements attributes

		public HorizontalLayoutGroup baseLayout;

		public Slider progressUISlider;

		public Canvas valueHolderCanvas;

		public Image valueHolderImage;

		public Text valueText;

		public Sprite fillSprite  {

			get { return progressUISlider.fillRect.GetComponent<Image>().sprite; }
			set { progressUISlider.fillRect.GetComponent<Image>().sprite = value; }
		}

		public Sprite backSprite {

			get { return progressUISlider.GetComponent<Image>().sprite; }
			set { progressUISlider.GetComponent<Image>().sprite = value; }
		}

		public float barHeight {

			get { return progressUISlider.GetComponent<LayoutElement> ().minHeight; }
			set { progressUISlider.GetComponent<LayoutElement> ().minHeight = value; }
		}

		public float barLenght {

			get { return baseLayout.GetComponent<RectTransform> ().sizeDelta.x; }
			set { SetLenght (value); } 
		}

		# endregion

		# region unity 

		new void Awake () {

			base.Awake ();
			_type = HUDElementTypes.ProgressBar;
		}

		# endregion

		# region methods

		/// <summary>
		/// Subscribes this <see cref="CustomHUD.HUDProgressBar"/> to the <see cref="CustomHUD.HUDController in the scene."/>
		/// </summary>
		protected override void SubscribeToController () {

			Debug.Log ("subscribinggggggg");
			_controller.SubscribeAs (this);
			CheckStartShown ();

		}

		/// <summary>
		/// Enables the GameObject holding this <see cref="CustomHUD.HUDProgressBar"/>.
		/// </summary>
		public void Enable () {
			
			this.gameObject.SetActive (true);
			
			_isCurrentlyShown = true;
		}
		
		/// <summary>
		/// Disables the GameObject holding this <see cref="CustomHUD.HUDProgressBar"/>.
		/// </summary>
		public void Disable () {
			
			this.gameObject.SetActive (false);
			
			_isCurrentlyShown = false;
		}

		/// <summary>
		/// Sets the role of this <see cref="CustomHUD.HUDProgressBar"/>, as which it will be subscribed to the <see cref="CustomHUD.HUDController"/>.
		/// </summary>
		/// <param name="role">Role.</param>
		public void SetRole (HUDProgressBarTypes role) {
			
			_subscribeAs = role;
		}

		public float GetValue (){

			return value;
		}

		public int GetIntValue () {

			return intValue;
		}
		
		public float GetMaxValue (){

			return maxValue;
		}

		public int GetIntMaxValue (){
			
			return intMaxValue;
		}

		public float GetMinValue () {

			return minValue;
		}
		
		public int GetIntMinValue () {

			return intMinValue;
		}
		
		public float GetNormalizedValue () {

			return progressUISlider.normalizedValue;
		}
		
		public void SetValue (float newValue) {

			newValue = Mathf.Clamp (newValue, minValue, maxValue);

			progressUISlider.value = newValue;

			if (valueText != null) {
			
				if (useFloatSteps) { valueText.text = value.ToString (toStringFloatFormat); }
				else {valueText.text = ((int)value).ToString (); }
			}
		}
		
		public void SetIntValue (int newValue) {
			
			newValue = Mathf.Clamp (newValue, intMinValue, intMaxValue);

			progressUISlider.value = (float) newValue;
			if (valueText != null) {
			
				if (useIntegerSteps) { valueText.text = value.ToString (); }
				else { valueText.text = ((float)value).ToString (toStringFloatFormat); }
			}
		}
		
		public void SetMaxValue (float newValue) {
			
			progressUISlider.maxValue = newValue;
		}
		
		public void SetMaxValue (int newValue) {
			
			progressUISlider.maxValue = (float)newValue;
		}
		
		public void SetMinValue (float newValue) {

			progressUISlider.minValue = newValue;
		}
		
		public void SetIntMinValue (float newValue) {
			
			progressUISlider.minValue = (int) newValue;
		}
		
		public void SetValuePercent (float newValue) {

			progressUISlider.normalizedValue = Mathf.Clamp (newValue, 0.0f, 1.0f);
		}

		public void LayoutContentAs (HUDProgressBarLayouts newLayout) {

			switch (newLayout) {
			
			case HUDProgressBarLayouts.NumberLeftBarRight : SetNumberFirst (); break;
			case HUDProgressBarLayouts.BarLeftNumberRight : SetBarFirst(); break;
			}
		}

		public void SetNumberFirst () {

			valueHolderCanvas.GetComponent<Transform> ().SetAsFirstSibling ();
		}

		public void SetBarFirst () {
		
			progressUISlider.GetComponent<Transform> ().SetAsFirstSibling ();
		}

		public void AlignContentTo (HUDProgressBarAlignment newAlignment) {

			switch (newAlignment) {
			
			case HUDProgressBarAlignment.Middle : 	AlignContentAtMiddle (); 	break;
			case HUDProgressBarAlignment.Up : 		AlignContentAtTop (); 		break;
			case HUDProgressBarAlignment.Down : 	AlignContentAtBottom ();	break;
			}
		}

		public void AlignContentAtMiddle () {

			baseLayout.childAlignment = TextAnchor.MiddleCenter;
		}

		public void AlignContentAtTop () {

			baseLayout.childAlignment = TextAnchor.UpperCenter;
		}

		public void AlignContentAtBottom () {
			baseLayout.childAlignment = TextAnchor.LowerCenter;

		}

		# endregion

		# region outline and shadow

		/// <summary>
		/// Adds a Shadow effect component to the UI Text of this <see cref="CustomHUD.HUDProgressBar"/> of the given Color.
		/// </summary>
		/// <param name="color">Color.</param>
		public void AddTextShadow (Color color) {
			
			valueText.gameObject.AddComponent<Shadow> ().effectColor = color;
		}
		
		/// <summary>
		/// Sets the color of the Shadow effect component of the UI Text of this <see cref="CustomHUD.HUDProgressBar"/> of the given Color. Creates it if the component doesn't exist.
		/// </summary>
		/// <param name="color">Color.</param>
		public void SetTextShadow (Color color) {
			
			
			if (valueText.GetComponent<Shadow> () != null) {
				
				valueText.GetComponent<Shadow> ().effectColor = color;
			}
			else {
				
				AddTextShadow (color);
			}
		}
		
		/// <summary>
		/// Removes the Shadow effect component of the UI Text of this <see cref="CustomHUD.HUDProgressBar"/>.
		/// </summary>
		public void RemoveTextShadow () {
			
			DestroyImmediate (valueText.GetComponent<Shadow> ());
		}
		
		/// <summary>
		/// Adds a Outline effect component to the UI Text of this <see cref="CustomHUD.HUDProgressBar"/> of the given Color.
		/// </summary>
		/// <param name="color">Color.</param>
		public void AddTextOutline (Color color) {
			
			valueText.gameObject.AddComponent<Outline> ().effectColor = color;
		}
		
		/// <summary>
		/// Sets the color of the Outline effect component of the UI Text of this <see cref="CustomHUD.HUDProgressBar"/> of the given Color. Creates it if the component doesn't exist.
		/// </summary>
		/// <param name="color">Color.</param>
		public void SetTextOutline (Color color) {
			
			
			if (valueText.GetComponent<Outline> () != null) {
				
				valueText.GetComponent<Outline> ().effectColor = color;
			}
			else {
				
				AddTextOutline (color);
			}
		}
		
		/// <summary>
		/// Removes the Outline effect component of the UI Text of this <see cref="CustomHUD.HUDProgressBar"/>.
		/// </summary>
		public void RemoveTextOutline () {
			
			DestroyImmediate (valueText.GetComponent<Outline> ());
		}

		# endregion

		# region layout setting

		/// <summary>
		/// Sets the lenght of this <see cref="CustomHUD.HUDProgressBar"/> by adjusting the width of the HorizontalLayoutGroup within it.
		/// </summary>
		/// <param name="newLenght">New lenght.</param>
		public void SetLenght (float newLenght) {
		
			baseLayout.GetComponent<RectTransform> ().sizeDelta = new Vector2 (newLenght, baseLayout.GetComponent<RectTransform> ().sizeDelta.y);
		}

		/// <summary>
		/// Anchors the layout of the HUDHintPanel from a given place.
		/// </summary>
		/// <param name="from">From.</param>
		public void AnchorLayoutFrom ( HUDAnchorTypes from ) {
			
			switch (from) {
				
			case HUDAnchorTypes.Top:
				AnchorLayoutFromTop ();
				break;
			case HUDAnchorTypes.Bottom:
				AnchorLayoutFromBottom ();
				break;
			case HUDAnchorTypes.Left:
				AnchorLayoutFromLeft ();
				break;
			case HUDAnchorTypes.Right:
				AnchorLayoutFromRight ();
				break;
			case HUDAnchorTypes.Center:
				AnchorLayoutFromCenter ();
				break;
			}
		}
		
		/// <summary>
		/// Sets up the layout RectTransform from the top.
		/// </summary>
		public void AnchorLayoutFromTop () {
			
			SetUpLayoutRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MAX_VALUE),
			                                        new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MAX_VALUE),
			                                        new Vector2 (HUDConst.PIVOT_X_FOR_TOP_ANCHOR, HUDConst.PIVOT_Y_FOR_TOP_ANCHOR)
			                                        );
		}
		
		/// <summary>
		/// Sets up the layout RectTransform from the bottom.
		/// </summary>
		public void AnchorLayoutFromBottom () {
			
			SetUpLayoutRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MIN_VALUE),
			                                        new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MIN_VALUE),
			                                        new Vector2 (HUDConst.PIVOT_X_FOR_BOTTOM_ANCHOR, HUDConst.PIVOT_Y_FOR_BOTTOM_ANCHOR)
			                                        );
		}
		
		/// <summary>
		/// Sets up the layout RectTransform from the left.
		/// </summary>
		public void AnchorLayoutFromLeft () {
			
			SetUpLayoutRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MIN_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                                        new Vector2 (HUDConst.ANCHOR_MIN_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                                        new Vector2 (HUDConst.PIVOT_X_FOR_LEFT_ANCHOR, HUDConst.PIVOT_Y_FOR_LEFT_ANCHOR)
			                                        );
		}
		
		/// <summary>
		/// Sets up the layout RectTransform from the right.
		/// </summary>
		public void AnchorLayoutFromRight () {
			
			SetUpLayoutRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MAX_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                                        new Vector2 (HUDConst.ANCHOR_MAX_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                                        new Vector2 (HUDConst.PIVOT_X_FOR_RIGHT_ANCHOR, HUDConst.PIVOT_Y_FOR_RIGHT_ANCHOR)
			                                        );
		}
		
		/// <summary>
		/// Sets up the layout RectTransform from the center.
		/// </summary>
		public void AnchorLayoutFromCenter () {
			
			SetUpLayoutRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                                        new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                                        new Vector2 (HUDConst.PIVOT_X_FOR_CENTER_ANCHOR, HUDConst.PIVOT_Y_FOR_CENTER_ANCHOR)
			                                        );
		}
		
		/// <summary>
		/// Sets up the layout rect tranform with given values.
		/// </summary>
		/// <param name="anchorMin">Anchor minimum.</param>
		/// <param name="anchorMax">Anchor max.</param>
		/// <param name="pivot">Pivot.</param>
		private void SetUpLayoutRectTranformWithGivenValues (Vector2 anchorMin, Vector2 anchorMax, Vector2 pivot) {
			
			RectTransform r = baseLayout.GetComponent<RectTransform> ();
			
			r.anchorMin = anchorMin;
			r.anchorMax = anchorMax;
			r.pivot = pivot;
			
			// by default at zero // it will be modified as needed
			r.anchoredPosition = Vector3.zero;
		}

		# endregion

		# region show

		/// <summary>
		/// Checks if this <see cref="CustomHUD.HUDHintPanel"/> should start shown or not, and shows or hides it accordingly.
		/// </summary>
		private void CheckStartShown () {
			
			if (!_startShown) {
				
				progressUISlider.fillRect.GetComponent<Image> ().CrossFadeAlpha (0.0f, 0.0f, true);
				progressUISlider.GetComponent<Image> (). CrossFadeAlpha (0.0f, 0.0f, true);

				if (valueHolderImage != null) { valueHolderImage.CrossFadeAlpha (0.0f, 0.0f, true); } 	
				if (valueText != null) { valueText.CrossFadeAlpha (0.0f, 0.0f, true); } 	

				Disable ();
			}
			else {
				
			}
			
			_isCurrentlyShown = _startShown;
			gameObject.SetActive (_startShown);
		}

		
		/// <summary>
		/// Shows this <see cref="CustomHUD.HUDHintPanel"/> on screen.
		/// </summary>
		public void Show () {
			
			Enable ();
			
			if (_animate) {
				
				if (_fadeIn) {

					// set hidden
					progressUISlider.fillRect.GetComponent<Image> ().CrossFadeAlpha (0.0f, 0.0f, false);
					progressUISlider.GetComponent<Image> (). CrossFadeAlpha (0.0f, 0.0f, false);
					
					if (valueHolderImage != null) { valueHolderImage.CrossFadeAlpha (0.0f, 0.0f, false); } 	
					if (valueText != null) { valueText.CrossFadeAlpha (0.0f, 0.0f, false); } 

					// then display
					progressUISlider.fillRect.GetComponent<Image> ().CrossFadeAlpha (1.0f, _enterTime, false);
					progressUISlider.GetComponent<Image> (). CrossFadeAlpha (1.0f, _enterTime, false);
					
					if (valueHolderImage != null) { valueHolderImage.CrossFadeAlpha (1.0f, _enterTime, false); } 	
					if (valueText != null) { valueText.CrossFadeAlpha (1.0f, _enterTime, false); } 
				}
				
				if (_slideIn) {
					GetComponent<Animator> ().speed = 1.0f / _enterTime;
					
					switch (_anchor.type) {
						
					case HUDAnchorTypes.Left: GetComponent<Animator> ().SetTrigger (HUDConst.SLIDE_LEFT_IN_TRIGGER); break;
					case HUDAnchorTypes.Right: GetComponent<Animator> ().SetTrigger (HUDConst.SLIDE_RIGHT_IN_TRIGGER); break;
					case HUDAnchorTypes.Top: GetComponent<Animator> ().SetTrigger (HUDConst.SLIDE_TOP_IN_TRIGGER); break;
					case HUDAnchorTypes.Bottom: GetComponent<Animator> ().SetTrigger (HUDConst.SLIDE_BOTTOM_IN_TRIGGER); break;
					}
					
				}
			}
		}
		
		/// <summary>
		/// Hides this <see cref="CustomHUD.HUDHintPanel"/> on screen.
		/// </summary>
		public void Hide () {
			
			
			if (_animate) {
				
				if (_fadeOut) {
					
					progressUISlider.fillRect.GetComponent<Image> ().CrossFadeAlpha (0.0f, _exitTime, false);
					progressUISlider.GetComponent<Image> (). CrossFadeAlpha (0.0f, _exitTime, false);
					
					if (valueHolderImage != null) { valueHolderImage.CrossFadeAlpha (0.0f, _exitTime, false); } 	
					if (valueText != null) { valueText.CrossFadeAlpha (0.0f, _exitTime, false); }
				}
				
				if (_slideOut) {
					
					GetComponent<Animator> ().speed = 1.0f / _exitTime;
					GetComponent<Animator> ().SetTrigger (HUDConst.SLIDE_OUT_TRIGGER);
				}
				
				if (_fadeOut || _slideOut) {
					
					Invoke ("Disable", _exitTime);
				}
			}
			else {
				
				Disable ();
			}
			
			_isCurrentlyShown = false;
		}
		
		/// <summary>
		/// Shows this <see cref="CustomHUD.HUDHintPanel"/> on the screen and automatically hides it after a given amount of time has passed.
		/// </summary>
		/// <param name="secondsToHide">The time in seconds between showing and hiding this <see cref="CustomHUD.HUDHintPanel"/>.</param>
		public void ShowAndHide (float secondsToHide) {
			
			Show ();
			Invoke ("Hide", secondsToHide + _enterTime);
		}
		
		# endregion

		# region special methods

		private Coroutine _tempRunningCoroutine;

		public void IncreaseValueOverTime (float increment, float secondsOfDuration) {

			if (increment != 0.0f) {

				if (_tempRunningCoroutine != null) {
					
					StopCoroutine (_tempRunningCoroutine);
				}

				if (useFloatSteps) {

					_tempRunningCoroutine = StartCoroutine (CoroutineSetValueOverTime (value + increment, secondsOfDuration, increment > 0.0f));
				}
				else {

					_tempRunningCoroutine = StartCoroutine (CoroutineSetValueOverTimeIntegerSteps ((int) (value + increment), secondsOfDuration, increment > 0.0f));
				}
			}
		}

		public void IncreaseValueOverTime (int increment, float secondsOfDuration) {

			IncreaseValueOverTime ((float) increment, secondsOfDuration);
		}

		public void DecreaseValueOverTime (float decrement, float secondsOfDuration) {
			

			IncreaseValueOverTime (-decrement, secondsOfDuration);
		}
		
		public void DecreaseValueOverTime (int decrement, float secondsOfDuration) {
			
			DecreaseValueOverTime ((float) decrement, secondsOfDuration);
		}

		public void SetValueOverTime (float targetValue, float secondsOfDuration) {

			if (_tempRunningCoroutine != null) {
				
				StopCoroutine (_tempRunningCoroutine);
			}

			if (useFloatSteps) {

				_tempRunningCoroutine = StartCoroutine (CoroutineSetValueOverTime (targetValue, secondsOfDuration, targetValue > value));
			}
			else {

				_tempRunningCoroutine = StartCoroutine (CoroutineSetValueOverTimeIntegerSteps ((int) targetValue, secondsOfDuration, targetValue > value));
			}
		}

		public void SetValueOverTime (int targetValue, float secondsOfDuration) {

			SetValueOverTime ((float) targetValue, secondsOfDuration);
		}


		# endregion


		# region coroutines

		private IEnumerator CoroutineSetValueOverTime (float targetValue, float secondsOfDuration, bool incrementing) {

			targetValue = Mathf.Clamp (targetValue, minValue, maxValue);

			float stepByFrame = Mathf.Abs (value - targetValue) * Time.deltaTime / secondsOfDuration;

			// going up
			while (incrementing && value < targetValue) {
			
				value += stepByFrame;
				yield return new WaitForEndOfFrame ();
			}

			//going down
			while (!incrementing && value > targetValue) {
			
				value -= stepByFrame;
				yield return new WaitForEndOfFrame ();
			}

		}

		private IEnumerator CoroutineSetValueOverTimeIntegerSteps (int targetValue, float secondsOfDuration, bool incrementing) {
			
			targetValue = Mathf.Clamp (targetValue, intMinValue, intMaxValue);
			
			float stepByFrame = (float) Mathf.Abs (value - targetValue) * Time.deltaTime / secondsOfDuration;

			float buffer = 0.0f;

			// going up
			while (incrementing && value < targetValue) {
				
				buffer += stepByFrame;

				if (buffer >= 1.0f) {

					value += 1;
					buffer -= 1.0f;
				}

				yield return new WaitForEndOfFrame ();
			}
			
			//going down
			while (!incrementing && value > targetValue) {
				
				buffer += stepByFrame;
				
				if (buffer >= 1.0f) {
					
					value -= 1;
					buffer -= 1.0f;
				}
				yield return new WaitForEndOfFrame ();
			}
			
		}

		# endregion

	}
}