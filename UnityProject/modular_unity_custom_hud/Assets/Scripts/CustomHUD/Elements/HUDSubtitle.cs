﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace CustomHUD {

		public class HUDSubtitle : HUDElement, IHUDFadeInOutAnimated {
	
	# region ui elements

		/// <summary>
		/// The UI Text that holds the text for the subtitle.
		/// </summary>
		[Tooltip ("The UI Text that holds the text for the subtitle.")]
		public Text subtitleUIText;
	

	# endregion

	# region attributes and properties

		/// <summary>
		/// The type of subtitle that this <see cref="CustomHUD.HUDSubtitle"/> will be suscribed as to the HUDController.
		/// </summary>
		[Tooltip ("The type of subtitle that this HUDSubtitle will be suscribed as to the HUDController.")]
		[SerializeField]
		private HUDSubtitleTypes _subscribeAs;
		
		public HUDSubtitleTypes subscribeAs {
			
			get { return _subscribeAs; }
		}

		/// <summary>
		/// Gets or sets the text placed in the UI Text of this <see cref="CustomHUD.HUDSubtitle"/>, representing the content of the hint.
		/// </summary>
		/// <value>The hint string.</value>
		public string text {
			
			get { return subtitleUIText.text; }
			set { subtitleUIText.text = value; }
		}
		
		/// <summary>
		/// Gets or sets the Color of the text of the subtitle.
		/// </summary>
		/// <value>The color of the text.</value>
		public Color textColor {
			
			get { return subtitleUIText.color; }
			set { subtitleUIText.color = value; }
		}

		/// <summary>
		/// Gets or sets the font of the UI Text of this <see cref="CustomHUD.HUDSubtitle"/>.
		/// </summary>
		/// <value>The font.</value>
		public Font font {
			
			get { return subtitleUIText.font; }
			set	{ subtitleUIText.font = value; }
		}

		/// <summary>
		/// Whether this <see cref="CustomHUD.HUDSubtitle"/> is currently shown on screen.
		/// </summary>
		private bool _isCurrentlyShown;
		
		/// <summary>
		/// Gets a value indicating whether this <see cref="CustomHUD.HUDSubtitle"/> is shown.
		/// </summary>
		/// <value><c>true</c> if is shown; otherwise, <c>false</c>.</value>
		public bool isCurrentlyShown {
			
			get { return _isCurrentlyShown; } 
		}
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDSubtitle"/> should start displayed on screen or not.
		/// </summary>
		[SerializeField]
		private bool _startShown;
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDSubtitle"/> should start displayed on screen or not.
		/// </summary>
		[SerializeField]
		public bool startShown {
			
			get { return _startShown; }
			set { _startShown = value; }
		}
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDSubtitle"/> will have some animations when being displayed.
		/// </summary>
		[SerializeField]
		private bool _animate;
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDSubtitle"/> will have some animations when being displayed.
		/// </summary>
		[SerializeField]
		public bool animate {
			
			get { return _animate; }
			set { _animate = value; }
		}
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDSubtitle"/> should have a fade-in effect when being shown or not, if <see cref="CustomHUD.HUDSubtitle.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private bool _fadeIn;
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDSubtitle"/> should have a fade-in effect when being shown or not, if <see cref="CustomHUD.HUDSubtitle.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		public bool fadeIn {
			
			get { return _fadeIn; }
			set { _fadeIn = value; }
		}
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDSubtitle"/> should have a fade-out effect when being hidden or not, if <see cref="CustomHUD.HUDSubtitle.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private bool _fadeOut;
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.HUDSubtitle"/> should have a fade-out effect when being hidden or not, if <see cref="CustomHUD.HUDSubtitle.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		public bool fadeOut {
			
			get { return _fadeOut; }
			set { _fadeOut = value; }
		}
		
		/// <summary>
		/// The it takes for the fade/hide-in animations of this <see cref="CustomHUD.HUDSubtitle"/> to be completed, if <see cref="CustomHUD.HUDSubtitle.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private float _enterTime;
		
		/// <summary>
		/// The it takes for the fade/hide-in animations of this <see cref="CustomHUD.HUDSubtitle"/> to be completed, if <see cref="CustomHUD.HUDSubtitle.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		public float enterTime {
			
			get { return _enterTime; }
			set { _enterTime = value; }
		}
		
		/// <summary>
		/// The it takes for the fade/hide-out animations of this <see cref="CustomHUD.HUDSubtitle"/> to be completed, if <see cref="CustomHUD.HUDSubtitle.animate"/> is set to true.
		/// </summary>
		[SerializeField]
		private float _exitTime;
		
		/// <summary>
		[SerializeField]
		/// The it takes for the fade/hide-out animations of this <see cref="CustomHUD.HUDSubtitle"/> to be completed, if <see cref="CustomHUD.HUDSubtitle.animate"/> is set to true.
		/// </summary>
		public float exitTime {
			
			get { return _exitTime; }
			set { _exitTime = value; }
		}

		# endregion


		# region methods

		
		/// <summary>
		/// Subscribes this <see cref="CustomHUD.HUDSubtitle"/> to the <see cref="CustomHUD.HUDController in the scene."/>
		/// </summary>
		protected override void SubscribeToController () {
			
			_controller.SubscribeAs (this);
			CheckStartShown ();
		}
		
		/// <summary>
		/// Checks if this <see cref="CustomHUD.HUDSubtitle"/> should start shown or not, and shows or hides it accordingly.
		/// </summary>
		private void CheckStartShown () {
			
			if (!_startShown) {

				subtitleUIText.canvasRenderer.SetAlpha (0.0f);
				subtitleUIText.CrossFadeAlpha (0.0f, 0.0f, false);
				
				Disable ();
			}
			else {
				
			}
			
			_isCurrentlyShown = _startShown;
			gameObject.SetActive (_startShown);
		}


		/// <summary>
		/// Enables the GameObject holding this <see cref="CustomHUD.HUDSubtitle"/>.
		/// </summary>
		public void Enable () {
			
			this.gameObject.SetActive (true);
			
			_isCurrentlyShown = true;
		}
		
		/// <summary>
		/// Disables the GameObject holding this <see cref="CustomHUD.HUDSubtitle"/>.
		/// </summary>
		public void Disable () {
			
			this.gameObject.SetActive (false);
			
			_isCurrentlyShown = false;
		}

		/// <summary>
		/// Sets the role of this <see cref="CustomHUD.HUDSubtitle"/>, as which it will be subscribed to the <see cref="CustomHUD.HUDController"/>.
		/// </summary>
		/// <param name="role">Role.</param>
		public void SetRole (HUDSubtitleTypes role) {
			
			_subscribeAs = role;
		}

		/// <summary>
		/// Lays the element and the UI Text for a given target anchor.
		/// </summary>
		/// <param name="targetAnchor">Target anchor.</param>
		public void LayoutToAnchor (HUDAnchor targetAnchor) {
		
			_anchor = targetAnchor;

			if (_anchor.type == HUDAnchorTypes.Bottom) 		{ LayoutToBottom (); }
			else if (_anchor.type == HUDAnchorTypes.Top) 	{ LayoutToTop (); }
		}

		/// <summary>
		/// Lays the elements out to the bottom.
		/// </summary>
		private void LayoutToBottom () {

			RectTransform r = GetComponent <RectTransform> ();
			
			r.anchorMin = new Vector2 (HUDConst.ANCHOR_MIN_VALUE, HUDConst.ANCHOR_MIN_VALUE);
			r.anchorMax = new Vector2 (HUDConst.ANCHOR_MAX_VALUE, HUDConst.ANCHOR_MIN_VALUE);

			r.anchoredPosition 	= new Vector2 (0.0f, 0.0f);
			r.sizeDelta 		= new Vector2 (0.0f, 0.0f);

			r.pivot = new Vector2 (HUDConst.PIVOT_X_FOR_BOTTOM_ANCHOR, HUDConst.PIVOT_Y_FOR_BOTTOM_ANCHOR);
			
			RectTransform t = subtitleUIText.GetComponent<RectTransform> ();
			
			t.anchorMin = new Vector2 (HUDConst.ANCHOR_MIN_VALUE, HUDConst.ANCHOR_MAX_VALUE);
			t.anchorMax = new Vector2 (HUDConst.ANCHOR_MAX_VALUE, HUDConst.ANCHOR_MAX_VALUE);
			t.pivot = new Vector2 (HUDConst.PIVOT_X_FOR_BOTTOM_ANCHOR, HUDConst.PIVOT_Y_FOR_BOTTOM_ANCHOR);
			
			t.anchoredPosition = new Vector2 (0.0f, 0.0f);
		}

		/// <summary>
		/// Lays the elements out to the top.
		/// </summary>
		private void LayoutToTop () {
		
			RectTransform r = GetComponent <RectTransform> ();
			
			r.anchorMin = new Vector2 (HUDConst.ANCHOR_MIN_VALUE, HUDConst.ANCHOR_MAX_VALUE);
			r.anchorMax = new Vector2 (HUDConst.ANCHOR_MAX_VALUE, HUDConst.ANCHOR_MAX_VALUE);

			r.anchoredPosition 	= new Vector2 (0.0f, 0.0f);
			r.sizeDelta 		= new Vector2 (0.0f, 0.0f);

			r.pivot = new Vector2 (HUDConst.PIVOT_X_FOR_TOP_ANCHOR, HUDConst.PIVOT_Y_FOR_TOP_ANCHOR);

			RectTransform t = subtitleUIText.GetComponent<RectTransform> ();

			t.anchorMin = new Vector2 (HUDConst.ANCHOR_MIN_VALUE, HUDConst.ANCHOR_MIN_VALUE);
			t.anchorMax = new Vector2 (HUDConst.ANCHOR_MAX_VALUE, HUDConst.ANCHOR_MIN_VALUE);
			t.pivot = new Vector2 (HUDConst.PIVOT_X_FOR_TOP_ANCHOR, HUDConst.PIVOT_Y_FOR_TOP_ANCHOR);

			t.anchoredPosition = new Vector2 (0.0f, 0.0f);
		}

		/// <summary>
		/// Sets the height of the subtitle.
		/// </summary>
		/// <param name="height">Height.</param>
		public void SetHeight (float height) {
		
			RectTransform t = subtitleUIText.GetComponent<RectTransform> ();
			t.sizeDelta = new Vector2 (t.anchoredPosition.x, height);
		}

		/// <summary>
		/// Sets the margins of the subtitle with respect to the sides of the screen.
		/// </summary>
		/// <param name="margin">Margin.</param>
		public void SetMargins (float margin) {
		
			RectTransform t = subtitleUIText.GetComponent<RectTransform> ();
			t.sizeDelta = new Vector2 (- 2.0f * margin, t.sizeDelta.y);
		}

		/// <summary>
		/// Sets the distance of the subititle from the border of the screen.
		/// </summary>
		/// <param name="distance">Distance.</param>
		public void SetDistance (float distance) {
		
			RectTransform r = GetComponent <RectTransform> ();
			r.sizeDelta = new Vector2 (r.sizeDelta.x, distance);
		}

		/// <summary>
		/// Adds a Shadow effect component to the UI Text of this <see cref="CustomHUD.HUDSubtitle"/> of the given Color.
		/// </summary>
		/// <param name="color">Color.</param>
		public void AddTextShadow (Color color) {
			
			subtitleUIText.gameObject.AddComponent<Shadow> ().effectColor = color;
		}
		
		/// <summary>
		/// Sets the color of the Shadow effect component of the UI Text of this <see cref="CustomHUD.HUDSubtitle"/> of the given Color. Creates it if the component doesn't exist.
		/// </summary>
		/// <param name="color">Color.</param>
		public void SetTextShadow (Color color) {
			
			
			if (subtitleUIText.GetComponent<Shadow> () != null) {
				
				subtitleUIText.GetComponent<Shadow> ().effectColor = color;
			}
			else {
				
				AddTextShadow (color);
			}
		}
		
		/// <summary>
		/// Removes the Shadow effect component of the UI Text of this <see cref="CustomHUD.HUDSubtitle"/>.
		/// </summary>
		public void RemoveTextShadow () {
			
			DestroyImmediate (subtitleUIText.GetComponent<Shadow> ());
		}
		
		/// <summary>
		/// Adds a Outline effect component to the UI Text of this <see cref="CustomHUD.HUDSubtitle"/> of the given Color.
		/// </summary>
		/// <param name="color">Color.</param>
		public void AddTextOutline (Color color) {
			
			subtitleUIText.gameObject.AddComponent<Outline> ().effectColor = color;
		}
		
		/// <summary>
		/// Sets the color of the Outline effect component of the UI Text of this <see cref="CustomHUD.HUDSubtitle"/> of the given Color. Creates it if the component doesn't exist.
		/// </summary>
		/// <param name="color">Color.</param>
		public void SetTextOutline (Color color) {
			
			
			if (subtitleUIText.GetComponent<Outline> () != null) {
				
				subtitleUIText.GetComponent<Outline> ().effectColor = color;
			}
			else {
				
				AddTextOutline (color);
			}
		}
		
		/// <summary>
		/// Removes the Outline effect component of the UI Text of this <see cref="CustomHUD.HUDSubtitle"/>.
		/// </summary>
		public void RemoveTextOutline () {
			
			DestroyImmediate (subtitleUIText.GetComponent<Outline> ());
		}	

		# endregion


		# region show
		
		/// <summary>
		/// Shows this <see cref="CustomHUD.HUDSubtitle"/> on screen.
		/// </summary>
		public void Show () {
			
			Enable ();
			
			if (_animate) {
				
				if (_fadeIn) {

					subtitleUIText.canvasRenderer.SetAlpha (0.0f);
					subtitleUIText	.CrossFadeAlpha (1.0f, _enterTime, false);
				}
			}
		}
		
		/// <summary>
		/// Hides this <see cref="CustomHUD.HUDSubtitle"/> on screen.
		/// </summary>
		public void Hide () {
			
			
			if (_animate) {
				
				if (_fadeOut) {

					subtitleUIText.canvasRenderer.SetAlpha (1.0f);
					subtitleUIText	.CrossFadeAlpha (0.0f, _exitTime, false);
					
					Invoke ("Disable", _exitTime);
				}
			}
			else {
				
				Disable ();
			}
			
			_isCurrentlyShown = false;
		}
		
		/// <summary>
		/// Shows this <see cref="CustomHUD.HUDSubtitle"/> on the screen and automatically hides it after a given amount of time has passed.
		/// </summary>
		/// <param name="secondsToHide">The time in seconds between showing and hiding this <see cref="CustomHUD.HUDSubtitle"/>.</param>
		public void ShowAndHide (float secondsToHide) {
			
			Show ();
			Invoke ("Hide", secondsToHide + _enterTime);
		}
		
		# endregion
	}
}