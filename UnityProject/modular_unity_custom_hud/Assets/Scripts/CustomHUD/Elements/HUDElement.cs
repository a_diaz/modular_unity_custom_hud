﻿using UnityEngine;
using System.Collections;

namespace CustomHUD {

	public abstract class HUDElement : MonoBehaviour, IHUDEntity, IHUDElement {

		# region constants

		/// <summary>
		/// The type of the HUD entity.
		/// </summary>
		private const HUDEntityTypes _entityType = HUDEntityTypes.Element;


		# endregion

		# region static variables and properties

		/// <summary>
		/// The number of HUDElements initialized.
		/// </summary>
		protected static int _elementsInitialized = 0;

		/// <summary>
		/// Gets the number of HUDElements initialized.
		/// </summary>
		/// <value>Number of elements initialized.</value>
		public static int elementsInitialized {

			get { return _elementsInitialized; }
		}

		# endregion

		# region attributes and properties

		/// <summary>
		/// Gets the type of the entity.
		/// </summary>
		/// <value>The type of the entity.</value>
		public HUDEntityTypes entityType {
			
			get { return _entityType; }
		}

		/// <summary>
		/// The type of this HUDElement.
		/// </summary>
		protected HUDElementTypes _type;

		/// <summary>
		/// Gets the type this HUDElement.
		/// </summary>
		/// <value>The type.</value>
		public HUDElementTypes type {

			get { return _type; }
		}

		/// <summary>
		/// The HUDController to which this HUDElement is suscribed.
		/// </summary>
		protected HUDController _controller;

		/// <summary>
		/// Gets the HUDcontroller to which the entity is suscribed. If the entity is the HUDController, gets a reference to itself.
		/// </summary>
		/// <value>The controller.</value>
		public HUDController controller {
			
			get { return _controller; }
		}


		/// <summary>
		/// The anchor to which this element is anchored.
		/// </summary>
		[SerializeField]protected HUDAnchor _anchor;

		/// <summary>
		/// Gets the anchor to which this element is anchored.
		/// </summary>
		/// <value>The anchor.</value>
		public HUDAnchor anchor {

			get { return _anchor; } 
		}

		/// <summary>
		/// Set to true when this HUDElement has been correctly initialized.
		/// </summary>
		protected bool _initialized;

		/// <summary>
		/// Gets a value indicating whether this <see cref="CustomHUD.HUDElement"/> is initialized.
		/// </summary>
		/// <value><c>true</c> if is initialized; otherwise, <c>false</c>.</value>
		public bool isInitialized {

			get { return _initialized; }
		}

		# endregion

		protected void Awake () {

			InitializeEvents ();
			CheckAnchorAssigned ();
		}

		# region events

		private void InitializeEvents () {
		
			Debug.Log ("subscribin");
			HUDEvents.OnHUDControllerInitializedHandler += OnHUDControllerInitialized;
		}

		private void UnsuscribeFromEvents () {
		
			HUDEvents.OnHUDControllerInitializedHandler -= OnHUDControllerInitialized;
		}

		# endregion


		# region set get

		/// <summary>
		/// Gets the anchor to which this element is anchored.
		/// </summary>
		/// <returns>The anchor.</returns>
		public HUDAnchor GetAnchor () {

			return anchor;
		}

		/// <summary>
		/// Gets the type of this HUDElement.
		/// </summary>
		/// <returns>The type.</returns>
		public HUDElementTypes GetElmentType () {

			return _type;
		}

		/// <summary>
		/// Gets the type of the entity.
		/// </summary>
		/// <returns>The entity type.</returns>
		public HUDEntityTypes GetEntityType () {

			return entityType;
		}

		/// <summary>
		/// Determines whether this entity is initialized.
		/// </summary>
		/// <returns>true</returns>
		/// <c>false</c>
		public bool IsInitialized () {

			return isInitialized;
		}

		/// <summary>
		/// Gets the HUDcontroller to which the entity is suscribed. If the entity is the HUDController, gets a reference to itself.
		/// </summary>
		/// <returns>The controller.</returns>
		public HUDController GetController () {

			return controller;
		}

		/// <summary>
		/// Sets the pivot of this UI element.
		/// </summary>
		/// <param name="pivotX">Pivot x.</param>
		/// <param name="pivotY">Pivot y.</param>
		public void SetPivot (float pivotX, float pivotY) {
		
			SetPivot (new Vector2 (pivotX, pivotY));
		}

		/// <summary>
		/// Sets the pivot of this UI element.
		/// </summary>
		/// <param name="newPivot">New pivot.</param>
		public void SetPivot (Vector2 newPivot) {
		
			GetComponent<RectTransform> ().pivot = newPivot;
		}

		/// <summary>
		/// Sets up the RectTransform for the given anchor.
		/// </summary>
		/// <param name="targetAnchor">Target anchor.</param>
		public void SetUpForAnchor (HUDAnchor targetAnchor) {
		
			_anchor = targetAnchor;

			switch (targetAnchor.type) {

			case HUDAnchorTypes.Top:
				SetUpForTopAnchor ();
				break;
			case HUDAnchorTypes.Bottom:
				SetUpForBottomAnchor ();
				break;
			case HUDAnchorTypes.Left:
				SetUpForLeftAnchor ();
				break;
			case HUDAnchorTypes.Right:
				SetUpForRightAnchor ();
				break;
			case HUDAnchorTypes.Center:
				SetUpForCenterAnchor ();
				break;
			}
		}

		/// <summary>
		/// Sets up the RectTransform for the top anchor.
		/// </summary>
		public void SetUpForTopAnchor () {

			SetUpRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MAX_VALUE),
			                  new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MAX_VALUE),
			                  new Vector2 (HUDConst.PIVOT_X_FOR_TOP_ANCHOR, HUDConst.PIVOT_Y_FOR_TOP_ANCHOR)
			);

		}

		/// <summary>
		/// Sets up the RectTransform for the bottom anchor.
		/// </summary>
		public void SetUpForBottomAnchor () {

			SetUpRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MIN_VALUE),
			                  new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MIN_VALUE),
			                  new Vector2 (HUDConst.PIVOT_X_FOR_BOTTOM_ANCHOR, HUDConst.PIVOT_Y_FOR_BOTTOM_ANCHOR)
			);
		}

		/// <summary>
		/// Sets up the RectTransform for the left anchor.
		/// </summary>
		public void SetUpForLeftAnchor () {

			SetUpRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MIN_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                  new Vector2 (HUDConst.ANCHOR_MIN_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                  new Vector2 (HUDConst.PIVOT_X_FOR_LEFT_ANCHOR, HUDConst.PIVOT_Y_FOR_LEFT_ANCHOR)
			);
		}

		/// <summary>
		/// Sets up the RectTransform for the right anchor.
		/// </summary>
		public void SetUpForRightAnchor () {

			SetUpRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MAX_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                  new Vector2 (HUDConst.ANCHOR_MAX_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                  new Vector2 (HUDConst.PIVOT_X_FOR_RIGHT_ANCHOR, HUDConst.PIVOT_Y_FOR_RIGHT_ANCHOR)
			);
		}

		/// <summary>
		/// Sets up the RectTransform for the center anchor.
		/// </summary>
		public void SetUpForCenterAnchor () {
			
			SetUpRectTranformWithGivenValues (new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                  new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MID_VALUE),
			                  new Vector2 (HUDConst.PIVOT_X_FOR_CENTER_ANCHOR, HUDConst.PIVOT_Y_FOR_CENTER_ANCHOR)
			                  );
		}

		/// <summary>
		/// Sets the basic RectTransform values to a given vectors. The localPosition is set to zero.
		/// </summary>
		/// <param name="anchorMin">Anchor minimum.</param>
		/// <param name="anchorMax">Anchor max.</param>
		/// <param name="pivot">Pivot.</param>
		private void SetUpRectTranformWithGivenValues (Vector2 anchorMin, Vector2 anchorMax, Vector2 pivot) {
		
			RectTransform r = GetComponent<RectTransform> ();
			
			r.anchorMin = anchorMin;
			r.anchorMax = anchorMax;
			r.pivot = pivot;

			// by default at zero // it will be modified as needed
			r.anchoredPosition = Vector3.zero;
		}
	
		# endregion 

		# region event response
		
		/// <summary>
		/// Called when a HUDController has been initialized in the hierarchy.
		/// </summary>
		/// <param name="initializedController">Initialized controller.</param>
		protected void OnHUDControllerInitialized (HUDController initializedController) {
			
			_controller = initializedController;
			
			SubscribeToController ();
			HUDEvents.OnHUDControllerInitializedHandler -= OnHUDControllerInitialized;
		}
		
		
		# endregion
		
		# region subscription
		
		protected abstract void SubscribeToController ();
		
		
		# endregion

		# region other

		private void CheckAnchorAssigned () {

			if (_anchor == null) {
			
				_anchor = GetComponentInParent<HUDAnchor> ();
			}
		}

		# endregion
	}
}


