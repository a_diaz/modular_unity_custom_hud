using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CustomHUD {

	/// <summary>
	/// Interface whose methods must implement all weak singletons of a given class T.
	/// </summary>
	public interface IWeakSingleton<T> {
			
		/*

			Static methods can't be defined in Interfaces, but still, a class who is a weak singleton
			must have the following members in order to be able to retrieve the singleton instance from it.
			
		 */

		/*

		static T Instance {

			get;
		}

		static bool hasSingletonInstanceSet {

			get;
		}
		
		static T GetInstance ();

		*/
	}

	/// <summary>
	/// Interface whose methods must implement all entities of the CustomHUD.
	/// </summary>
	public interface IHUDEntity {

		/// <summary>
		/// Gets the type of the entity.
		/// </summary>
		/// <value>The type of the entity.</value>
		HUDEntityTypes entityType {

			get;
		}

		/// <summary>
		/// Gets the type of the entity.
		/// </summary>
		/// <returns>The entity type.</returns>
		HUDEntityTypes GetEntityType ();

		/// <summary>
		/// Gets a value indicating whether this <see cref="CustomHUD.IHUDEntity"/> is initialized.
		/// </summary>
		/// <value><c>true</c> if is initialized; otherwise, <c>false</c>.</value>
		bool isInitialized {

			get;
		}

		/// <summary>
		/// Determines whether this entity is initialized.
		/// </summary>
		/// <returns><c>true</c> if this entity is initialized; otherwise, <c>false</c>.</returns>
		bool IsInitialized ();

		/// <summary>
		/// Gets the HUDcontroller to which the entity is suscribed. If the entity is the HUDController, gets a reference to itself.
		/// </summary>
		/// <value>The controller.</value>
		HUDController controller {

			get;
		}

		/// <summary>
		/// Gets the HUDcontroller to which the entity is suscribed. If the entity is the HUDController, gets a reference to itself.
		/// </summary>
		/// <returns>The controller.</returns>
		HUDController GetController ();
	}

	/// <summary>
	/// Interface whose methods that the HUDController must implement.
	/// </summary>
	public interface IHUDController {

		/// <summary>
		/// Gets a dictionary with the HUDAnchorTypes as keys identifying each HUDAnchor suscribed to this IHUDController.
		/// </summary>
		/// <value>The anchors.</value>
		Dictionary<HUDAnchorTypes, HUDAnchor> anchors {
		
			get;
		}

		/// <summary>
		/// Gets a dictionary with the HUDAnchorTypes as keys identifying each IHUDAnchor suscribed to this IHUDController.
		/// </summary>
		/// <returns>The anchors.</returns>
		Dictionary<HUDAnchorTypes, HUDAnchor> GetAnchors ();

		/// <summary>
		/// Gets an IHUDAnchor suscribed to this IHUDController given its type.
		/// </summary>
		/// <returns>The anchor.</returns>
		/// <param name="anchorType">Anchor type.</param>
		HUDAnchor GetAnchor (HUDAnchorTypes anchorType);

		/// <summary>
		/// Gets the left IHUDAnchor.
		/// </summary>
		/// <value>The left anchor.</value>
		HUDAnchor leftAnchor {

			get;
		}

		/// <summary>
		/// Gets the right HUDAnchor.
		/// </summary>
		/// <value>The right anchor.</value>
		HUDAnchor rightAnchor {
			
			get;
		}

		/// <summary>
		/// Gets the top HUDAnchor.
		/// </summary>
		/// <value>The top anchor.</value>
		HUDAnchor topAnchor {
			
			get;
		}

		/// <summary>
		/// Gets the bottom HUDAnchor.
		/// </summary>
		/// <value>The bottom anchor.</value>
		HUDAnchor bottomAnchor {
			
			get;
		}

		/// <summary>
		/// Gets the center HUDAnchor.
		/// </summary>
		/// <value>The center anchor.</value>
		HUDAnchor centerAnchor {
			
			get;
		}

		/// <summary>
		/// Gets the number of HUDElements suscribed to this IHUDController.
		/// </summary>
		/// <value>The number of elements.</value>
		int numberOfElements {
		
			get;
		
		}

		/// <summary>
		/// The HUDAnchor anchor subscribes to this HUDControler as a given HUDAnchorType.
		/// </summary>
		/// <param name="anchorType">Anchor type.</param>
		/// <param name="anchor">Anchor.</param>
		void SubscribeAs (HUDAnchorTypes anchorType, HUDAnchor anchor);
	}

	/// <summary>
	/// Interface whose methods must implement all HUDElements.
	/// </summary>
	public interface IHUDElement {

		/// <summary>
		/// Gets the type of this HUDElement.
		/// </summary>
		/// <value>The type.</value>
		HUDElementTypes type {

			get;
		}

		/// <summary>
		/// Gets the type of this HUDElement.
		/// </summary>
		/// <returns>The type.</returns>
		HUDElementTypes GetElmentType ();

		/// <summary>
		/// Gets the HUDAnchor to which this HUDElement is anchored to.
		/// </summary>
		/// <value>The anchor.</value>
		HUDAnchor anchor {

			get;
		}

		/// <summary>
		/// Gets the HUDAnchor to which this HUDElement is anchored to.
		/// </summary>
		/// <returns>The anchor.</returns>
		HUDAnchor GetAnchor ();
	}


	/// <summary>
	/// Interface whose methods must implement all HUDAnchors.
	/// </summary>
	public interface IHUDAnchor {

		/// <summary>
		/// Gets the type of this HUDAnchor. Indicates where the elements are going to be anchored in the screen.
		/// </summary>
		/// <value>The type.</value>
		HUDAnchorTypes type {

			get;
		}

		/// <summary>
		/// Gets the type of this HUDAnchor. Indicates where the elements are going to be anchored in the screen.
		/// </summary>
		/// <returns>The type.</returns>
		HUDAnchorTypes GetAnchorType ();

		/// <summary>
		/// Gets a list with all the HUDElements anchored to this anchor.
		/// </summary>
		List<HUDElement> elements {

			get;
		}

		/// <summary>
		/// Gets a list with all the HUDElements anchored to this anchor.
		/// </summary>
		/// <returns>The elements.</returns>
		List<HUDElement> GetElements ();

		/// <summary>
		/// Gets the number of HUDElements anchored to this HUDAnchor.
		/// </summary>
		/// <value>The number of elements.</value>
		int numberOfElements {

			get;
		}
	}
	
	/// <summary>
	/// Interface whose members all warning panels must implement.
	/// </summary>
	public interface IHUDWarningPanel {
		
		/// <summary>
		/// A string representing the content of the warning.
		/// </summary>
		/// <value>The text.</value>
		string text {
			
			get;
			set;
		}
		
		/// <summary>
		/// Gets or sets the Color of the text of the warning.
		/// </summary>
		/// <value>The color of the text.</value>
		Color textColor {
			
			get;
			set;
		}
	}

	/// <summary>
	/// Interface whose members all hint panels must implement.
	/// </summary>
	public interface IHUDHintPanel {

		/// <summary>
		/// A string representing the content of the hint.
		/// </summary>
		/// <value>The text.</value>
		string text {

			get;
			set;
		}

		/// <summary>
		/// Gets or sets the Color of the text of the hint.
		/// </summary>
		/// <value>The color of the text.</value>
		Color textColor {

			get;
			set;
		}
	}


	public interface IHUDProgressBar {


		/// <summary>
		/// Gets or sets the current value to display by the progress bar. Clamped between minValue and maxValue;
		/// </summary>
		/// <value>The current value.</value>
		float value {

			get;
			set;
		}

		/// <summary>
		/// Gets or sets the maximum value the progress bar can represent. At this value, the bar will be completely full.
		/// </summary>
		/// <value>The max value.</value>
		float maxValue {

			get;
			set;
		}

		/// <summary>
		/// Gets or sets the minimum value this progress bar can represent. At this value, the bar will be completely empty.
		/// </summary>
		/// <value>The minimum value.</value>
		float minValue {

			get;
			set;
		}

		/// <summary>
		/// Gets or sets the current value percentually clamped by [0.0f, 1.0f].
		/// </summary>
		/// <value>The value percent.</value>
		float normalizedValue {

			get;
			set;
		}
	}

	/// <summary>
	/// Interface for all HUDElemnts that can be shown or hidden on the screen.
	/// </summary>
	public interface IHUDShowable {

		/// <summary>
		/// Gets a value indicating whether this <see cref="CustomHUD.IHUDShowable"/> is currently shown on screen.
		/// </summary>
		/// <value><c>true</c> if is currently shown; otherwise, <c>false</c>.</value>
		bool isCurrentlyShown {

			get;
		}

		
		/// <summary>
		/// Wether this <see cref="CustomHUD.IHUDInOutAnimations"/> should start displayed on screen or not.
		/// </summary>
		bool startShown {
			
			get;
			set;
		}

		/// <summary>
		/// Shows this <see cref="CustomHUD.IHUDShowable"/> on screen.
		/// </summary>
		void Show ();

		/// <summary>
		/// Hides this <see cref="CustomHUD.IHUDShowable"/> from the screen.
		/// </summary>
		void Hide ();
	
		/// <summary>
		/// Shows this <see cref="CustomHUD.IHUDShowable"/> on the screen and automatically hides it after a given amount of time has passed.
		/// </summary>
		/// <param name="secondsToHide">The time in seconds between showing and hiding this <see cref="CustomHUD.IHUDShowable"/>.</param>
		void ShowAndHide (float secondsToHide);
	}

	/// <summary>
	/// Interface for all IHUDShowable HUDElements that are able to be shown or dismissed with a fade in/out effect.
	/// </summary>
	public interface IHUDFadeInOutAnimated : IHUDShowable {
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.IHUDInOutAnimations"/> will have some animations when being displayed.
		/// </summary>
		bool animate {
			
			get;
			set;
		}
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.IHUDInOutAnimations"/> should have a fade-in effect when being shown or not, if <see cref="CustomHUD.IHUDInOutAnimations.animate"/> is set to true.
		/// </summary>
		bool fadeIn {
			
			get;
			set;
		}
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.IHUDInOutAnimations"/> should have a fade-out effect when being hidden or not, if <see cref="CustomHUD.IHUDInOutAnimations.animate"/> is set to true.
		/// </summary>
		bool fadeOut {
			
			get;
			set;
		}
		
		/// <summary>
		/// The it takes for the fade/hide-in animations of this <see cref="CustomHUD.IHUDInOutAnimations"/> to be completed, if <see cref="CustomHUD.IHUDInOutAnimations.animate"/> is set to true.
		/// </summary>
		float enterTime {
			
			get;
			set;
		}
		
		/// <summary>
		/// The it takes for the fade/hide-out animations of this <see cref="CustomHUD.IHUDInOutAnimations"/> to be completed, if <see cref="CustomHUD.IHUDInOutAnimations.animate"/> is set to true.
		/// </summary>
		float exitTime {
			
			get;
			set;
		}
	}

	/// <summary>
	/// Interface for all IHUDShowable HUDElements that are able to be shown or dismissed with a slide in/out effect.
	/// </summary>
	public interface IHUDSlideInOutAnimated : IHUDShowable {
	
		/// <summary>
		/// Wether this <see cref="CustomHUD.IHUDInOutAnimations"/> will have some animations when being displayed.
		/// </summary>
		bool animate {
			
			get;
			set;
		}
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.IHUDInOutAnimations"/> should have a slide-in effect when being shown or not, if <see cref="CustomHUD.IHUDInOutAnimations.animate"/> is set to true.
		/// </summary>
		bool slideIn {
			
			get;
			set;
		}
		
		/// <summary>
		/// Wether this <see cref="CustomHUD.IHUDInOutAnimations"/> should have a slide-out effect when being hidden or not, if <see cref="CustomHUD.IHUDInOutAnimations.animate"/> is set to true.
		/// </summary>
		bool slideOut {
			
			get;
			set;
		}
		
		/// <summary>
		/// The it takes for the fade/hide-in animations of this <see cref="CustomHUD.IHUDInOutAnimations"/> to be completed, if <see cref="CustomHUD.IHUDInOutAnimations.animate"/> is set to true.
		/// </summary>
		float enterTime {
			
			get;
			set;
		}
		
		/// <summary>
		/// The it takes for the fade/hide-out animations of this <see cref="CustomHUD.IHUDInOutAnimations"/> to be completed, if <see cref="CustomHUD.IHUDInOutAnimations.animate"/> is set to true.
		/// </summary>
		float exitTime {
			
			get;
			set;
		}
	}
}