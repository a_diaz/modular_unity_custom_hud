﻿using UnityEngine;
using System.Collections;


namespace CustomHUD {

	public enum HUDEntityTypes {

		Controller = 0,
		Anchor,
		Element
	}

	public enum HUDAnchorTypes {

		Left	= 0,
		Right,
		Top,
		Bottom,
		Center
	}

	public enum HUDElementTypes {

		Subtitle = 0,
		WarningPanel,
		HintPanel,
		ProgressBar
	}

	public enum HUDSubtitleTypes {

		PrimarySubtitle = 0,
		SecondarySubtitle = 1
	}

	public enum HUDHintTypes {

		ActionHint = 0,
		ReminderHint,
		OtherHint1,
		OtherHint2,
		OtherHint3
	}

	public enum HUDProgressBarTypes {

		HealthBar = 0,
		EnergyBar,
		ExperienceBar,
		LevelBar,
		OtherBar1,
		OtherBar2,
		OtherBar3
	}

	public enum HUDHintLayouts {
		
		IconLeftTextRight = 0,
		TextLeftIconRight = 1,
		IconAboveTextBelow = 2,
		TextAboveIconBelow = 3
	}

	public enum HUDWarningLayouts {

		IconLeftTextRight = 0,
		TextLeftIconRight = 1
	}

	public enum HUDProgressBarLayouts {

		NumberLeftBarRight = 0,
		BarLeftNumberRight = 1
	}

	public enum HUDProgressBarAlignment {

		Middle = 0,
		Up = 1,
		Down = 2
	}
}
