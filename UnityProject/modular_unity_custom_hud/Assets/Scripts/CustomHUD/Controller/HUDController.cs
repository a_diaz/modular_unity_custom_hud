using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace CustomHUD {
	 
	public class HUDController : MonoBehaviour, IWeakSingleton<HUDController>, IHUDEntity, IHUDController {

		# region constants
		
		/// <summary>
		/// The type of the HUD entity.
		/// </summary>
		private const HUDEntityTypes _entityType = HUDEntityTypes.Controller;
		
		
		# endregion


		# region static variables

		/// <summary>
		/// The static reference to the only HUDController instance that should exist in the scene.
		/// </summary>
		private static HUDController _Instance;

		/// <summary>
		/// Gets the static reference to the only HUDController instance that should exist in the scene.
		/// </summary>
		/// <value>The instance.</value>
		public static HUDController Instance {

			get { return _Instance; }
		}

		/// <summary>
		/// Gets a value indicating whether this <see cref="CustomHUD.HUDController"/> has an singleton instance set.
		/// </summary>
		/// <value><c>true</c> if has an singleton instance set; otherwise, <c>false</c>.</value>
		public static bool hasSingletonInstanceSet {

			get { return _Instance != null; }
		}


		# endregion

		# region static methods

		
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		/// <returns>The instance.</returns>
		public static HUDController GetInstance () {
			
			return Instance;
		}

		# endregion

		# region attributes and properties

		/// <summary>
		/// Gets the type of the entity.
		/// </summary>
		/// <value>The type of the entity.</value>
		public HUDEntityTypes entityType {
			
			get { return _entityType; }
		}
		
		/// <summary>
		/// Set to true when this HUDController has been correctly initialized.
		/// </summary>
		private bool _initialized;
		
		/// <summary>
		/// Gets a value indicating whether this <see cref="CustomHUD.HUDController/> is initialized.
		/// </summary>
		/// <value><c>true</c> if is initialized; otherwise, <c>false</c>.</value>
		public bool isInitialized {
			
			get { return _initialized; }
		}

		/// <summary>
		/// Gets the HUDcontroller to which the entity is suscribed. If the entity is the HUDController, gets a reference to itself.
		/// </summary>
		/// <value>The controller.</value>
		public HUDController controller {
			
			get { return this; }
		}

		private HUDEvents _events;

		public HUDEvents events {

			get { 

				if (_events == null) { _events = new HUDEvents (); }

				return _events; 
			}
		}

		/// <summary>
		/// Gets the number of HUDElements subscribed to this HUDController.
		/// </summary>
		/// <value>The number of elements.</value>
		public int numberOfElements {

			get { return 0; }
		}

		/// <summary>
		/// Gets the number of HUDAnchors subscribed to this HUDController.
		/// </summary>
		/// <value>The number of anchors.</value>
		public int numberOfAnchors {

			get { return _anchors.Count; }
		}

		/// <summary>
		/// The same HUDAnchors in the dictionary, but cached for re-assigning them when the compiler clears the dictionary.		
		/// </summary>
		private HUDAnchor[] _tempCachedAnchors;

		/// <summary>
		/// A dictionary indexed by HUDAnchorTypes as keys that holds references to the five HUDAnchors subscribed to this HUDController.
		/// </summary>
		private Dictionary<HUDAnchorTypes,HUDAnchor> _anchors;

		/// <summary>
		/// Gets a dictionary with the HUDAnchorTypes as keys identifying each HUDAnchor suscribed to this IHUDController.
		/// </summary>
		/// <value>The anchors.</value>
		public Dictionary<HUDAnchorTypes,HUDAnchor> anchors {

			get { return _anchors; }
		}

		/// <summary>
		/// Gets the left HUDAnchor.
		/// </summary>
		/// <value>The left anchor.</value>
		public HUDAnchor leftAnchor {

			get { return anchors[HUDAnchorTypes.Left]; }
		}
		
		/// <summary>
		/// Gets the right HUDAnchor.
		/// </summary>
		/// <value>The right anchor.</value>
		public HUDAnchor rightAnchor {
			
			get { return anchors[HUDAnchorTypes.Right]; }
		}
		
		/// <summary>
		/// Gets the top HUDAnchor.
		/// </summary>
		/// <value>The top anchor.</value>
		public HUDAnchor topAnchor {
			
			get { return anchors[HUDAnchorTypes.Top]; }
		}
		
		/// <summary>
		/// Gets the bottom HUDAnchor.
		/// </summary>
		/// <value>The bottom anchor.</value>
		public HUDAnchor bottomAnchor {
			
			get { return anchors[HUDAnchorTypes.Bottom]; }
		}

		/// <summary>
		/// Gets the center HUDAnchor.
		/// </summary>
		/// <value>The center anchor.</value>
		public HUDAnchor centerAnchor {
			
			get { return anchors[HUDAnchorTypes.Center]; }
		}

		private Dictionary<string, HUDElement> _elements;

		public Dictionary<string, HUDElement> elements {

			get { return _elements; }
		}
				
		/// <summary>
		/// Gets the CanvasScaler component of this HUDAnchor.
		/// </summary>
		/// <value>The canvas scaler.</value>
		public CanvasScaler canvasScaler { 
			
			get { return GetComponent<CanvasScaler> (); } 
		}
		
		/// <summary>
		/// Gets the reference resolution of the CanvasScaler component of this HUDAnchor.
		/// </summary>
		/// <value>The reference resolution.</value>
		public Vector2 referenceResolution {
			
			get { return GetComponent<CanvasScaler> ().referenceResolution; }
		}

		# region public variables for editor

		/// <summary>
		/// The default font for the HUDElements. They can override it and use a different one.
		/// </summary>
		public Font defaultFont;

		# endregion

		# endregion

		# region unity messages 

		void Awake () {

			if (hasSingletonInstanceSet && Instance != this) {
			
				DestroyImmediate (this.gameObject);
				return;
			}

				
			Initialize ();
			InitializeCollections ();
			
		}

		void Start () {

			// if the initialization succeeded, notify it
			if (_initialized) {
			
				NotifyInitialization ();
			}
		}

		void OnDestroy () {

			if (_Instance == this) {
			
				_Instance = null;
			}

			NotifyDestroyed ();
			Debug.Log ("DESTROYED");
		}

		# endregion

		# region general methods

		
		/// <summary>
		/// Determines whether this entity is initialized.
		/// </summary>
		/// <returns>true</returns>
		/// <c>false</c>
		public bool IsInitialized () {
			
			return isInitialized;
		}

		/// <summary>
		/// Gets the type of the entity.
		/// </summary>
		/// <returns>The entity type.</returns>
		public HUDEntityTypes GetEntityType () {
			
			return entityType;
		}

		/// <summary>
		/// Gets the HUDcontroller to which the entity is suscribed. If the entity is the HUDController, gets a reference to itself.
		/// </summary>
		/// <returns>The controller.</returns>
		public HUDController GetController () {

			return controller;
		}

		/// <summary>
		/// Gets an IHUDAnchor suscribed to this IHUDController given its type.
		/// </summary>
		/// <returns>The anchor.</returns>
		/// <param name="anchorType">Anchor type.</param>
		public HUDAnchor GetAnchor (HUDAnchorTypes anchorType) {
		
			return anchors[anchorType];
		}

		/// <summary>
		/// Gets a dictionary with the HUDAnchorTypes as keys identifying each IHUDAnchor suscribed to this IHUDController.
		/// </summary>
		/// <returns>The anchors.</returns>
		public Dictionary<HUDAnchorTypes,HUDAnchor> GetAnchors () { 

			return anchors;
		}

		/// <summary>
		/// Sets the default font.
		/// </summary>
		/// <param name="newDefaultFont">New default font.</param>
		public void SetDefaultFont (Font newDefaultFont) {
		
			defaultFont = newDefaultFont;
		}

		/// <summary>
		/// Gets the default font.
		/// </summary>
		/// <returns>The default font.</returns>
		public Font GetDefaultFont () {
		
			return defaultFont;
		}

		# endregion

		# region initialization

		/// <summary>
		/// A special initialization to be called only by Editor elements.
		/// </summary>
		public void EditorInitialize () {
		
			_Instance = this;
		}

		/// <summary>
		/// A special destruction to be called only by Editor elements.
		/// </summary>
		public void EditorDestroy () {

			DestroyImmediate (this.gameObject);
		}

		/// <summary>
		/// Caches the HUDAnchors this HUDController has as children in an array.
		/// </summary>
		public void EditorCacheAnchors () {
		
			List<HUDAnchor> anchors = new List<HUDAnchor> ();

			foreach (Transform child in GetComponent<Transform> ()) {
			
				HUDAnchor childAnchor = child.GetComponent<HUDAnchor> ();

				if (childAnchor!= null) { anchors.Add (childAnchor); }
			} 

			_tempCachedAnchors = anchors.ToArray ();
		}

		/// <summary>
		/// Initializes the anchor dictionary with cached anchors.
		/// </summary>
		public void InitializeAnchorDictionaryWithCachedAnchors () {
		
			_anchors = new Dictionary<HUDAnchorTypes, HUDAnchor> ();

			foreach (HUDAnchor anchor in _tempCachedAnchors) {
			
				_anchors [anchor.type] = anchor;
			}
		}

		/// <summary>
		/// Initialize this instance.
		/// </summary>
		private void Initialize () {

			_Instance = this;

			_events = new HUDEvents ();

			_initialized = true;
		}

		/// <summary>
		/// Initializes the collections of the HUDController.
		/// </summary>
		private void InitializeCollections () {
		
			_anchors = new Dictionary<HUDAnchorTypes, HUDAnchor> ();

			_elements = new Dictionary<string, HUDElement> ();

		}

		# endregion
		
		# region notify events

		/// <summary>
		/// Throws the events notifying that the HUDController is initialized.
		/// </summary>
		private void NotifyInitialization () {

			Debug.Log ("Notifying init");
			events.NotifyHUDControllerInitialized (this);
			events.NotifyHUDInitialized ();
		}

		/// <summary>
		/// Throws the event that the HUDController has been disabled.
		/// </summary>
		private void NotifyDisabled () {
		
			events.NotifyHUDControllerDisabled ();
		}

		/// <summary>
		/// Throws the event that the HUDController has been destroyed.
		/// </summary>
		private void NotifyDestroyed () {
		
			events.NotifyHUDDestroyed ();
		}


		# endregion

		# region anchors

		/// <summary>
		/// A HUDAnchor subscribes to this HUDController as a given anchor type.
		/// </summary>
		/// <param name="type">The type of the anchor.</param>
		/// <param name="anchor">The anchor object.</param>
		public void SubscribeAs (HUDAnchorTypes type, HUDAnchor anchor) {

			if (_anchors == null) { _anchors = new Dictionary<HUDAnchorTypes, HUDAnchor> (); }

			_anchors [type] = anchor;
		}


		# endregion

		# region elements

		/// <summary>
		/// A <see cref="CustomHUD.HUDElement"/> wants to subscribe to this <see cref="CustomHUD.HUDController"/>.
		/// </summary>
		/// <param name="element">Element to subscribe.</param>
		public void SubscribeAs (HUDElement element) {

			switch (element.type) {

			case HUDElementTypes.Subtitle:		SubscribeAsSubtitle ((HUDSubtitle) element); 			break;
			case HUDElementTypes.WarningPanel: 	SubscribeAsWarningPanel ((HUDWarningPanel) element); 	break;
			case HUDElementTypes.HintPanel: 	SubscribeAsHintPanel ((HUDHintPanel) element); 			break;
			case HUDElementTypes.ProgressBar:	SubscribeAsProgressBar ((HUDProgressBar) element);		break;
			}
		}

		/// <summary>
		/// A <see cref="CustomHUD.HUDSubtitle"/> wants to subscribe to this <see cref="CustomHUD.HUDController"/>.
		/// </summary>
		/// <param name="element">Element to subscribe.</param>
		public void SubscribeAsSubtitle (HUDSubtitle element) {
			
			if (_elements.ContainsKey (element.subscribeAs.ToString ())) {
				
				
				Debug.LogWarning ("HUDController: Overwriting previously subscribed " + element.subscribeAs.ToString ());
			}
			
			_elements [element.subscribeAs.ToString ()] = element;
		}

		/// <summary>
		/// A <see cref="CustomHUD.HUDWarningPanel"/> wants to subscribe to this <see cref="CustomHUD.HUDController"/>.
		/// </summary>
		/// <param name="element">Element to subscribe.</param>
		public void SubscribeAsWarningPanel (HUDWarningPanel element) {
		
			if (_elements.ContainsKey (element.type.ToString ())) {
			
				Debug.LogWarning ("HudController: Overwriting previously subscribed " + element.type.ToString ());
			}
			
			_elements[element.type.ToString ()] = element;

		}

		/// <summary>
		/// A <see cref="CustomHUD.HUDHintPanel"/> wants to subscribe to this <see cref="CustomHUD.HUDController"/>.
		/// </summary>
		/// <param name="element">Element to subscribe.</param>
		public void SubscribeAsHintPanel (HUDHintPanel element) {

			if (_elements.ContainsKey (element.subscribeAs.ToString ())) {
			

				Debug.LogWarning ("HUDController: Overwriting previously subscribed " + element.subscribeAs.ToString ());
			}

			_elements [element.subscribeAs.ToString ()] = element;
		}

		/// <summary>
		/// A <see cref="CustomHUD.HUDProgressBar"/> wants to subscribe to this <see cref="CustomHUD.HUDController"/>.
		/// </summary>
		/// <param name="element">Element to subscribe.</param>
		public void SubscribeAsProgressBar (HUDProgressBar element) {
		
			if (_elements.ContainsKey (element.subscribeAs.ToString ())) {
				
				
				Debug.LogWarning ("HUDController: Overwriting previously subscribed " + element.subscribeAs.ToString ());
			}
			
			_elements [element.subscribeAs.ToString ()] = element;
		}

		# endregion

		# region api calls

		// general purpose api calls

		/// <summary>
		/// Shows a <see cref="CustomHUD.HUDElement"/> registered to this <see cref="CustomHUD.HUDController"/>, given its key.
		/// </summary>
		/// <param name="elementKey">Element key.</param>
		public HUDElement ShowElement (string elementKey) {
		
			if (_elements.ContainsKey (elementKey)) {
				
				HUDElement elem = _elements [elementKey];
				
				if (elem.GetType ().GetInterface ("IHUDShowable") == typeof(IHUDShowable) && !((IHUDShowable) elem).isCurrentlyShown) {
					
					((IHUDShowable) elem).Show ();
				}
				else {

					// if it doesn't implement showable, just set its GameObject active
					elem.gameObject.SetActive (true);
				}

				return elem;
			}
			else {

				Debug.LogWarning ("HUDController: There is no HUDElement subscribed to this controller as " + elementKey);
				return null;
			}
		}

		/// <summary>
		/// Shows a given <see cref="CustomHUD.HUDElement"/>.
		/// </summary>
		/// <returns>The element.</returns>
		/// <param name="element">Element.</param>
		public HUDElement ShowElement (HUDElement element) {

			if (element.GetType ().GetInterface ("IHUDShowable") == typeof(IHUDShowable) && !((IHUDShowable) element).isCurrentlyShown) {
				
				((IHUDShowable) element).Show ();
			}
			else {
				
				// if it doesn't implement showable, just set its GameObject active
				element.gameObject.SetActive (true);
			}
			
			return element;
		}

		/// <summary>
		/// Hides a <see cref="CustomHUD.HUDElement"/> registered to this <see cref="CustomHUD.HUDController"/>, given its key.
		/// </summary>
		/// <param name="elementKey">Element key.</param>
		public HUDElement HideElement (string elementKey) {

			if (_elements.ContainsKey (elementKey)) {
				
				HUDElement elem = _elements [elementKey];
				
				if (elem.GetType ().GetInterface ("IHUDShowable") == typeof(IHUDShowable) && ((IHUDShowable) elem).isCurrentlyShown) {
					
					((IHUDShowable) elem).Hide ();
				}
				else {
					
					// if it doesn't implement showable, just set its GameObject active
					elem.gameObject.SetActive (false);
				}

				return elem;
			}
			else {
				
				Debug.LogWarning ("HUDController: There is no HUDElement subscribed to this controller as " + elementKey);
				return null;
			}
		}

		/// <summary>
		/// Hides a given <see cref="CustomHUD.HUDElement"/>.
		/// </summary>
		/// <returns>The element.</returns>
		/// <param name="element">Element.</param>
		public HUDElement HideElement (HUDElement element) {
		
			if (element.GetType ().GetInterface ("IHUDShowable") == typeof(IHUDShowable) && ((IHUDShowable) element).isCurrentlyShown) {
				
				((IHUDShowable) element).Hide ();
			}
			else {
				
				// if it doesn't implement showable, just set its GameObject active
				element.gameObject.SetActive (false);
			}
			
			return element;
		}

		/// <summary>
		/// Shows a <see cref="CustomHUD.HUDElement"/> registered to this <see cref="CustomHUD.HUDController"/>, given its key, and hides it after some seconds have passed.
		/// </summary>
		/// <param name="elementKey">Element key.</param>
		/// <param name="secondsToHide">Time in seconds to hide the element after it has been shown.</param>
		public HUDElement ShowAndHideElement (string elementKey, float secondsToHide) {
		
			if (_elements.ContainsKey (elementKey)) {
				
				HUDElement elem = _elements [elementKey];
				
				if (elem.GetType ().GetInterface ("IHUDShowable") == typeof(IHUDShowable) && !((IHUDShowable) elem).isCurrentlyShown) {
					
					((IHUDShowable) elem).ShowAndHide (secondsToHide);
				}
				else {
					
					// if it doesn't implement showable, just set its GameObject active
					elem.gameObject.SetActive (true);

					StartCoroutine (CoroutineHideAfterSeconds (elem, secondsToHide));
				}

				return elem;
			}
			else {
				
				Debug.LogWarning ("HUDController: There is no HUDElement subscribed to this controller as " + elementKey);
				return null;
			}
		}

		/// <summary>
		/// Shows a given <see cref="CustomHUD.HUDElement"/> and hides it after some seconds have passed.
		/// </summary>
		/// <returns>The element.</returns>
		/// <param name="element">Element.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDElement ShowAndHideElement (HUDElement element, float secondsToHide) {
		
			if (element.GetType ().GetInterface ("IHUDShowable") == typeof(IHUDShowable) && !((IHUDShowable) element).isCurrentlyShown) {
				
				((IHUDShowable) element).ShowAndHide (secondsToHide);
			}
			else {
				
				// if it doesn't implement showable, just set its GameObject active
				element.gameObject.SetActive (true);
				
				StartCoroutine (CoroutineHideAfterSeconds (element, secondsToHide));
			}
			
			return element;
		}

		/// <summary>
		/// Hides an <see cref="CustomHUD.HUDElement"/> by disabling it after some seconds have passed.
		/// </summary>
		/// <returns>Enumerator.</returns>
		/// <param name="elem">Element.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		private IEnumerator CoroutineHideAfterSeconds (HUDElement elem, float secondsToHide) {
		
			yield return new WaitForSeconds (secondsToHide);

			elem.gameObject.SetActive (false);
		}

		//
		// HUDSubtitle calls
		//

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed to this <see cref="CustomHUD.HUDController"/> as a given type.
		/// </summary>
		/// <returns>The subtitle.</returns>
		/// <param name="subtitleType">Subtitle type.</param>
		public HUDSubtitle ShowSubtitle (HUDSubtitleTypes subtitleType) {
		
			if (_elements.ContainsKey (subtitleType.ToString ()) && _elements [subtitleType.ToString ()] != null) {
				
				return (HUDSubtitle) ShowElement (_elements[subtitleType.ToString ()]);
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed to this <see cref="CustomHUD.HUDController"/> as a given type with a new text.
		/// </summary>
		/// <returns>The subtitle.</returns>
		/// <param name="subtitleType">Subtitle type.</param>
		/// <param name="newSubtitleText">New subtitle text.</param>
		public HUDSubtitle ShowSubtitle (HUDSubtitleTypes subtitleType, string newSubtitleText) {
			
			if (_elements.ContainsKey (subtitleType.ToString ()) && _elements [subtitleType.ToString ()] != null) {
				
				HUDSubtitle sub =  (HUDSubtitle) _elements[subtitleType.ToString ()];
				sub.text = newSubtitleText;
				
				ShowElement (sub);
				return sub;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed to this <see cref="CustomHUD.HUDController"/> as a given type and hides it after some time has passed.
		/// </summary>
		/// <returns>The and hide hint.</returns>
		/// <param name="subtitleType">Subtitle type.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDSubtitle ShowAndHideSubtitle (HUDSubtitleTypes subtitleType, float secondsToHide) {
			
			if (_elements.ContainsKey (subtitleType.ToString ()) && _elements [subtitleType.ToString ()] != null) {
				
				HUDSubtitle sub =  (HUDSubtitle) _elements[subtitleType.ToString ()];
				
				ShowAndHideElement (sub, secondsToHide);
				return sub;
			}
			else {
				
				return null;
			}
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed to this <see cref="CustomHUD.HUDController"/> as a given type with a new text and hides it after some time has passed.
		/// </summary>
		public HUDSubtitle ShowAndHideSubtitle (HUDSubtitleTypes subtitleType, string newSubtitleText, float secondsToHide) {
			
			if (_elements.ContainsKey (subtitleType.ToString ()) && _elements [subtitleType.ToString ()] != null) {
				
				HUDSubtitle sub =  (HUDSubtitle) _elements[subtitleType.ToString ()];
				sub.text = newSubtitleText;
				
				ShowAndHideElement (sub, secondsToHide);
				return sub;
			}
			else {
				
				return null;
			}
		}

		
		/// <summary>
		/// Hides the <see cref="CustomHUD.HUDSubtitle"/> subscribed to this <see cref="CustomHUD.HUDController"/> as a given type.
		/// </summary>
		/// <returns>The subtitle.</returns>
		/// <param name="subtitleType">Subtitle type.</param>
		public HUDSubtitle HideSubtitle (HUDSubtitleTypes subtitleType) {
			
			if (_elements.ContainsKey (subtitleType.ToString ()) && _elements [subtitleType.ToString ()] != null) {
				
				return (HUDSubtitle) HideElement ( _elements [subtitleType.ToString ()]);
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Sets the text of the <see cref="CustomHUD.HUDSubtitle"/> subscribed to this <see cref="CustomHUD.HUDController"/> as a given type regardless of it is currently shown or not.
		/// </summary>
		/// <returns>The subtitle.</returns>
		/// <param name="subtitleType">Subtitle type.</param>
		/// <param name="newSubtitleText">New subtitle text.</param>
		public HUDSubtitle SetSubtitleText (HUDSubtitleTypes subtitleType, string newSubtitleText) {
		
			if (_elements.ContainsKey (subtitleType.ToString ()) && _elements [subtitleType.ToString ()] != null) {
				
				HUDSubtitle sub =  (HUDSubtitle) _elements[subtitleType.ToString ()];
				sub.text = newSubtitleText;

				return sub;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Primary subtitle.
		/// </summary>
		/// <returns>The Primary subtitle.</returns>
		public HUDSubtitle ShowPrimarySubtitle () {

			return ShowSubtitle (HUDSubtitleTypes.PrimarySubtitle);
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Primary subtitle with a new given text.
		/// </summary>
		/// <returns>The Primary subtitle.</returns>
		/// <param name="newSubtitleText">New subtitle text.</param>
		public HUDSubtitle ShowPrimarySubtitle (string newSubtitleText) {
		
			return ShowSubtitle (HUDSubtitleTypes.PrimarySubtitle, newSubtitleText);
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Primary subtitle and hides it after a given seconds have passed.
		/// </summary>
		/// <returns>The Primary subtitle.</returns>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDSubtitle ShowAndHidePrimarySubtitle (float secondsToHide) {
		
			return ShowAndHideSubtitle (HUDSubtitleTypes.PrimarySubtitle, secondsToHide);
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Primary subtitle with a new text and hides it after a given seconds have passed.
		/// </summary>
		/// <returns>The Primary subtitle.</returns>
		/// <param name="newSubtitleText">New subtitle text.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDSubtitle ShowAndHidePrimarySubtitle (string newSubtitleText, float secondsToHide) {
		
			return ShowAndHideSubtitle (HUDSubtitleTypes.PrimarySubtitle, newSubtitleText, secondsToHide);
		}

		/// <summary>
		/// Hides the <see cref="CustomHUD.HUDSubtitle"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Primary subtitle.
		/// </summary>
		/// <returns>The Primary subtitle.</returns>
		public HUDSubtitle HidePrimarySubtitle () {
		
			return HideSubtitle (HUDSubtitleTypes.PrimarySubtitle);
		}

		/// <summary>
		/// Sets the text of the <see cref="CustomHUD.HUDSubtitle"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Primary subtitle, regardless of it is currently shown or not.
		/// </summary>
		/// <returns>The Primary subtitle text.</returns>
		/// <param name="newSubtitleText">New subtitle text.</param>
		public HUDSubtitle SetPrimarySubtitleText (string newSubtitleText) {
		
			return SetSubtitleText (HUDSubtitleTypes.PrimarySubtitle, newSubtitleText);
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Secondary subtitle.
		/// </summary>
		/// <returns>The Secondary subtitle.</returns>
		public HUDSubtitle ShowSecondarySubtitle () {
			
			return ShowSubtitle (HUDSubtitleTypes.SecondarySubtitle);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Secondary subtitle with a new given text.
		/// </summary>
		/// <returns>The Secondary subtitle.</returns>
		/// <param name="newSubtitleText">New subtitle text.</param>
		public HUDSubtitle ShowSecondarySubtitle (string newSubtitleText) {
			
			return ShowSubtitle (HUDSubtitleTypes.SecondarySubtitle, newSubtitleText);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Secondary subtitle and hides it after a given seconds have passed.
		/// </summary>
		/// <returns>The Secondary subtitle.</returns>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDSubtitle ShowAndHideSecondarySubtitle (float secondsToHide) {
			
			return ShowAndHideSubtitle (HUDSubtitleTypes.SecondarySubtitle, secondsToHide);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Secondary subtitle with a new text and hides it after a given seconds have passed.
		/// </summary>
		/// <returns>The Secondary subtitle.</returns>
		/// <param name="newSubtitleText">New subtitle text.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDSubtitle ShowAndHideSecondarySubtitle (string newSubtitleText, float secondsToHide) {
			
			return ShowAndHideSubtitle (HUDSubtitleTypes.SecondarySubtitle, newSubtitleText, secondsToHide);
		}
		
		/// <summary>
		/// Hides the <see cref="CustomHUD.HUDSubtitle"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Secondary subtitle.
		/// </summary>
		/// <returns>The Secondary subtitle.</returns>
		public HUDSubtitle HideSecondarySubtitle () {
			
			return HideSubtitle (HUDSubtitleTypes.SecondarySubtitle);
		}
		
		/// <summary>
		/// Sets the text of the <see cref="CustomHUD.HUDSubtitle"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Secondary subtitle, regardless of it is currently shown or not.
		/// </summary>
		/// <returns>The Secondary subtitle text.</returns>
		/// <param name="newSubtitleText">New subtitle text.</param>
		public HUDSubtitle SetSecondarySubtitleText (string newSubtitleText) {
			
			return SetSubtitleText (HUDSubtitleTypes.SecondarySubtitle, newSubtitleText);
		}


		//
		// HUDWarningPanel calls
		//

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDWarningPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/>.
		/// </summary>
		/// <returns>The warning.</returns>
		public HUDWarningPanel ShowWarning () {
			
			if (_elements.ContainsKey (HUDElementTypes.WarningPanel.ToString ()) && _elements [HUDElementTypes.WarningPanel.ToString ()] != null) {
			
				return (HUDWarningPanel) ShowElement (_elements[HUDElementTypes.WarningPanel.ToString ()]);
			}
			else {

				return null;
			}
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDWarningPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> with a new given text.
		/// </summary>
		/// <returns>The warning.</returns>
		/// <param name="newWarningText">New warning text.</param>
		public HUDWarningPanel ShowWarning (string newWarningText) {
		
			if (_elements.ContainsKey (HUDElementTypes.WarningPanel.ToString ()) && _elements [HUDElementTypes.WarningPanel.ToString ()] != null) {
				
				HUDWarningPanel warning = (HUDWarningPanel) _elements [HUDElementTypes.WarningPanel.ToString ()];

				warning.text = newWarningText;
				ShowElement (warning);
			
				return warning;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDWarningPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> with a new given sprite.
		/// </summary>
		/// <returns>The warning.</returns>
		/// <param name="newWarningSprite">New warning sprite.</param>
		public HUDWarningPanel ShowWarning (Sprite newWarningSprite) {
			
			if (_elements.ContainsKey (HUDElementTypes.WarningPanel.ToString ()) && _elements [HUDElementTypes.WarningPanel.ToString ()] != null) {
				
				HUDWarningPanel warning = (HUDWarningPanel) _elements [HUDElementTypes.WarningPanel.ToString ()];
				
				warning.icon = newWarningSprite;
				ShowElement (warning);
			
				return warning;
			}
			else {
				
				return null;
			}
		}


		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDWarningPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> with a new given text and a new given sprite.
		/// </summary>
		/// <returns>The warning.</returns>
		/// <param name="newWarningText">New warning text.</param>
		/// <param name="newWarningSprite">New warning sprite.</param>
		public HUDWarningPanel ShowWarning (string newWarningText, Sprite newWarningSprite) {
			
			if (_elements.ContainsKey (HUDElementTypes.WarningPanel.ToString ()) && _elements [HUDElementTypes.WarningPanel.ToString ()] != null) {
				
				HUDWarningPanel warning = (HUDWarningPanel) _elements [HUDElementTypes.WarningPanel.ToString ()];

				warning.text = newWarningText;
				warning.icon = newWarningSprite;
				ShowElement (warning);

				return warning;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDWarningPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> and hides it after a specified time has passed.
		/// </summary>
		/// <returns>The warning.</returns>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDWarningPanel ShowAndHideWarning (float secondsToHide) {
			
			if (_elements.ContainsKey (HUDElementTypes.WarningPanel.ToString ()) && _elements [HUDElementTypes.WarningPanel.ToString ()] != null) {
				
				return (HUDWarningPanel) ShowAndHideElement (_elements[HUDElementTypes.WarningPanel.ToString ()], secondsToHide);
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDWarningPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> with a new given text and hides it after a specified time has passed.
		/// </summary>
		/// <returns>The warning.</returns>
		/// <param name="newWarningText">New warning text.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDWarningPanel ShowAndHideWarning (string newWarningText, float secondsToHide) {
			
			if (_elements.ContainsKey (HUDElementTypes.WarningPanel.ToString ()) && _elements [HUDElementTypes.WarningPanel.ToString ()] != null) {
				
				HUDWarningPanel warning = (HUDWarningPanel) _elements [HUDElementTypes.WarningPanel.ToString ()];
				
				warning.text = newWarningText;
				ShowAndHideElement (warning, secondsToHide);

				return warning;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDWarningPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> with a new given sprite and hides it after a specified time has passed.
		/// </summary>
		/// <returns>The and hide warning.</returns>
		/// <param name="newWarningSprite">New warning sprite.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDWarningPanel ShowAndHideWarning (Sprite newWarningSprite, float secondsToHide) {
			
			if (_elements.ContainsKey (HUDElementTypes.WarningPanel.ToString ()) && _elements [HUDElementTypes.WarningPanel.ToString ()] != null) {
				
				HUDWarningPanel warning = (HUDWarningPanel) _elements [HUDElementTypes.WarningPanel.ToString ()];
				
				warning.icon = newWarningSprite;
				ShowAndHideElement (warning, secondsToHide);

				return warning;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDWarningPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> with a new given text and a new given sprite and hides it after a specified time has passed.
		/// </summary>
		/// <returns>The warning.</returns>
		/// <param name="newWarningText">New warning text.</param>
		/// <param name="newWarningSprite">New warning sprite.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDWarningPanel ShowAndHideWarning (string newWarningText, Sprite newWarningSprite, float secondsToHide) {
			
			if (_elements.ContainsKey (HUDElementTypes.WarningPanel.ToString ()) && _elements [HUDElementTypes.WarningPanel.ToString ()] != null) {
				
				HUDWarningPanel warning = (HUDWarningPanel) _elements [HUDElementTypes.WarningPanel.ToString ()];
				
				warning.text = newWarningText;
				warning.icon = newWarningSprite;
				ShowAndHideElement (warning, secondsToHide);
			
				return warning;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Hides the <see cref="CustomHUD.HUDWarningPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/>.
		/// </summary>
		/// <returns>The warning.</returns>
		public HUDWarningPanel HideWarning () {
			
			if (_elements.ContainsKey (HUDElementTypes.WarningPanel.ToString ()) && _elements [HUDElementTypes.WarningPanel.ToString ()] != null) {
				
				return (HUDWarningPanel) HideElement ( _elements [HUDElementTypes.WarningPanel.ToString ()]);
			}
			else {
				
				return null;
			}
		}

		//
		// HUDHintPanel calls
		//

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> as a given type.
		/// </summary>
		/// <returns>The hint.</returns>
		/// <param name="hintType">Hint type.</param>
		public HUDHintPanel ShowHint (HUDHintTypes hintType) {
			
			if (_elements.ContainsKey (hintType.ToString ()) && _elements [hintType.ToString ()] != null) {
				
				return (HUDHintPanel) ShowElement (_elements[hintType.ToString ()]);
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> as a given type with a new text.
		/// </summary>
		/// <returns>The hint.</returns>
		/// <param name="hintType">Hint type.</param>
		/// <param name="newHintText">New hint text.</param>
		public HUDHintPanel ShowHint (HUDHintTypes hintType, string newHintText) {
			
			if (_elements.ContainsKey (hintType.ToString ()) && _elements [hintType.ToString ()] != null) {
				
				HUDHintPanel hint =  (HUDHintPanel) _elements[hintType.ToString ()];
				hint.text = newHintText;
				
				ShowElement (hint);
				return hint;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> as a given type with a new sprite.
		/// </summary>
		/// <returns>The hint.</returns>
		/// <param name="hintType">Hint type.</param>
		/// <param name="newHintSprite">New hint sprite.</param>
		public HUDHintPanel ShowHint (HUDHintTypes hintType, Sprite newHintSprite) {
			
			if (_elements.ContainsKey (hintType.ToString ()) && _elements [hintType.ToString ()] != null) {
				
				HUDHintPanel hint =  (HUDHintPanel) _elements[hintType.ToString ()];
				hint.icon = newHintSprite;
				
				ShowElement (hint);
				return hint;
			}
			else {
				
				return null;
			}
		} 

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> as a given type with a new text and a new sprite.
		/// </summary>
		/// <returns>The hint.</returns>
		/// <param name="hintType">Hint type.</param>
		/// <param name="newHintText">New hint text.</param>
		/// <param name="newHintSprite">New hint sprite.</param>
		public HUDHintPanel ShowHint (HUDHintTypes hintType, string newHintText, Sprite newHintSprite) {
			
			if (_elements.ContainsKey (hintType.ToString ()) && _elements [hintType.ToString ()] != null) {
				
				HUDHintPanel hint =  (HUDHintPanel) _elements[hintType.ToString ()];
				hint.text = newHintText;
				hint.icon = newHintSprite;

				ShowElement (hint);
				return hint;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> as a given type and hides it after some time has passed.
		/// </summary>
		/// <returns>The hint.</returns>
		/// <param name="hintType">Hint type.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDHintPanel ShowAndHideHint (HUDHintTypes hintType, float secondsToHide) {
			
			if (_elements.ContainsKey (hintType.ToString ()) && _elements [hintType.ToString ()] != null) {
				
				HUDHintPanel hint =  (HUDHintPanel) _elements[hintType.ToString ()];
				
				ShowAndHideElement (hint, secondsToHide);
				return hint;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> as a given type with a new text and hides it after some time has passed.
		/// </summary>
		/// <returns>The hint.</returns>
		/// <param name="hintType">Hint type.</param>
		/// <param name="newHintText">New hint text.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDHintPanel ShowAndHideHint (HUDHintTypes hintType, string newHintText, float secondsToHide) {
			
			if (_elements.ContainsKey (hintType.ToString ()) && _elements [hintType.ToString ()] != null) {
				
				HUDHintPanel hint =  (HUDHintPanel) _elements[hintType.ToString ()];
				hint.text = newHintText;
				
				ShowAndHideElement (hint, secondsToHide);
				return hint;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> as a given type with a new sprite and hides it after some time has passed.
		/// </summary>
		/// <returns>The hint.</returns>
		/// <param name="hintType">Hint type.</param>
		/// <param name="newHintSprite">New hint sprite.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDHintPanel ShowAndHideHint (HUDHintTypes hintType, Sprite newHintSprite, float secondsToHide) {
			
			if (_elements.ContainsKey (hintType.ToString ()) && _elements [hintType.ToString ()] != null) {
				
				HUDHintPanel hint =  (HUDHintPanel) _elements[hintType.ToString ()];
				hint.icon = newHintSprite;
				
				ShowAndHideElement (hint, secondsToHide);
				return hint;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> as a given type with a new text and a new sprite and hides it after some time has passed.
		/// </summary>
		/// <returns>The hint.</returns>
		/// <param name="hintType">Hint type.</param>
		/// <param name="newHintText">New hint text.</param>
		/// <param name="newHintSprite">New hint sprite.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDHintPanel ShowAndHideHint (HUDHintTypes hintType, string newHintText, Sprite newHintSprite, float secondsToHide) {
			
			if (_elements.ContainsKey (hintType.ToString ()) && _elements [hintType.ToString ()] != null) {
				
				HUDHintPanel hint =  (HUDHintPanel) _elements[hintType.ToString ()];
				hint.text = newHintText;
				hint.icon = newHintSprite;
				
				ShowAndHideElement (hint, secondsToHide);
				return hint;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Hides the <see cref="CustomHUD.HUDHintPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> as a given type.
		/// </summary>
		/// <returns>The hint.</returns>
		/// <param name="hintType">Hint type.</param>
		public HUDHintPanel HideHint (HUDHintTypes hintType) {
			
			if (_elements.ContainsKey (hintType.ToString ()) && _elements [hintType.ToString ()] != null) {
				
				return (HUDHintPanel) HideElement ( _elements [hintType.ToString ()]);
			}
			else {
				
				return null;
			}
		}

		// ACTION HUDHintPanel calls

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> suscribed to this <see cref="CustomHUD.HUDController"/> as Action hint.
		/// </summary>
		/// <returns>The action hint.</returns>
		public HUDHintPanel ShowActionHint () {
			
			return ShowHint (HUDHintTypes.ActionHint);
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> suscribed to this <see cref="CustomHUD.HUDController"/> as Action hint with a new text.
		/// </summary>
		/// <returns>The action hint.</returns>
		/// <param name="newHintText">New hint text.</param>
		public HUDHintPanel ShowActionHint (string newHintText) {
			
			return ShowHint (HUDHintTypes.ActionHint, newHintText);
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> suscribed to this <see cref="CustomHUD.HUDController"/> as Action hint with a new sprite.
		/// </summary>
		/// <returns>The action hint.</returns>
		/// <param name="newHintSprite">New hint sprite.</param>
		public HUDHintPanel ShowActionHint (Sprite newHintSprite) {
			
			return ShowHint (HUDHintTypes.ActionHint, newHintSprite);
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> suscribed to this <see cref="CustomHUD.HUDController"/> as Action hint with a new text and a new sprite.
		/// </summary>
		/// <returns>The action hint.</returns>
		/// <param name="newHintText">New hint text.</param>
		/// <param name="newHintSprite">New hint sprite.</param>
		public HUDHintPanel ShowActionHint (string newHintText, Sprite newHintSprite) {
			
			return ShowHint (HUDHintTypes.ActionHint, newHintText, newHintSprite);
		}
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Action hint and hides it after some seconds have passed.
		/// </summary>
		/// <returns>The action hint.</returns>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDHintPanel ShowAndHideActionHint (float secondsToHide) {
			
			return ShowAndHideHint (HUDHintTypes.ActionHint, secondsToHide);
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Action hint with a new text and hides it after some seconds have passed.
		/// </summary>
		/// <returns>The and hide action hint.</returns>
		/// <param name="newHintText">New hint text.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDHintPanel ShowAndHideActionHint (string newHintText, float secondsToHide) {
			
			return ShowAndHideHint (HUDHintTypes.ActionHint, newHintText, secondsToHide);
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Action hint with a new sprite and hides it after some seconds have passed.
		/// </summary>
		/// <returns>The action hint.</returns>
		/// <param name="newHintSprite">New hint sprite.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDHintPanel ShowAndHideActionHint (Sprite newHintSprite, float secondsToHide) {
			
			return ShowAndHideHint (HUDHintTypes.ActionHint, newHintSprite, secondsToHide);
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Action hint with a new text and a new sprite and hides it after some seconds have passed.
		/// </summary>
		/// <returns>The action hint.</returns>
		/// <param name="newHintText">New hint text.</param>
		/// <param name="newHintSprite">New hint sprite.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDHintPanel ShowAndHideActionHint (string newHintText, Sprite newHintSprite, float secondsToHide) {
			
			return ShowAndHideHint (HUDHintTypes.ActionHint, newHintText, newHintSprite, secondsToHide);
		}

		/// <summary>
		/// Hides the <see cref="CustomHUD.HUDHintPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Action hint.
		/// </summary>
		/// <returns>The action hint.</returns>
		public HUDHintPanel HideActionHint () {
			
			return HideHint (HUDHintTypes.ActionHint);	
		}

		//
		// REMINDER HUDHintPanel calls
		//

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> suscribed to this <see cref="CustomHUD.HUDController"/> as Reminder hint.
		/// </summary>
		/// <returns>The reminder hint.</returns>
		public HUDHintPanel ShowReminderHint () {
			
			return ShowHint (HUDHintTypes.ReminderHint);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> suscribed to this <see cref="CustomHUD.HUDController"/> as Reminder hint with a new text.
		/// </summary>
		/// <returns>The reminder hint.</returns>
		/// <param name="newHintText">New hint text.</param>
		public HUDHintPanel ShowReminderHint (string newHintText) {
			
			return ShowHint (HUDHintTypes.ReminderHint, newHintText);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> suscribed to this <see cref="CustomHUD.HUDController"/> as Reminder hint with a new sprite.
		/// </summary>
		/// <returns>The reminder hint.</returns>
		/// <param name="newHintSprite">New hint sprite.</param>
		public HUDHintPanel ShowReminderHint (Sprite newHintSprite) {
			
			return ShowHint (HUDHintTypes.ReminderHint, newHintSprite);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> suscribed to this <see cref="CustomHUD.HUDController"/> as Reminder hint with a new text and a new sprite.
		/// </summary>
		/// <returns>The reminder hint.</returns>
		/// <param name="newHintText">New hint text.</param>
		/// <param name="newHintSprite">New hint sprite.</param>
		public HUDHintPanel ShowReminderHint (string newHintText, Sprite newHintSprite) {
			
			return ShowHint (HUDHintTypes.ReminderHint, newHintText, newHintSprite);
		}
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Reminder hint and hides it after some seconds have passed.
		/// </summary>
		/// <returns>The reminder hint.</returns>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDHintPanel ShowAndHideReminderHint (float secondsToHide) {
			
			return ShowAndHideHint (HUDHintTypes.ReminderHint, secondsToHide);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Reminder hint with a new text and hides it after some seconds have passed.
		/// </summary>
		/// <returns>The and hide reminder hint.</returns>
		/// <param name="newHintText">New hint text.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDHintPanel ShowAndHideReminderHint (string newHintText, float secondsToHide) {
			
			return ShowAndHideHint (HUDHintTypes.ReminderHint, newHintText, secondsToHide);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Reminder hint with a new sprite and hides it after some seconds have passed.
		/// </summary>
		/// <returns>The reminder hint.</returns>
		/// <param name="newHintSprite">New hint sprite.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDHintPanel ShowAndHideReminderHint (Sprite newHintSprite, float secondsToHide) {
			
			return ShowAndHideHint (HUDHintTypes.ReminderHint, newHintSprite, secondsToHide);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Reminder hint with a new text and a new sprite and hides it after some seconds have passed.
		/// </summary>
		/// <returns>The reminder hint.</returns>
		/// <param name="newHintText">New hint text.</param>
		/// <param name="newHintSprite">New hint sprite.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDHintPanel ShowAndHideReminderHint (string newHintText, Sprite newHintSprite, float secondsToHide) {
			
			return ShowAndHideHint (HUDHintTypes.ReminderHint, newHintText, newHintSprite, secondsToHide);
		}
		
		/// <summary>
		/// Hides the <see cref="CustomHUD.HUDHintPanel"/> subscribed to this <see cref="CustomHUD.HUDController"/> as Reminder hint.
		/// </summary>
		/// <returns>The reminder hint.</returns>
		public HUDHintPanel HideReminderHint () {
			
			return HideHint (HUDHintTypes.ReminderHint);	
		}


		//
		// HUDProgressBar calls
		//

		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as the given type to a given value.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetProgressBarValue (HUDProgressBarTypes barType, float newValue) {
			
			if (_elements.ContainsKey (barType.ToString()) && _elements[barType.ToString ()] != null) {

				HUDProgressBar bar = (HUDProgressBar) _elements[barType.ToString ()];

				bar.value = newValue;
				return bar;
			}
			else {

				return null;
			}
		}

		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as the given type to a given value.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetProgressBarValue (HUDProgressBarTypes barType, int newValue) {
		
			return 	SetProgressBarValue (barType, (float)newValue);
		}

		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as the given type to a given value.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetProgressBarMaximumValue (HUDProgressBarTypes barType, float newValue) {
		
			if (_elements.ContainsKey (barType.ToString()) && _elements[barType.ToString ()] != null) {
				
				HUDProgressBar bar = (HUDProgressBar) _elements[barType.ToString ()];
				
				bar.maxValue = newValue;
				return bar;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as the given type to a given value.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetProgressBarMaximumValue (HUDProgressBarTypes barType, int newValue) {
		
			return SetProgressBarMaximumValue (barType, (float)newValue);
		}

		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as the given type to a given value.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetProgressBarMinimumValue (HUDProgressBarTypes barType, float newValue) {
			
			if (_elements.ContainsKey (barType.ToString()) && _elements[barType.ToString ()] != null) {
				
				HUDProgressBar bar = (HUDProgressBar) _elements[barType.ToString ()];
				
				bar.minValue = newValue;
				return bar;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as the given type to a given value.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetProgressBarMinimumValue (HUDProgressBarTypes barType, int newValue) {
		
			return SetProgressBarMinimumValue (barType, newValue);
		}

		/// <summary>
		/// Shows the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as the given type.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		public HUDProgressBar ShowProgressBar (HUDProgressBarTypes barType) {
		
			if (_elements.ContainsKey (barType.ToString ()) && _elements [barType.ToString ()] != null) {
				
				return (HUDProgressBar) ShowElement (_elements [barType.ToString ()]);
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Hides the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as the given type.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		public HUDProgressBar HideProgressBar (HUDProgressBarTypes barType) {
			
			if (_elements.ContainsKey (barType.ToString ()) && _elements [barType.ToString ()] != null) {
				
				return (HUDProgressBar) HideElement(_elements [barType.ToString ()]);
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Shows the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as the given type, and hides it after a given time has passed.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDProgressBar ShowAndHideProgressBar (HUDProgressBarTypes barType, float secondsToHide) {
			
			if (_elements.ContainsKey (barType.ToString ()) && _elements [barType.ToString ()] != null) {
				
				return (HUDProgressBar) ShowAndHideElement (_elements [barType.ToString ()], secondsToHide);
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as the given type, by the given increment.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="increment">Increment.</param>
		public HUDProgressBar IncreaseBarValue (HUDProgressBarTypes barType, float increment) {
		
			if (_elements.ContainsKey (barType.ToString()) && _elements[barType.ToString ()] != null) {
				
				HUDProgressBar bar = (HUDProgressBar) _elements[barType.ToString ()];
				
				bar.value = bar.value + increment;
				return bar;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as the given type, by the given increment.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="increment">Increment.</param>
		public HUDProgressBar IncreaseBarValue (HUDProgressBarTypes barType, int increment) {
		
			return IncreaseBarValue (barType, (float)increment);
		}

		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as the given type, by the given decrement.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="decrement">Decrement.</param>
		public HUDProgressBar DecreaseBarValue (HUDProgressBarTypes barType, float decrement) {
			
			return IncreaseBarValue (barType, -decrement);
		}

		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as the given type, by the given decrement.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="decrement">Decrement.</param>
		public HUDProgressBar DecreaseBarValue (HUDProgressBarTypes barType, int decrement) {
		
			return DecreaseBarValue (barType, (float)decrement);
		}

		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as the given type during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The progress bar..</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar SetBarValueOverTime (HUDProgressBarTypes barType, float targetValue, float secondsOfDuration) {
		
			if (_elements.ContainsKey (barType.ToString()) && _elements[barType.ToString ()] != null) {
				
				HUDProgressBar bar = (HUDProgressBar) _elements[barType.ToString ()];
				
				bar.SetValueOverTime (targetValue, secondsOfDuration);
				return bar;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as the given type during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar SetBarValueOverTime (HUDProgressBarTypes barType, int targetValue, float secondsOfDuration) {
			
			if (_elements.ContainsKey (barType.ToString()) && _elements[barType.ToString ()] != null) {
				
				HUDProgressBar bar = (HUDProgressBar) _elements[barType.ToString ()];
				
				bar.SetValueOverTime (targetValue, secondsOfDuration);
				return bar;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as the given type by the given ammount, during the specified time.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="increment">Increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar IncreaseBarValueOverTime (HUDProgressBarTypes barType, float increment, float secondsOfDuration) {
		
			if (_elements.ContainsKey (barType.ToString()) && _elements[barType.ToString ()] != null) {
				
				HUDProgressBar bar = (HUDProgressBar) _elements[barType.ToString ()];
				
				bar.IncreaseValueOverTime (increment, secondsOfDuration);
				return bar;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as the given type by the given ammount, during the specified time.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="increment">Increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar IncreaseBarValueOverTime (HUDProgressBarTypes barType, int increment, float secondsOfDuration) {
			
			if (_elements.ContainsKey (barType.ToString()) && _elements[barType.ToString ()] != null) {
				
				HUDProgressBar bar = (HUDProgressBar) _elements[barType.ToString ()];
				
				bar.IncreaseValueOverTime (increment, secondsOfDuration);
				return bar;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as the given type by the given ammount, during the specified time.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="decrement">decrease.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar DecreaseBarValueOverTime (HUDProgressBarTypes barType, float increment, float secondsOfDuration) {
			
			if (_elements.ContainsKey (barType.ToString()) && _elements[barType.ToString ()] != null) {
				
				HUDProgressBar bar = (HUDProgressBar) _elements[barType.ToString ()];
				
				bar.IncreaseValueOverTime (increment, secondsOfDuration);
				return bar;
			}
			else {
				
				return null;
			}
		}

		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as the given type by the given ammount, during the specified time.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="decrement">decrease.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar DecreaseBarValueOverTime (HUDProgressBarTypes barType, int increment, float secondsOfDuration) {
			
			if (_elements.ContainsKey (barType.ToString()) && _elements[barType.ToString ()] != null) {
				
				HUDProgressBar bar = (HUDProgressBar) _elements[barType.ToString ()];
				
				bar.IncreaseValueOverTime (increment, secondsOfDuration);
				return bar;
			}
			else {
				
				return null;
			}
		}


		//
		// HUD Health Progress Bar
		//

		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Health bar.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetHealthBarValue (float newValue) {
		
			return SetProgressBarValue (HUDProgressBarTypes.HealthBar, newValue);
		}

		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Health bar.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetHealthBarValue (int newValue) {
		
			return SetHealthBarValue ((float)newValue);
		}

		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Health bar.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetHealthBarMaximumValue (float newValue) {
		
			return SetProgressBarMaximumValue (HUDProgressBarTypes.HealthBar, newValue);
		}

		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Health bar.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetHealthBarMaximumValue (int newValue) {
		
			return SetHealthBarMaximumValue ((float)newValue);
		}

		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Health bar.
		/// </summary>
		/// <returns>The health bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetHealthBarMinimumValue (float newValue) {
		
			return SetProgressBarValue (HUDProgressBarTypes.HealthBar, newValue);
		}

		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Health bar.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetHealthBarMinimumValue (int newValue) {
		
			return SetHealthBarMinimumValue ((float)newValue);
		}

		/// <summary>
		/// Shows the Health bar.
		/// </summary>
		/// <returns>The Health bar.</returns>
		public HUDProgressBar ShowHealthBar () {
		
			return ShowProgressBar (HUDProgressBarTypes.HealthBar);
		}

		/// <summary>
		/// Hides the Health bar.
		/// </summary>
		/// <returns>The Health bar.</returns>
		public HUDProgressBar HideHealthBar () {

			return HideProgressBar (HUDProgressBarTypes.HealthBar);
		}

		/// <summary>
		/// Shows and hides the Health bar after some seconds have passed.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDProgressBar ShowAndHideHealthBar (float secondsToHide) {

			return ShowAndHideProgressBar (HUDProgressBarTypes.HealthBar, secondsToHide);
		}

		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Health bar by a given amount.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="increment">The value increment.</param>
		public HUDProgressBar IncreaseHealthBarValue (float increment) {
		
			return IncreaseBarValue (HUDProgressBarTypes.HealthBar, increment);
		}

		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Health bar by a given amount.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="increment">The value increment.</param>
		public HUDProgressBar IncreaseHealthBarValue (int increment) {
		
			return IncreaseBarValue (HUDProgressBarTypes.HealthBar, (int)increment);
		}

		/// <summary>
		/// Decreass the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Health bar by a given amount.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="decrement">The value decrement.</param>
		public HUDProgressBar DecreaseHealthBarValue (float decrement) {
		
			return DecreaseBarValue (HUDProgressBarTypes.HealthBar, decrement);
		}

		/// <summary>
		/// Decreass the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Health bar by a given amount.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="decrement">The value decrement.</param>
		public HUDProgressBar DecreaseHealthBarValue (int decrement) {
			
			return DecreaseBarValue (HUDProgressBarTypes.HealthBar, (float) decrement);
		}

		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Health bar during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar SetHealthBarValueOverTime (float targetValue, float secondsOfDuration) {
		
			return SetBarValueOverTime (HUDProgressBarTypes.HealthBar, targetValue, secondsOfDuration);
		}

		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Health bar during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar SetHealthBarValueOverTime (int targetValue, float secondsOfDuration) {
		
			return SetBarValueOverTime (HUDProgressBarTypes.HealthBar, targetValue, secondsOfDuration);
		}
	
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Health bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="increment">Value increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar IncreaseHealthBarValueOverTime (float increment, float secondsOfDuration) {
		
			return IncreaseBarValueOverTime (HUDProgressBarTypes.HealthBar, increment, secondsOfDuration);
		}

		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Health bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="increment">Value increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar IncreaseHealthBarValueOverTime (int increment, float secondsOfDuration) {
			
			return IncreaseBarValueOverTime (HUDProgressBarTypes.HealthBar, increment, secondsOfDuration);
		}

		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Health bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="decrement">Value decrement.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar DecreaseHealthBarValueOverTime (float decrement, float secondsOfDuration) {
			
			return DecreaseBarValueOverTime (HUDProgressBarTypes.HealthBar, decrement, secondsOfDuration);
		}

		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Health bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="decrement">Value decrement.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar DecreaseHealthBarValueOverTime (int decrement, float secondsOfDuration) {
			
			return DecreaseBarValueOverTime (HUDProgressBarTypes.HealthBar, decrement, secondsOfDuration);
		}

		//
		// Energy bar
		//
		
		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Energy bar.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetEnergyBarValue (float newValue) {
			
			return SetProgressBarValue (HUDProgressBarTypes.EnergyBar, newValue);
		}
		
		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Energy bar.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetEnergyBarValue (int newValue) {
			
			return SetEnergyBarValue ((float)newValue);
		}
		
		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Energy bar.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetEnergyBarMaximumValue (float newValue) {
			
			return SetProgressBarMaximumValue (HUDProgressBarTypes.EnergyBar, newValue);
		}
		
		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Energy bar.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetEnergyBarMaximumValue (int newValue) {
			
			return SetEnergyBarMaximumValue ((float)newValue);
		}
		
		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Energy bar.
		/// </summary>
		/// <returns>The health bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetEnergyBarMinimumValue (float newValue) {
			
			return SetProgressBarValue (HUDProgressBarTypes.EnergyBar, newValue);
		}
		
		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Energy bar.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetEnergyBarMinimumValue (int newValue) {
			
			return SetEnergyBarMinimumValue ((float)newValue);
		}
		
		/// <summary>
		/// Shows the Energy bar.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		public HUDProgressBar ShowEnergyBar () {
			
			return ShowProgressBar (HUDProgressBarTypes.EnergyBar);
		}
		
		/// <summary>
		/// Hides the Energy bar.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		public HUDProgressBar HideEnergyBar () {
			
			return HideProgressBar (HUDProgressBarTypes.EnergyBar);
		}
		
		/// <summary>
		/// Shows and hides the Energy bar after some seconds have passed.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDProgressBar ShowAndHideEnergyBar (float secondsToHide) {
			
			return ShowAndHideProgressBar (HUDProgressBarTypes.EnergyBar, secondsToHide);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Energy bar by a given amount.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="increment">The value increment.</param>
		public HUDProgressBar IncreaseEnergyBarValue (float increment) {
			
			return IncreaseBarValue (HUDProgressBarTypes.EnergyBar, increment);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Energy bar by a given amount.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="increment">The value increment.</param>
		public HUDProgressBar IncreaseEnergyBarValue (int increment) {
			
			return IncreaseBarValue (HUDProgressBarTypes.EnergyBar, (int)increment);
		}
		
		/// <summary>
		/// Decreass the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Energy bar by a given amount.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="decrement">The value decrement.</param>
		public HUDProgressBar DecreaseEnergyBarValue (float decrement) {
			
			return DecreaseBarValue (HUDProgressBarTypes.EnergyBar, decrement);
		}
		
		/// <summary>
		/// Decreass the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Energy bar by a given amount.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="decrement">The value decrement.</param>
		public HUDProgressBar DecreaseEnergyBarValue (int decrement) {
			
			return DecreaseBarValue (HUDProgressBarTypes.EnergyBar, (float) decrement);
		}
		
		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Energy bar during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar SetEnergyBarValueOverTime (float targetValue, float secondsOfDuration) {
			
			return SetBarValueOverTime (HUDProgressBarTypes.EnergyBar, targetValue, secondsOfDuration);
		}
		
		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Energy bar during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar SetEnergyBarValueOverTime (int targetValue, float secondsOfDuration) {
			
			return SetBarValueOverTime (HUDProgressBarTypes.EnergyBar, targetValue, secondsOfDuration);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Energy bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="increment">Value increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar IncreaseEnergyBarValueOverTime (float increment, float secondsOfDuration) {
			
			return IncreaseBarValueOverTime (HUDProgressBarTypes.EnergyBar, increment, secondsOfDuration);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Energy bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="increment">Value increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar IncreaseEnergyBarValueOverTime (int increment, float secondsOfDuration) {
			
			return IncreaseBarValueOverTime (HUDProgressBarTypes.EnergyBar, increment, secondsOfDuration);
		}
		
		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Energy bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="decrement">Value decrement.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar DecreaseEnergyBarValueOverTime (float decrement, float secondsOfDuration) {
			
			return DecreaseBarValueOverTime (HUDProgressBarTypes.EnergyBar, decrement, secondsOfDuration);
		}
		
		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Energy bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="decrement">Value decrement.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar DecreaseEnergyBarValueOverTime (int decrement, float secondsOfDuration) {
			
			return DecreaseBarValueOverTime (HUDProgressBarTypes.EnergyBar, decrement, secondsOfDuration);
		}

		//
		// Experience bar
		//

		
		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Experience bar.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetExperienceBarValue (float newValue) {
			
			return SetProgressBarValue (HUDProgressBarTypes.ExperienceBar, newValue);
		}
		
		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Experience bar.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetExperienceBarValue (int newValue) {
			
			return SetExperienceBarValue ((float)newValue);
		}
		
		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Experience bar.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetExperienceBarMaximumValue (float newValue) {
			
			return SetProgressBarMaximumValue (HUDProgressBarTypes.ExperienceBar, newValue);
		}
		
		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Experience bar.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetExperienceBarMaximumValue (int newValue) {
			
			return SetExperienceBarMaximumValue ((float)newValue);
		}
		
		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Experience bar.
		/// </summary>
		/// <returns>The health bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetExperienceBarMinimumValue (float newValue) {
			
			return SetProgressBarValue (HUDProgressBarTypes.ExperienceBar, newValue);
		}
		
		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Experience bar.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetExperienceBarMinimumValue (int newValue) {
			
			return SetExperienceBarMinimumValue ((float)newValue);
		}
		
		/// <summary>
		/// Shows the Experience bar.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		public HUDProgressBar ShowExperienceBar () {
			
			return ShowProgressBar (HUDProgressBarTypes.ExperienceBar);
		}
		
		/// <summary>
		/// Hides the Experience bar.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		public HUDProgressBar HideExperienceBar () {
			
			return HideProgressBar (HUDProgressBarTypes.ExperienceBar);
		}
		
		/// <summary>
		/// Shows and hides the Experience bar after some seconds have passed.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDProgressBar ShowAndHideExperienceBar (float secondsToHide) {
			
			return ShowAndHideProgressBar (HUDProgressBarTypes.ExperienceBar, secondsToHide);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Experience bar by a given amount.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="increment">The value increment.</param>
		public HUDProgressBar IncreaseExperienceBarValue (float increment) {
			
			return IncreaseBarValue (HUDProgressBarTypes.ExperienceBar, increment);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Experience bar by a given amount.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="increment">The value increment.</param>
		public HUDProgressBar IncreaseExperienceBarValue (int increment) {
			
			return IncreaseBarValue (HUDProgressBarTypes.ExperienceBar, (int)increment);
		}
		
		/// <summary>
		/// Decreass the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Experience bar by a given amount.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="decrement">The value decrement.</param>
		public HUDProgressBar DecreaseExperienceBarValue (float decrement) {
			
			return DecreaseBarValue (HUDProgressBarTypes.ExperienceBar, decrement);
		}
		
		/// <summary>
		/// Decreass the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Experience bar by a given amount.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="decrement">The value decrement.</param>
		public HUDProgressBar DecreaseExperienceBarValue (int decrement) {
			
			return DecreaseBarValue (HUDProgressBarTypes.ExperienceBar, (float) decrement);
		}
		
		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Experience bar during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar SetExperienceBarValueOverTime (float targetValue, float secondsOfDuration) {
			
			return SetBarValueOverTime (HUDProgressBarTypes.ExperienceBar, targetValue, secondsOfDuration);
		}
		
		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Experience bar during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar SetExperienceBarValueOverTime (int targetValue, float secondsOfDuration) {
			
			return SetBarValueOverTime (HUDProgressBarTypes.ExperienceBar, targetValue, secondsOfDuration);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Experience bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="increment">Value increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar IncreaseExperienceBarValueOverTime (float increment, float secondsOfDuration) {
			
			return IncreaseBarValueOverTime (HUDProgressBarTypes.ExperienceBar, increment, secondsOfDuration);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Experience bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="increment">Value increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar IncreaseExperienceBarValueOverTime (int increment, float secondsOfDuration) {
			
			return IncreaseBarValueOverTime (HUDProgressBarTypes.ExperienceBar, increment, secondsOfDuration);
		}
		
		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Experience bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="decrement">Value decrement.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar DecreaseExperienceBarValueOverTime (float decrement, float secondsOfDuration) {
			
			return DecreaseBarValueOverTime (HUDProgressBarTypes.ExperienceBar, decrement, secondsOfDuration);
		}
		
		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Experience bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="decrement">Value decrement.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar DecreaseExperienceBarValueOverTime (int decrement, float secondsOfDuration) {
			
			return DecreaseBarValueOverTime (HUDProgressBarTypes.ExperienceBar, decrement, secondsOfDuration);
		}

		//
		// Level bar
		//

		
		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Level bar.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetLevelBarValue (float newValue) {
			
			return SetProgressBarValue (HUDProgressBarTypes.LevelBar, newValue);
		}
		
		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Level bar.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetLevelBarValue (int newValue) {
			
			return SetLevelBarValue ((float)newValue);
		}
		
		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Level bar.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetLevelBarMaximumValue (float newValue) {
			
			return SetProgressBarMaximumValue (HUDProgressBarTypes.LevelBar, newValue);
		}
		
		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Level bar.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetLevelBarMaximumValue (int newValue) {
			
			return SetLevelBarMaximumValue ((float)newValue);
		}
		
		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Level bar.
		/// </summary>
		/// <returns>The health bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetLevelBarMinimumValue (float newValue) {
			
			return SetProgressBarValue (HUDProgressBarTypes.LevelBar, newValue);
		}
		
		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Level bar.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="newValue">New value.</param>
		public HUDProgressBar SetLevelBarMinimumValue (int newValue) {
			
			return SetLevelBarMinimumValue ((float)newValue);
		}
		
		/// <summary>
		/// Shows the Level bar.
		/// </summary>
		/// <returns>The Level bar.</returns>
		public HUDProgressBar ShowLevelBar () {
			
			return ShowProgressBar (HUDProgressBarTypes.LevelBar);
		}
		
		/// <summary>
		/// Hides the Level bar.
		/// </summary>
		/// <returns>The Level bar.</returns>
		public HUDProgressBar HideLevelBar () {
			
			return HideProgressBar (HUDProgressBarTypes.LevelBar);
		}
		
		/// <summary>
		/// Shows and hides the Level bar after some seconds have passed.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public HUDProgressBar ShowAndHideLevelBar (float secondsToHide) {
			
			return ShowAndHideProgressBar (HUDProgressBarTypes.LevelBar, secondsToHide);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Level bar by a given amount.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="increment">The value increment.</param>
		public HUDProgressBar IncreaseLevelBarValue (float increment) {
			
			return IncreaseBarValue (HUDProgressBarTypes.LevelBar, increment);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Level bar by a given amount.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="increment">The value increment.</param>
		public HUDProgressBar IncreaseLevelBarValue (int increment) {
			
			return IncreaseBarValue (HUDProgressBarTypes.LevelBar, (int)increment);
		}
		
		/// <summary>
		/// Decreass the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Level bar by a given amount.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="decrement">The value decrement.</param>
		public HUDProgressBar DecreaseLevelBarValue (float decrement) {
			
			return DecreaseBarValue (HUDProgressBarTypes.LevelBar, decrement);
		}
		
		/// <summary>
		/// Decreass the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Level bar by a given amount.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="decrement">The value decrement.</param>
		public HUDProgressBar DecreaseLevelBarValue (int decrement) {
			
			return DecreaseBarValue (HUDProgressBarTypes.LevelBar, (float) decrement);
		}
		
		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Level bar during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar SetLevelBarValueOverTime (float targetValue, float secondsOfDuration) {
			
			return SetBarValueOverTime (HUDProgressBarTypes.LevelBar, targetValue, secondsOfDuration);
		}
		
		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Level bar during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar SetLevelBarValueOverTime (int targetValue, float secondsOfDuration) {
			
			return SetBarValueOverTime (HUDProgressBarTypes.LevelBar, targetValue, secondsOfDuration);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Level bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="increment">Value increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar IncreaseLevelBarValueOverTime (float increment, float secondsOfDuration) {
			
			return IncreaseBarValueOverTime (HUDProgressBarTypes.LevelBar, increment, secondsOfDuration);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Level bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="increment">Value increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar IncreaseLevelBarValueOverTime (int increment, float secondsOfDuration) {
			
			return IncreaseBarValueOverTime (HUDProgressBarTypes.LevelBar, increment, secondsOfDuration);
		}
		
		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Level bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="decrement">Value decrement.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar DecreaseLevelBarValueOverTime (float decrement, float secondsOfDuration) {
			
			return DecreaseBarValueOverTime (HUDProgressBarTypes.LevelBar, decrement, secondsOfDuration);
		}
		
		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDController"/> as Level bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="decrement">Value decrement.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public HUDProgressBar DecreaseLevelBarValueOverTime (int decrement, float secondsOfDuration) {
			
			return DecreaseBarValueOverTime (HUDProgressBarTypes.LevelBar, decrement, secondsOfDuration);
		}


		# endregion

		# region others

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents the current <see cref="CustomHUD.HUDController"/>.
		/// </summary>
		/// <returns>A <see cref="System.String"/> that represents the current <see cref="CustomHUD.HUDController"/>.</returns>
		public override string ToString ()
		{
			return string.Format ("HUDController with {0} anchors and {1} elements", numberOfAnchors, numberOfElements);
		}

		# endregion
	}

}