using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace CustomHUD {


	//[RequireComponent(typeof(Canvas))]
	//[RequireComponent(typeof(CanvasScaler))]
	public class HUDAnchor : MonoBehaviour, IHUDEntity, IHUDAnchor {

		# region constants

		/// <summary>
		/// The type of the HUDentity.
		/// </summary>
		private const HUDEntityTypes _entityType = HUDEntityTypes.Anchor;

		# endregion

		# region class attributes

		/// <summary>
		/// The number of HUDAnchors initialized.
		/// </summary>
		private static int _anchorsInitialized = 0;

		/// <summary>
		/// Gets the amount of HUDAnchors initialized.
		/// </summary>
		/// <value>The anchors initialized.</value>
		public static int anchorsInitialized {

			get { return _anchorsInitialized; }
		}

		# endregion

		# region attributes and properties

		/// <summary>
		/// Indicates if this HUDAnchor has been initialized.
		/// </summary>
		private bool _initialized;

		/// <summary>
		/// Gets a value indicating whether this <see cref="CustomHUD.HUDAnchor"/> is initialized.
		/// </summary>
		/// <value><c>true</c> if is initialized; otherwise, <c>false</c>.</value>
		public bool isInitialized {

			get { return _initialized; }
		}

		/// <summary>
		/// Gets the type of the HUDEntity.
		/// </summary>
		/// <value>The type of the entity.</value>
		public HUDEntityTypes entityType {

			get { return _entityType; } 
		}

		/// <summary>
		/// The type of this HUDAnchor.
		/// </summary>
		public HUDAnchorTypes _type;

		/// <summary>
		/// Gets the type of this HUDAnchor. Indicates where the elements are going to be anchored in the screen.
		/// </summary>
		/// <value>The type.</value>
		public HUDAnchorTypes type {

			get { return _type; }
		}

		/// <summary>
		/// A list of all the HUDElements that are anchored to this HUDAnchor.
		/// </summary>
		private List<HUDElement> _anchoredElements;

		/// <summary>
		/// Gets a list with all the HUDElements anchored to this HUDAnchor.
		/// </summary>
		/// <value>The elements.</value>
		public List<HUDElement> elements {

			get { return _anchoredElements; } 
		}

		/// <summary>
		/// Gets the number of HUDElements anchored to this HUDAnchor.
		/// </summary>
		/// <value>The number of elements.</value>
		public int numberOfElements {

			get { return _anchoredElements.Count; }
		}

		/// <summary>
		/// The HUDController to which this HUDAnchor is subscribed.
		/// </summary>
		private HUDController _controller;

		/// <summary>
		/// Gets the HUDcontroller to which the entity is suscribed. If the entity is the HUDController, gets a reference to itself.
		/// </summary>
		/// <value>The controller.</value>
		public HUDController controller {

			get { return _controller; }
		}

		# endregion

		# region unity default

		void Awake () {
		
			InitializeCollections ();
		}

		void Start () {

			Initialize ();
		} 

		void OnEnable () {
		
			InitEvents ();
		}

		void OnDisable () {


			UnsuscribeFromEvents ();
		}
		# endregion

		# region constructor

		public HUDAnchor (HUDAnchorTypes newType, HUDController newController) {

			_type = newType;
			_controller = newController;

			_initialized = false;
		}
		
		# endregion

		# region methods

		/// <summary>
		/// Gets the type of the HUDEntity.
		/// </summary>
		/// <returns>The entity type.</returns>
		public HUDEntityTypes GetEntityType () {

			return _entityType;
		}

		/// <summary>
		/// Determines whether this entity is initialized.
		/// </summary>
		/// <returns>true</returns>
		/// <c>false</c>
		public bool IsInitialized () {

			return _initialized;
		}

		/// <summary>
		/// Gets the type of this HUDAnchor. Indicates where the elements are going to be anchored in the screen.
		/// </summary>
		/// <returns>The type.</returns>
		public HUDAnchorTypes GetAnchorType () {

			return _type;
		}

		/// <summary>
		/// Gets a list with all the HUDElements anchored to this anchor.
		/// </summary>
		/// <returns>The elements.</returns>
		public List<HUDElement> GetElements () {

			return _anchoredElements;
		}

		/// <summary>
		/// Gets the HUDcontroller to which the entity is suscribed. If the entity is the HUDController, gets a reference to itself.
		/// </summary>
		/// <returns>The controller.</returns>
		public HUDController GetController () {

			return _controller;
		}

		# endregion

		# region initialization

		/// <summary>
		/// Initialization called only in the editor window.
		/// </summary>
		public void EditorInitialize (HUDAnchorTypes initialType, HUDController newController) {
		
			_type = initialType;
			_controller = newController;

			EditorInitializeRectTransform ();
		}

		/// <summary>
		/// Initialization of the RectTranform component of this HUDAnchor, to be called only from the Editor.
		/// </summary>
		private void EditorInitializeRectTransform () {

			RectTransform r = GetComponent<RectTransform> ();

			r.anchoredPosition = Vector2.zero;
			r.sizeDelta = Vector2.zero;

			Vector2[] vectors = GetRectTransformAnchorsAndPivotForCanvas ();

			r.anchorMin = vectors [0];
			r.anchorMax = vectors [1];
			r.pivot = vectors [2];
		}

		/// <summary>
		/// Gets the rect transform anchors and pivot for the canvas depending on the HUDAnchor type.
		/// </summary>
		/// <returns>The rect transform anchors and pivot for canvas.</returns>
		private Vector2[] GetRectTransformAnchorsAndPivotForCanvas () {
		
			// 0 is min anchor
			// 1 is max anchor
			// 2 is pivot
			Vector2[] vectors = new Vector2[3];

			switch (_type) {

			case HUDAnchorTypes.Left:
				vectors[0] = new Vector2 (HUDConst.ANCHOR_MIN_VALUE, HUDConst.ANCHOR_MIN_VALUE);
				vectors[1] = new Vector2 (HUDConst.ANCHOR_MIN_VALUE, HUDConst.ANCHOR_MAX_VALUE);
				vectors[2] = new Vector2 (HUDConst.PIVOT_X_FOR_LEFT_ANCHOR, HUDConst.PIVOT_Y_FOR_LEFT_ANCHOR);
				break;
			case HUDAnchorTypes.Right:
				vectors[0] = new Vector2 (HUDConst.ANCHOR_MAX_VALUE, HUDConst.ANCHOR_MIN_VALUE);
				vectors[1] = new Vector2 (HUDConst.ANCHOR_MAX_VALUE, HUDConst.ANCHOR_MAX_VALUE);
				vectors[2] = new Vector2 (HUDConst.PIVOT_X_FOR_RIGHT_ANCHOR, HUDConst.PIVOT_Y_FOR_RIGHT_ANCHOR);
				break;
			case HUDAnchorTypes.Top:
				vectors[0] = new Vector2 (HUDConst.ANCHOR_MIN_VALUE, HUDConst.ANCHOR_MAX_VALUE);
				vectors[1] = new Vector2 (HUDConst.ANCHOR_MAX_VALUE, HUDConst.ANCHOR_MAX_VALUE);
				vectors[2] = new Vector2 (HUDConst.PIVOT_X_FOR_TOP_ANCHOR, HUDConst.PIVOT_Y_FOR_TOP_ANCHOR);
				break;
			case HUDAnchorTypes.Bottom:
				vectors[0] = new Vector2 (HUDConst.ANCHOR_MIN_VALUE, HUDConst.ANCHOR_MIN_VALUE);
				vectors[1] = new Vector2 (HUDConst.ANCHOR_MAX_VALUE, HUDConst.ANCHOR_MIN_VALUE);
				vectors[2] = new Vector2 (HUDConst.PIVOT_X_FOR_BOTTOM_ANCHOR, HUDConst.PIVOT_Y_FOR_BOTTOM_ANCHOR);
				break;
			case HUDAnchorTypes.Center:
				vectors[0] = new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MID_VALUE);
				vectors[1] = new Vector2 (HUDConst.ANCHOR_MID_VALUE, HUDConst.ANCHOR_MID_VALUE);
				vectors[2] = new Vector2 (HUDConst.PIVOT_X_FOR_CENTER_ANCHOR, HUDConst.PIVOT_Y_FOR_CENTER_ANCHOR);
				break;
			}

			return vectors;
		}

		/// <summary>
		/// Initialize this instance.
		/// </summary>
		private void Initialize () {

			_anchorsInitialized += 1;

			_initialized = true;
		}

		/// <summary>
		/// Initializes the collections.
		/// </summary>
		private void InitializeCollections () {

			_anchoredElements = new List<HUDElement> ();
		}

		/// <summary>
		/// Inits the events.
		/// </summary>
		private void InitEvents () {

			HUDEvents.OnHUDControllerInitializedHandler += OnHUDControllerInitialized;
		}

		/// <summary>
		/// Unsuscribes from events.
		/// </summary>
		private void UnsuscribeFromEvents () {

			HUDEvents.OnHUDControllerInitializedHandler -= OnHUDControllerInitialized;
		}

		# endregion

		# region event response

		/// <summary>
		/// Called when a HUDController has been initialized in the hierarchy.
		/// </summary>
		/// <param name="initializedController">Initialized controller.</param>
		private void OnHUDControllerInitialized (HUDController initializedController) {

			_controller = initializedController;

			SuscribeToController ();
		}


		# endregion

		# region subscription

		/// <summary>
		/// Suscribes to the <see cref="CustomHUD.HUDController"/> existing in the scene as a given anchor type.
		/// </summary>
		private void SuscribeToController () {
		
			_controller.SubscribeAs (type, this);
		}


		# endregion

		# region other

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents the current <see cref="CustomHUD.HUDAnchor"/>.
		/// </summary>
		/// <returns>A <see cref="System.String"/> that represents the current <see cref="CustomHUD.HUDAnchor"/>.</returns>
		public override string ToString () {

			return string.Format ("[HUDAnchor {0} with {1} elems", type, numberOfElements);
		}

		# endregion
	}
}