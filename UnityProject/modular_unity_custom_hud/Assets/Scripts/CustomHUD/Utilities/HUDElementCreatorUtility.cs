﻿using UnityEngine;
using System;
using System.Collections;

namespace CustomHUD {

	/// <summary>
	/// Class that holds all methods to properly instantiate HUDElements.
	/// </summary>
	public static class HUDElementCreatorUtility {

		/// <summary>
		/// Instantiates a given HUDElement given its type and returns it.
		/// </summary>
		/// <returns>The HUD element.</returns>
		/// <param name="elementType">Element type.</param>
		public static GameObject InstantiateHUDElement (HUDElementTypes elementType) {

			return GameObject.Instantiate (GetElementPrefab (elementType));
		}

		/// <summary>
		/// Gets the default prefab associated to a given HUDElementType.
		/// </summary>
		/// <returns>The element prefab.</returns>
		/// <param name="elementType">Element type.</param>
		private static GameObject GetElementPrefab (HUDElementTypes elementType) {
		
			switch (elementType) {
				
			case HUDElementTypes.Subtitle:
				return HUDResources.prefabs.subtitle;
			case HUDElementTypes.WarningPanel:
				return HUDResources.prefabs.warningPanel;
			case HUDElementTypes.HintPanel:
				return HUDResources.prefabs.hintPanel;
			case HUDElementTypes.ProgressBar: 
				return HUDResources.prefabs.progressBar;
			default:
				return HUDResources.prefabs.hintPanel;
			}
		}

		/// <summary>
		/// Gets the System.Type class associated to a given HUDElementType.
		/// </summary>
		/// <returns>The associated HUD element System.Type class.</returns>
		/// <param name="elementType">Element type.</param>
		public static Type GetAssociatedHUDElementType (HUDElementTypes elementType) {

			switch (elementType) {

			case HUDElementTypes.Subtitle:
				return typeof (HUDSubtitle);
			case HUDElementTypes.WarningPanel:
				return typeof(HUDHintPanel);
			case HUDElementTypes.HintPanel:
				return typeof(HUDHintPanel);
			case HUDElementTypes.ProgressBar: 
				return typeof(HUDHintPanel);
			default:
				return typeof(HUDHintPanel);
			}
		}
	}
}
