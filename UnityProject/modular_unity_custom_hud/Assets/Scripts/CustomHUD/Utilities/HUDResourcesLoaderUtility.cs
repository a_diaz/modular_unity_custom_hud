﻿using UnityEngine;
using System.Collections;

namespace CustomHUD {

	/// <summary>
	/// Class that holds all the methods to load default resources for the creation of the HUD.
	/// </summary>
	public static class HUDResourcesLoaderUtility {

		/// <summary>
		/// Loads the default font.
		/// </summary>
		/// <returns>The default font.</returns>
		public static Font LoadDefaultFont () {

			return Resources.Load ("HUDFonts/Roboto/Roboto-Regular") as Font;
		}
		
		/// <summary>
		/// Loads the default hint sprite. Like an A button, for instance.
		/// </summary>
		/// <returns>The default hint sprite.</returns>
		public static Sprite LoadDefaultHintSprite () {
			
			return Resources.Load<Sprite> ("HUDArt/Icons/Buttons_GDquest_Freebies/ButtonA") as Sprite;
		}

		/// <summary>
		/// Loads the canvas scaler info image.
		/// </summary>
		/// <returns>The canvas scaler info image.</returns>
		public static Texture2D LoadCanvasScalerInfoImage () {

			return Resources.Load<Texture2D> ("HUDImages/Editor/canvas_scaler_info") as Texture2D;
		}

		/// <summary>
		/// Loads the test distance indicator info image.
		/// </summary>
		/// <returns>The test distance indicator info image.</returns>
		public static Texture2D LoadTestDistanceIndicatorInfoImage () {
		
			return Resources.Load<Texture2D> ("HUDImages/Editor/test_distance_indicator") as Texture2D;
		}

		/// <summary>
		/// Loads the hint layout info images as an array of four Texture2D.
		/// </summary>
		/// <returns>The hint layout info images.</returns>
		public static Texture2D[] LoadHintLayoutInfoImages () {
		
			return Resources.LoadAll<Texture2D> ("HUDImages/Editor/HintPanel/") as Texture2D[];
		}

		/// <summary>
		/// Loads the progress bar alignment info images as an array three Texture2D.
		/// </summary>
		/// <returns>The progress bar alignment info images.</returns>
		public static Texture2D[] LoadProgressBarAlignmentInfoImages () {
		
			return Resources.LoadAll<Texture2D> ("HUDImages/Editor/ProgressBar/Alignment/") as Texture2D[];
		} 

		/// <summary>
		/// Loads the progress bar layout info images as an array three Texture2D.
		/// </summary>
		/// <returns>The progress bar alignment info images	.</returns>
		public static Texture2D[] LoadProgressBarLayoutInfoImages () {
			
			return Resources.LoadAll<Texture2D> ("HUDImages/Editor/ProgressBar/Layout/") as Texture2D[];
		} 

		/// <summary>
		/// Loads the subtitle prefab.
		/// </summary>
		/// <returns>The warning panel prefab.</returns>
		public static GameObject LoadSubtitlePrefab () {
			
			return Resources.Load ("HUDPrefabs/Subtitle") as GameObject;
		}
		
		/// <summary>
		/// Loads the warning panel prefab.
		/// </summary>
		/// <returns>The warning panel prefab.</returns>
		public static GameObject LoadWarningPanelPrefab () {
			
			return Resources.Load ("HUDPrefabs/WarningPanel") as GameObject;
		}

		/// <summary>
		/// Loads the hint panel prefab.
		/// </summary>
		/// <returns>The hint panel prefab.</returns>
		public static GameObject LoadHintPanelPrefab () {

			return Resources.Load ("HUDPrefabs/HintPanel") as GameObject;
		}

		/// <summary>
		/// Loads the progress bar prefab.
		/// </summary>
		/// <returns>The progress bar prefab.</returns>
		public static GameObject LoadProgressBarPrefab () {

			return Resources.Load ("HUDPrefabs/ProgressBar") as GameObject;
		}
	}

	/// <summary>
	/// Class that provides quick access to all the default resources for the creation of the HUD.
	/// </summary>
	public static class HUDResources {

		/// <summary>
		/// Gets the default font for the HUD.
		/// </summary>
		/// <value>The default font.</value>
		public static Font defaultFont {

			get { return HUDResourcesLoaderUtility.LoadDefaultFont (); }
		}

		/// <summary>
		/// Gets the default hint sprite. Like an A button, for instance.
		/// </summary>
		/// <value>The default hint sprite.</value>
		public static Sprite defaultHintSprite {

			get { return HUDResourcesLoaderUtility.LoadDefaultHintSprite (); } 
		}

		/// <summary>
		/// Gets the canvas scaler info image.
		/// </summary>
		/// <value>The canvas scaler info image.</value>
		public static Texture2D canvasScalerInfoImage {

			get { return HUDResourcesLoaderUtility.LoadCanvasScalerInfoImage (); }
		}

		/// <summary>
		/// Gets the distance info image.
		/// </summary>
		/// <value>The distance info image.</value>
		public static Texture2D distanceInfoImage {

			get { return HUDResourcesLoaderUtility.LoadTestDistanceIndicatorInfoImage (); }
		}

		/// <summary>
		/// Gets the hint panel layout info images as an array of four Texture2D. 
		/// </summary>
		/// <value>The hint panel layout info images.</value>
		public static Texture2D[] hintPanelLayoutInfoImages {

			get { return HUDResourcesLoaderUtility.LoadHintLayoutInfoImages (); } 
		}

		/// <summary>
		/// Gets the progress bar alignment info images as an array of three Texture2D.
		/// </summary>
		/// <value>The progress bar alignment info images.</value>
		public static Texture2D[] progressBarAlignmentInfoImages {

			get { return HUDResourcesLoaderUtility.LoadProgressBarAlignmentInfoImages (); }
		}

		/// <summary>
		/// Gets the progress bar layout info images as an array of two Texture2D.
		/// </summary>
		/// <value>The progress bar layout info images.</value>
		public static Texture2D[] progressBarLayoutInfoImages {

			get { return HUDResourcesLoaderUtility.LoadProgressBarLayoutInfoImages (); }
		}

		/// <summary>
		/// All the pre-defined prefabs.
		/// </summary>
		public static class prefabs {

			/// <summary>
			/// Gets the default subtitle.
			/// </summary>
			/// <value>The warning panel.</value>
			public static GameObject subtitle {
				
				get { return HUDResourcesLoaderUtility.LoadSubtitlePrefab (); } 
			}
			
			/// <summary>
			/// Gets the default warning panel.
			/// </summary>
			/// <value>The warning panel.</value>
			public static GameObject warningPanel {
				
				get { return HUDResourcesLoaderUtility.LoadWarningPanelPrefab (); } 
			}

			/// <summary>
			/// Gets the default hint panel.
			/// </summary>
			/// <value>The hint panel.</value>
			public static GameObject hintPanel {

				get { return HUDResourcesLoaderUtility.LoadHintPanelPrefab (); }
			}

			public static GameObject progressBar {

				get { return HUDResourcesLoaderUtility.LoadProgressBarPrefab (); } 
			}
		}
	}
}
