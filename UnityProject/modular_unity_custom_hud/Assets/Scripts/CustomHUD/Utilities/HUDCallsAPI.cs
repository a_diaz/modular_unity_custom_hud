﻿using UnityEngine;
using System.Collections;

namespace CustomHUD {

	/// <summary>
	/// Provides a series of calls for abstracting common HUD behaviour.
	/// </summary>
	public static class HUD {

		public static HUDController Controller {

			get { return HUDController.Instance; }
		}

		# region api calls
		
		// general purpose api calls
		
		/// <summary>
		/// Shows a <see cref="CustomHUD.HUDElement"/> registered, given its key.
		/// </summary>
		/// <param name="elementKey">Element key.</param>
		public static HUDElement ShowElement (string elementKey) {
			
			return Controller.ShowElement (elementKey);
		}
		
		/// <summary>
		/// Shows a given <see cref="CustomHUD.HUDElement"/>.
		/// </summary>
		/// <returns>The element.</returns>
		/// <param name="element">Element.</param>
		public static HUDElement ShowElement (HUDElement element) {
			
			return Controller.ShowElement (element);
		}
		
		/// <summary>
		/// Hides a <see cref="CustomHUD.HUDElement"/> registered, given its key.
		/// </summary>
		/// <param name="elementKey">Element key.</param>
		public static HUDElement HideElement (string elementKey) {

			return Controller.HideElement (elementKey);
		}
		
		/// <summary>
		/// Hides a given <see cref="CustomHUD.HUDElement"/>.
		/// </summary>
		/// <returns>The element.</returns>
		/// <param name="element">Element.</param>
		public static HUDElement HideElement (HUDElement element) {
			
			return Controller.HideElement (element);	
		}
		
		/// <summary>
		/// Shows a <see cref="CustomHUD.HUDElement"/> registered, given its key, and hides it after some seconds have passed.
		/// </summary>
		/// <param name="elementKey">Element key.</param>
		/// <param name="secondsToHide">Time in seconds to hide the element after it has been shown.</param>
		public static HUDElement ShowAndHideElement (string elementKey, float secondsToHide) {
			
			return Controller.ShowAndHideElement (elementKey, secondsToHide);	
		}
		
		/// <summary>
		/// Shows a given <see cref="CustomHUD.HUDElement"/> and hides it after some seconds have passed.
		/// </summary>
		/// <returns>The element.</returns>
		/// <param name="element">Element.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDElement ShowAndHideElement (HUDElement element, float secondsToHide) {
			
			return Controller.ShowAndHideElement (element, secondsToHide);	
		}

		//
		// HUDSubtitle calls
		//

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed as a given type.
		/// </summary>
		/// <returns>The subtitle.</returns>
		/// <param name="subtitleType">Subtitle type.</param>
		public static HUDSubtitle ShowSubtitle (HUDSubtitleTypes subtitleType) {
			
			return Controller.ShowSubtitle (subtitleType);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed as a given type with a new text.
		/// </summary>
		/// <returns>The subtitle.</returns>
		/// <param name="subtitleType">Subtitle type.</param>
		/// <param name="newSubtitleText">New subtitle text.</param>
		public static HUDSubtitle ShowSubtitle (HUDSubtitleTypes subtitleType, string newSubtitleText) {
			
			return Controller.ShowSubtitle (subtitleType, newSubtitleText);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed as a given type and hides it after some time has passed.
		/// </summary>
		/// <returns>The and hide hint.</returns>
		/// <param name="subtitleType">Subtitle type.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDSubtitle ShowAndHideSubtitle (HUDSubtitleTypes subtitleType, float secondsToHide) {
			
			return Controller. ShowAndHideSubtitle (subtitleType, secondsToHide);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed as a given type with a new text and hides it after some time has passed.
		/// </summary>
		public static HUDSubtitle ShowAndHideSubtitle (HUDSubtitleTypes subtitleType, string newSubtitleText, float secondsToHide) {
			
			return Controller.ShowAndHideSubtitle (subtitleType, newSubtitleText, secondsToHide);
		}
		
		
		/// <summary>
		/// Hides the <see cref="CustomHUD.HUDSubtitle"/> subscribed as a given type.
		/// </summary>
		/// <returns>The subtitle.</returns>
		/// <param name="subtitleType">Subtitle type.</param>
		public static HUDSubtitle HideSubtitle (HUDSubtitleTypes subtitleType) {
			
			return Controller.HideSubtitle (subtitleType);
		}
		
		/// <summary>
		/// Sets the text of the <see cref="CustomHUD.HUDSubtitle"/> subscribed as a given type regardless of it is currently shown or not.
		/// </summary>
		/// <returns>The subtitle.</returns>
		/// <param name="subtitleType">Subtitle type.</param>
		/// <param name="newSubtitleText">New subtitle text.</param>
		public static HUDSubtitle SetSubtitleText (HUDSubtitleTypes subtitleType, string newSubtitleText) {
			
			return Controller.SetSubtitleText (subtitleType, newSubtitleText);
		}


		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed as Primary subtitle.
		/// </summary>
		/// <returns>The Primary subtitle.</returns>
		public static HUDSubtitle ShowPrimarySubtitle () {
			
			return Controller.ShowPrimarySubtitle ();
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed as Primary subtitle with a new given text.
		/// </summary>
		/// <returns>The Primary subtitle.</returns>
		/// <param name="newSubtitleText">New subtitle text.</param>
		public static HUDSubtitle ShowPrimarySubtitle (string newSubtitleText) {
			
			return Controller.ShowPrimarySubtitle (newSubtitleText);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed as Primary subtitle and hides it after a given seconds have passed.
		/// </summary>
		/// <returns>The Primary subtitle.</returns>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDSubtitle ShowAndHidePrimarySubtitle (float secondsToHide) {
			
			return Controller.ShowAndHidePrimarySubtitle (secondsToHide);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed as Primary subtitle with a new text and hides it after a given seconds have passed.
		/// </summary>
		/// <returns>The Primary subtitle.</returns>
		/// <param name="newSubtitleText">New subtitle text.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDSubtitle ShowAndHidePrimarySubtitle (string newSubtitleText, float secondsToHide) {
			
			return Controller.ShowAndHidePrimarySubtitle (newSubtitleText, secondsToHide);
		}
		
		/// <summary>
		/// Hides the <see cref="CustomHUD.HUDSubtitle"/> subscribed as Primary subtitle.
		/// </summary>
		/// <returns>The Primary subtitle.</returns>
		public static HUDSubtitle HidePrimarySubtitle () {
			
			return Controller.HidePrimarySubtitle ();
		}
		
		/// <summary>
		/// Sets the text of the <see cref="CustomHUD.HUDSubtitle"/> subscribed as Primary subtitle, regardless of it is currently shown or not.
		/// </summary>
		/// <returns>The Primary subtitle text.</returns>
		/// <param name="newSubtitleText">New subtitle text.</param>
		public static HUDSubtitle SetPrimarySubtitleText (string newSubtitleText) {
			
			return Controller.SetPrimarySubtitleText (newSubtitleText);
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed as Secondary subtitle.
		/// </summary>
		/// <returns>The Secondary subtitle.</returns>
		public static HUDSubtitle ShowSecondarySubtitle () {
			
			return Controller.ShowSecondarySubtitle ();
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed as Secondary subtitle with a new given text.
		/// </summary>
		/// <returns>The Secondary subtitle.</returns>
		/// <param name="newSubtitleText">New subtitle text.</param>
		public static HUDSubtitle ShowSecondarySubtitle (string newSubtitleText) {
			
			return Controller.ShowSecondarySubtitle (newSubtitleText);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed as Secondary subtitle and hides it after a given seconds have passed.
		/// </summary>
		/// <returns>The Secondary subtitle.</returns>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDSubtitle ShowAndHideSecondarySubtitle (float secondsToHide) {
			
			return Controller.ShowAndHideSecondarySubtitle (secondsToHide);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDSubtitle"/> subscribed as Secondary subtitle with a new text and hides it after a given seconds have passed.
		/// </summary>
		/// <returns>The Secondary subtitle.</returns>
		/// <param name="newSubtitleText">New subtitle text.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDSubtitle ShowAndHideSecondarySubtitle (string newSubtitleText, float secondsToHide) {
			
			return Controller.ShowAndHideSecondarySubtitle (newSubtitleText, secondsToHide);
		}
		
		/// <summary>
		/// Hides the <see cref="CustomHUD.HUDSubtitle"/> subscribed as Secondary subtitle.
		/// </summary>
		/// <returns>The Secondary subtitle.</returns>
		public static HUDSubtitle HideSecondarySubtitle () {
			
			return Controller.HideSecondarySubtitle ();
		}
		
		/// <summary>
		/// Sets the text of the <see cref="CustomHUD.HUDSubtitle"/> subscribed as Secondary subtitle, regardless of it is currently shown or not.
		/// </summary>
		/// <returns>The Secondary subtitle text.</returns>
		/// <param name="newSubtitleText">New subtitle text.</param>
		public static HUDSubtitle SetSecondarySubtitleText (string newSubtitleText) {
			
			return Controller.SetSecondarySubtitleText (newSubtitleText);
		}

		//
		// HUDWarningPanel calls
		//
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDWarningPanel"/> subscribed.
		/// </summary>
		/// <returns>The warning.</returns>
		public static HUDWarningPanel ShowWarning () {

			return Controller.ShowWarning ();
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDWarningPanel"/> subscribed with a new given text.
		/// </summary>
		/// <returns>The warning.</returns>
		/// <param name="newWarningText">New warning text.</param>
		public static HUDWarningPanel ShowWarning (string newWarningText) {
			
			return Controller.ShowWarning (newWarningText);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDWarningPanel"/> subscribed with a new given sprite.
		/// </summary>
		/// <returns>The warning.</returns>
		/// <param name="newWarningSprite">New warning sprite.</param>
		public static HUDWarningPanel ShowWarning (Sprite newWarningSprite) {
			
			return Controller.ShowWarning (newWarningSprite);
		}
		
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDWarningPanel"/> subscribed with a new given text and a new given sprite.
		/// </summary>
		/// <returns>The warning.</returns>
		/// <param name="newWarningText">New warning text.</param>
		/// <param name="newWarningSprite">New warning sprite.</param>
		public static HUDWarningPanel ShowWarning (string newWarningText, Sprite newWarningSprite) {
			
			return Controller.ShowWarning (newWarningText, newWarningSprite);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDWarningPanel"/> subscribed and hides it after a specified time has passed.
		/// </summary>
		/// <returns>The warning.</returns>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDWarningPanel ShowAndHideWarning (float secondsToHide) {
		
			return Controller.ShowAndHideWarning (secondsToHide);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDWarningPanel"/> subscribed with a new given text and hides it after a specified time has passed.
		/// </summary>
		/// <returns>The warning.</returns>
		/// <param name="newWarningText">New warning text.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDWarningPanel ShowAndHideWarning (string newWarningText, float secondsToHide) {
			
			return Controller.ShowAndHideWarning (newWarningText, secondsToHide);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDWarningPanel"/> subscribed with a new given sprite and hides it after a specified time has passed.
		/// </summary>
		/// <returns>The and hide warning.</returns>
		/// <param name="newWarningSprite">New warning sprite.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDWarningPanel ShowAndHideWarning (Sprite newWarningSprite, float secondsToHide) {
			
			return Controller.ShowAndHideWarning (newWarningSprite, secondsToHide);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDWarningPanel"/> subscribed with a new given text and a new given sprite and hides it after a specified time has passed.
		/// </summary>
		/// <returns>The warning.</returns>
		/// <param name="newWarningText">New warning text.</param>
		/// <param name="newWarningSprite">New warning sprite.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDWarningPanel ShowAndHideWarning (string newWarningText, Sprite newWarningSprite, float secondsToHide) {

			return Controller.ShowAndHideWarning (newWarningText, newWarningSprite, secondsToHide);
		}
		
		/// <summary>
		/// Hides the <see cref="CustomHUD.HUDWarningPanel"/> subscribed.
		/// </summary>
		/// <returns>The warning.</returns>
		public static HUDWarningPanel HideWarning () {

			return Controller.HideWarning ();
		}
		
		//
		// HUDHintPanel calls
		//
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed as a given type.
		/// </summary>
		/// <returns>The hint.</returns>
		/// <param name="hintType">Hint type.</param>
		public static HUDHintPanel ShowHint (HUDHintTypes hintType) {

			return Controller.ShowHint (hintType);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed as a given type with a new text.
		/// </summary>
		/// <returns>The hint.</returns>
		/// <param name="hintType">Hint type.</param>
		/// <param name="newHintText">New hint text.</param>
		public static HUDHintPanel ShowHint (HUDHintTypes hintType, string newHintText) {
			
			return Controller.ShowHint (hintType, newHintText);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed as a given type with a new sprite.
		/// </summary>
		/// <returns>The hint.</returns>
		/// <param name="hintType">Hint type.</param>
		/// <param name="newHintSprite">New hint sprite.</param>
		public static HUDHintPanel ShowHint (HUDHintTypes hintType, Sprite newHintSprite) {

			return Controller.ShowHint (hintType, newHintSprite);
		} 
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed as a given type with a new text and a new sprite.
		/// </summary>
		/// <returns>The hint.</returns>
		/// <param name="hintType">Hint type.</param>
		/// <param name="newHintText">New hint text.</param>
		/// <param name="newHintSprite">New hint sprite.</param>
		public static HUDHintPanel ShowHint (HUDHintTypes hintType, string newHintText, Sprite newHintSprite) {

			return Controller.ShowHint (hintType, newHintText, newHintSprite);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed as a given type and hides it after some time has passed.
		/// </summary>
		/// <returns>The hint.</returns>
		/// <param name="hintType">Hint type.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDHintPanel ShowAndHideHint (HUDHintTypes hintType, float secondsToHide) {
		
			return Controller.ShowAndHideHint (hintType, secondsToHide);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed as a given type with a new text and hides it after some time has passed.
		/// </summary>
		/// <returns>The hint.</returns>
		/// <param name="hintType">Hint type.</param>
		/// <param name="newHintText">New hint text.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDHintPanel ShowAndHideHint (HUDHintTypes hintType, string newHintText, float secondsToHide) {
			
			return Controller.ShowAndHideHint (hintType, newHintText, secondsToHide);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed as a given type with a new sprite and hides it after some time has passed.
		/// </summary>
		/// <returns>The hint.</returns>
		/// <param name="hintType">Hint type.</param>
		/// <param name="newHintSprite">New hint sprite.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDHintPanel ShowAndHideHint (HUDHintTypes hintType, Sprite newHintSprite, float secondsToHide) {
			
			return Controller.ShowAndHideHint (hintType, newHintSprite, secondsToHide);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed as a given type with a new text and a new sprite and hides it after some time has passed.
		/// </summary>
		/// <returns>The hint.</returns>
		/// <param name="hintType">Hint type.</param>
		/// <param name="newHintText">New hint text.</param>
		/// <param name="newHintSprite">New hint sprite.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDHintPanel ShowAndHideHint (HUDHintTypes hintType, string newHintText, Sprite newHintSprite, float secondsToHide) {
			
			return Controller.ShowAndHideHint (hintType, newHintText, newHintSprite, secondsToHide);
		}
		
		/// <summary>
		/// Hides the <see cref="CustomHUD.HUDHintPanel"/> subscribed as a given type.
		/// </summary>
		/// <returns>The hint.</returns>
		/// <param name="hintType">Hint type.</param>
		public static HUDHintPanel HideHint (HUDHintTypes hintType) {
		
			return Controller.HideHint (hintType);
		}
		
		// ACTION HUDHintPanel calls
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> suscribed as Action hint.
		/// </summary>
		/// <returns>The Action hint.</returns>
		public static HUDHintPanel ShowActionHint () {

			return Controller.ShowActionHint ();
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> suscribed as Action hint with a new text.
		/// </summary>
		/// <returns>The Action hint.</returns>
		/// <param name="newHintText">New hint text.</param>
		public static HUDHintPanel ShowActionHint (string newHintText) {
			
			return Controller.ShowActionHint (newHintText);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> suscribed as Action hint with a new sprite.
		/// </summary>
		/// <returns>The Action hint.</returns>
		/// <param name="newHintSprite">New hint sprite.</param>
		public static HUDHintPanel ShowActionHint (Sprite newHintSprite) {
			
			return Controller.ShowActionHint (newHintSprite);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> suscribed as Action hint with a new text and a new sprite.
		/// </summary>
		/// <returns>The Action hint.</returns>
		/// <param name="newHintText">New hint text.</param>
		/// <param name="newHintSprite">New hint sprite.</param>
		public static HUDHintPanel ShowActionHint (string newHintText, Sprite newHintSprite) {
			
			return Controller.ShowActionHint (newHintText, newHintSprite);
		}

		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed as Action hint and hides it after some seconds have passed.
		/// </summary>
		/// <returns>The Action hint.</returns>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDHintPanel ShowAndHideActionHint (float secondsToHide) {
		
			return Controller.ShowAndHideActionHint (secondsToHide);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed as Action hint with a new text and hides it after some seconds have passed.
		/// </summary>
		/// <returns>The Action hint.</returns>
		/// <param name="newHintText">New hint text.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDHintPanel ShowAndHideActionHint (string newHintText, float secondsToHide) {
			
			return Controller.ShowAndHideActionHint (newHintText, secondsToHide);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed as Action hint with a new sprite and hides it after some seconds have passed.
		/// </summary>
		/// <returns>The Action hint.</returns>
		/// <param name="newHintSprite">New hint sprite.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDHintPanel ShowAndHideActionHint (Sprite newHintSprite, float secondsToHide) {
		
			return Controller.ShowAndHideActionHint (newHintSprite, secondsToHide);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed as Action hint with a new text and a new sprite and hides it after some seconds have passed.
		/// </summary>
		/// <returns>The Action hint.</returns>
		/// <param name="newHintText">New hint text.</param>
		/// <param name="newHintSprite">New hint sprite.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDHintPanel ShowAndHideActionHint (string newHintText, Sprite newHintSprite, float secondsToHide) {
			
			return Controller.ShowAndHideActionHint (newHintText, newHintSprite, secondsToHide);
		}
		
		/// <summary>
		/// Hides the <see cref="CustomHUD.HUDHintPanel"/> subscribed as Action hint.
		/// </summary>
		/// <returns>The Action hint.</returns>
		public static HUDHintPanel HideActionHint () {

			return Controller.HideActionHint ();
		}
		
		//
		// REMINDER HUDHintPanel calls
		//
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> suscribed as Reminder hint.
		/// </summary>
		/// <returns>The Reminder hint.</returns>
		public static HUDHintPanel ShowReminderHint () {
			
			return Controller.ShowReminderHint ();
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> suscribed as Reminder hint with a new text.
		/// </summary>
		/// <returns>The Reminder hint.</returns>
		/// <param name="newHintText">New hint text.</param>
		public static HUDHintPanel ShowReminderHint (string newHintText) {
			
			return Controller.ShowReminderHint (newHintText);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> suscribed as Reminder hint with a new sprite.
		/// </summary>
		/// <returns>The Reminder hint.</returns>
		/// <param name="newHintSprite">New hint sprite.</param>
		public static HUDHintPanel ShowReminderHint (Sprite newHintSprite) {
			
			return Controller.ShowReminderHint (newHintSprite);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> suscribed as Reminder hint with a new text and a new sprite.
		/// </summary>
		/// <returns>The Reminder hint.</returns>
		/// <param name="newHintText">New hint text.</param>
		/// <param name="newHintSprite">New hint sprite.</param>
		public static HUDHintPanel ShowReminderHint (string newHintText, Sprite newHintSprite) {
			
			return Controller.ShowReminderHint (newHintText, newHintSprite);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed as Reminder hint and hides it after some seconds have passed.
		/// </summary>
		/// <returns>The Reminder hint.</returns>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDHintPanel ShowAndHideReminderHint (float secondsToHide) {
			
			return Controller.ShowAndHideReminderHint (secondsToHide);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed as Reminder hint with a new text and hides it after some seconds have passed.
		/// </summary>
		/// <returns>The Reminder hint.</returns>
		/// <param name="newHintText">New hint text.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDHintPanel ShowAndHideReminderHint (string newHintText, float secondsToHide) {
			
			return Controller.ShowAndHideReminderHint (newHintText, secondsToHide);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed as Reminder hint with a new sprite and hides it after some seconds have passed.
		/// </summary>
		/// <returns>The Reminder hint.</returns>
		/// <param name="newHintSprite">New hint sprite.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDHintPanel ShowAndHideReminderHint (Sprite newHintSprite, float secondsToHide) {
			
			return Controller.ShowAndHideReminderHint (newHintSprite, secondsToHide);
		}
		
		/// <summary>
		/// Shows the <see cref="CustomHUD.HUDHintPanel"/> subscribed as Reminder hint with a new text and a new sprite and hides it after some seconds have passed.
		/// </summary>
		/// <returns>The Reminder hint.</returns>
		/// <param name="newHintText">New hint text.</param>
		/// <param name="newHintSprite">New hint sprite.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDHintPanel ShowAndHideReminderHint (string newHintText, Sprite newHintSprite, float secondsToHide) {
			
			return Controller.ShowAndHideReminderHint (newHintText, newHintSprite, secondsToHide);
		}
		
		/// <summary>
		/// Hides the <see cref="CustomHUD.HUDHintPanel"/> subscribed as Reminder hint.
		/// </summary>
		/// <returns>The Reminder hint.</returns>
		public static HUDHintPanel HideReminderHint () {
			
			return Controller.HideReminderHint ();
		}

		
		//
		// HUDProgressBar calls
		//
		
		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed as the given type to a given value.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetProgressBarValue (HUDProgressBarTypes barType, float newValue) {

			return Controller.SetProgressBarValue (barType, newValue);
		}
		
		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed as the given type to a given value.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetProgressBarValue (HUDProgressBarTypes barType, int newValue) {
			
			return Controller.SetProgressBarValue (barType, newValue);
		}
		
		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed as the given type to a given value.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetProgressBarMaximumValue (HUDProgressBarTypes barType, float newValue) {
			
			return Controller.SetProgressBarMaximumValue (barType, newValue);
		}
		
		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed as the given type to a given value.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetProgressBarMaximumValue (HUDProgressBarTypes barType, int newValue) {
			
			return Controller.SetProgressBarMaximumValue (barType, newValue);
		}
		
		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed as the given type to a given value.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetProgressBarMinimumValue (HUDProgressBarTypes barType, float newValue) {
			
			return Controller.SetProgressBarMinimumValue (barType, newValue);
		}
		
		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed as the given type to a given value.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetProgressBarMinimumValue (HUDProgressBarTypes barType, int newValue) {
			
			return Controller.SetProgressBarMinimumValue (barType, newValue);
		}
		
		/// <summary>
		/// Shows the HUDProgressBar subscribed as the given type.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		public static HUDProgressBar ShowProgressBar (HUDProgressBarTypes barType) {

			return Controller.ShowProgressBar (barType);
		}
		
		/// <summary>
		/// Hides the HUDProgressBar subscribed to this <see cref="CustomHUD.HUDthis <see cref="CustomHUD.HUDController"/>"/> as the given type.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		public static HUDProgressBar HideProgressBar (HUDProgressBarTypes barType) {

			return Controller.HideProgressBar (barType);
		}
		
		/// <summary>
		/// Shows the HUDProgressBar subscribed as the given type, and hides it after a given time has passed.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDProgressBar ShowAndHideProgressBar (HUDProgressBarTypes barType, float secondsToHide) {

			return Controller.ShowAndHideProgressBar (barType, secondsToHide);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as the given type, by the given increment.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="increment">Increment.</param>
		public static HUDProgressBar IncreaseBarValue (HUDProgressBarTypes barType, float increment) {

			return Controller.IncreaseBarValue (barType, increment);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as the given type, by the given increment.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="increment">Increment.</param>
		public static HUDProgressBar IncreaseBarValue (HUDProgressBarTypes barType, int increment) {

			return Controller.IncreaseBarValue (barType, increment);
		}
		
		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed as the given type, by the given decrement.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="decrement">Decrement.</param>
		public static HUDProgressBar DecreaseBarValue (HUDProgressBarTypes barType, float decrement) {
			
			return Controller.DecreaseBarValue (barType, decrement);
		}
		
		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed as the given type, by the given decrement.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="decrement">Decrement.</param>
		public static HUDProgressBar DecreaseBarValue (HUDProgressBarTypes barType, int decrement) {
			
			return Controller.DecreaseBarValue (barType, decrement);
		}
		
		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed as the given type during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The progress bar..</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar SetBarValueOverTime (HUDProgressBarTypes barType, float targetValue, float secondsOfDuration) {

			return Controller.SetBarValueOverTime (barType, targetValue, secondsOfDuration);
		}
		
		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed as the given type during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar SetBarValueOverTime (HUDProgressBarTypes barType, int targetValue, float secondsOfDuration) {
			
			return Controller.SetBarValueOverTime (barType, targetValue, secondsOfDuration);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as the given type by the given ammount, during the specified time.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="increment">Increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar IncreaseBarValueOverTime (HUDProgressBarTypes barType, float increment, float secondsOfDuration) {
			
			return Controller.IncreaseBarValueOverTime (barType, increment, secondsOfDuration);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as the given type by the given ammount, during the specified time.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="increment">Increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar IncreaseBarValueOverTime (HUDProgressBarTypes barType, int increment, float secondsOfDuration) {
			
			return Controller.IncreaseBarValueOverTime (barType, increment, secondsOfDuration);
		}
		
		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed as the given type by the given ammount, during the specified time.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="decrement">decrease.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar DecreaseBarValueOverTime (HUDProgressBarTypes barType, float decrement, float secondsOfDuration) {
			
			return Controller.DecreaseBarValueOverTime (barType, decrement, secondsOfDuration);
		}
		
		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed as the given type by the given ammount, during the specified time.
		/// </summary>
		/// <returns>The progress bar.</returns>
		/// <param name="barType">Bar type.</param>
		/// <param name="decrement">decrease.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar DecreaseBarValueOverTime (HUDProgressBarTypes barType, int decrement, float secondsOfDuration) {

			return Controller.DecreaseBarValueOverTime (barType, decrement, secondsOfDuration);
		}
		
		
		//
		// HUD Health Progress Bar
		//
		
		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed as Health bar.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetHealthBarValue (float newValue) {

			return Controller.SetHealthBarValue (newValue);
		}
		
		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed as Health bar.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetHealthBarValue (int newValue) {
			
			return Controller.SetHealthBarValue (newValue);
		}
		
		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed as Health bar.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetHealthBarMaximumValue (float newValue) {
			
			return Controller.SetHealthBarMaximumValue (newValue);
		}
		
		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed as Health bar.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetHealthBarMaximumValue (int newValue) {
			
			return Controller.SetHealthBarMaximumValue (newValue);
		}
		
		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed as Health bar.
		/// </summary>
		/// <returns>The health bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetHealthBarMinimumValue (float newValue) {
			
			return Controller.SetHealthBarMinimumValue (newValue);
		}
		
		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed as Health bar.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetHealthBarMinimumValue (int newValue) {
			
			return Controller.SetHealthBarMinimumValue (newValue);
		}
		
		/// <summary>
		/// Shows the Health bar.
		/// </summary>
		/// <returns>The Health bar.</returns>
		public static HUDProgressBar ShowHealthBar () {
		
			return Controller.ShowHealthBar ();
		}
		
		/// <summary>
		/// Hides the Health bar.
		/// </summary>
		/// <returns>The Health bar.</returns>
		public static HUDProgressBar HideHealthBar () {
			
			return Controller.HideHealthBar ();
		}
		
		/// <summary>
		/// Shows and hides the Health bar after some seconds have passed.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDProgressBar ShowAndHideHealthBar (float secondsToHide) {

			return Controller.ShowAndHideHealthBar (secondsToHide);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as Health bar by a given amount.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="increment">The value increment.</param>
		public static HUDProgressBar IncreaseHealthBarValue (float increment) {

			return Controller.IncreaseHealthBarValue (increment);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as Health bar by a given amount.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="increment">The value increment.</param>
		public static HUDProgressBar IncreaseHealthBarValue (int increment) {
			
			return Controller.IncreaseHealthBarValue (increment);
		}
		
		/// <summary>
		/// Decreass the current value of the HUDProgressBar subscribed as Health bar by a given amount.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="decrement">The value decrement.</param>
		public static HUDProgressBar DecreaseHealthBarValue (float decrement) {
			
			return Controller.DecreaseHealthBarValue (decrement);
		}
		
		/// <summary>
		/// Decreass the current value of the HUDProgressBar subscribed as Health bar by a given amount.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="decrement">The value decrement.</param>
		public static HUDProgressBar DecreaseHealthBarValue (int decrement) {
			
			return Controller.DecreaseHealthBarValue (decrement);
		}
		
		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed as Health bar during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar SetHealthBarValueOverTime (float targetValue, float secondsOfDuration) {

			return Controller.SetHealthBarValueOverTime (targetValue, secondsOfDuration);
		}
		
		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed as Health bar during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar SetHealthBarValueOverTime (int targetValue, float secondsOfDuration) {
		
			return Controller.SetHealthBarValueOverTime (targetValue, secondsOfDuration);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as Health bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="increment">Value increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar IncreaseHealthBarValueOverTime (float increment, float secondsOfDuration) {

			return Controller.IncreaseHealthBarValueOverTime (increment, secondsOfDuration);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as Health bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="increment">Value increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar IncreaseHealthBarValueOverTime (int increment, float secondsOfDuration) {
			
			return Controller.IncreaseHealthBarValueOverTime (increment, secondsOfDuration);
		}
		
		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed as Health bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="decrement">Value decrement.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar DecreaseHealthBarValueOverTime (float decrement, float secondsOfDuration) {
			
			return Controller.DecreaseHealthBarValueOverTime (decrement, secondsOfDuration);
		}
		
		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed as Health bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Health bar.</returns>
		/// <param name="decrement">Value decrement.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar DecreaseHealthBarValueOverTime (int decrement, float secondsOfDuration) {
			
			return Controller.DecreaseHealthBarValueOverTime (decrement, secondsOfDuration);
		}
		
		//
		// Energy bar
		//

		
		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed as Energy bar.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetEnergyBarValue (float newValue) {
			
			return Controller.SetEnergyBarValue (newValue);
		}
		
		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed as Energy bar.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetEnergyBarValue (int newValue) {
			
			return Controller.SetEnergyBarValue (newValue);
		}
		
		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed as Energy bar.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetEnergyBarMaximumValue (float newValue) {
			
			return Controller.SetEnergyBarMaximumValue (newValue);
		}
		
		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed as Energy bar.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetEnergyBarMaximumValue (int newValue) {
			
			return Controller.SetEnergyBarMaximumValue (newValue);
		}
		
		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed as Energy bar.
		/// </summary>
		/// <returns>The health bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetEnergyBarMinimumValue (float newValue) {
			
			return Controller.SetEnergyBarMinimumValue (newValue);
		}
		
		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed as Energy bar.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetEnergyBarMinimumValue (int newValue) {
			
			return Controller.SetEnergyBarMinimumValue (newValue);
		}
		
		/// <summary>
		/// Shows the Energy bar.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		public static HUDProgressBar ShowEnergyBar () {
			
			return Controller.ShowEnergyBar ();
		}
		
		/// <summary>
		/// Hides the Energy bar.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		public static HUDProgressBar HideEnergyBar () {
			
			return Controller.HideEnergyBar ();
		}
		
		/// <summary>
		/// Shows and hides the Energy bar after some seconds have passed.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDProgressBar ShowAndHideEnergyBar (float secondsToHide) {
			
			return Controller.ShowAndHideEnergyBar (secondsToHide);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as Energy bar by a given amount.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="increment">The value increment.</param>
		public static HUDProgressBar IncreaseEnergyBarValue (float increment) {
			
			return Controller.IncreaseEnergyBarValue (increment);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as Energy bar by a given amount.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="increment">The value increment.</param>
		public static HUDProgressBar IncreaseEnergyBarValue (int increment) {
			
			return Controller.IncreaseEnergyBarValue (increment);
		}
		
		/// <summary>
		/// Decreass the current value of the HUDProgressBar subscribed as Energy bar by a given amount.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="decrement">The value decrement.</param>
		public static HUDProgressBar DecreaseEnergyBarValue (float decrement) {
			
			return Controller.DecreaseEnergyBarValue (decrement);
		}
		
		/// <summary>
		/// Decreass the current value of the HUDProgressBar subscribed as Energy bar by a given amount.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="decrement">The value decrement.</param>
		public static HUDProgressBar DecreaseEnergyBarValue (int decrement) {
			
			return Controller.DecreaseEnergyBarValue (decrement);
		}
		
		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed as Energy bar during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar SetEnergyBarValueOverTime (float targetValue, float secondsOfDuration) {
			
			return Controller.SetEnergyBarValueOverTime (targetValue, secondsOfDuration);
		}
		
		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed as Energy bar during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar SetEnergyBarValueOverTime (int targetValue, float secondsOfDuration) {
			
			return Controller.SetEnergyBarValueOverTime (targetValue, secondsOfDuration);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as Energy bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="increment">Value increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar IncreaseEnergyBarValueOverTime (float increment, float secondsOfDuration) {
			
			return Controller.IncreaseEnergyBarValueOverTime (increment, secondsOfDuration);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as Energy bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="increment">Value increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar IncreaseEnergyBarValueOverTime (int increment, float secondsOfDuration) {
			
			return Controller.IncreaseEnergyBarValueOverTime (increment, secondsOfDuration);
		}
		
		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed as Energy bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="decrement">Value decrement.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar DecreaseEnergyBarValueOverTime (float decrement, float secondsOfDuration) {
			
			return Controller.DecreaseEnergyBarValueOverTime (decrement, secondsOfDuration);
		}
		
		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed as Energy bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Energy bar.</returns>
		/// <param name="decrement">Value decrement.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar DecreaseEnergyBarValueOverTime (int decrement, float secondsOfDuration) {
			
			return Controller.DecreaseEnergyBarValueOverTime (decrement, secondsOfDuration);
		}


		
		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed as Experience bar.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetExperienceBarValue (float newValue) {
			
			return Controller.SetExperienceBarValue (newValue);
		}
		
		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed as Experience bar.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetExperienceBarValue (int newValue) {
			
			return Controller.SetExperienceBarValue (newValue);
		}
		
		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed as Experience bar.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetExperienceBarMaximumValue (float newValue) {
			
			return Controller.SetExperienceBarMaximumValue (newValue);
		}
		
		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed as Experience bar.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetExperienceBarMaximumValue (int newValue) {
			
			return Controller.SetExperienceBarMaximumValue (newValue);
		}
		
		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed as Experience bar.
		/// </summary>
		/// <returns>The health bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetExperienceBarMinimumValue (float newValue) {
			
			return Controller.SetExperienceBarMinimumValue (newValue);
		}
		
		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed as Experience bar.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetExperienceBarMinimumValue (int newValue) {
			
			return Controller.SetExperienceBarMinimumValue (newValue);
		}
		
		/// <summary>
		/// Shows the Experience bar.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		public static HUDProgressBar ShowExperienceBar () {
			
			return Controller.ShowExperienceBar ();
		}
		
		/// <summary>
		/// Hides the Experience bar.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		public static HUDProgressBar HideExperienceBar () {
			
			return Controller.HideExperienceBar ();
		}
		
		/// <summary>
		/// Shows and hides the Experience bar after some seconds have passed.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDProgressBar ShowAndHideExperienceBar (float secondsToHide) {
			
			return Controller.ShowAndHideExperienceBar (secondsToHide);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as Experience bar by a given amount.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="increment">The value increment.</param>
		public static HUDProgressBar IncreaseExperienceBarValue (float increment) {
			
			return Controller.IncreaseExperienceBarValue (increment);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as Experience bar by a given amount.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="increment">The value increment.</param>
		public static HUDProgressBar IncreaseExperienceBarValue (int increment) {
			
			return Controller.IncreaseExperienceBarValue (increment);
		}
		
		/// <summary>
		/// Decreass the current value of the HUDProgressBar subscribed as Experience bar by a given amount.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="decrement">The value decrement.</param>
		public static HUDProgressBar DecreaseExperienceBarValue (float decrement) {
			
			return Controller.DecreaseExperienceBarValue (decrement);
		}
		
		/// <summary>
		/// Decreass the current value of the HUDProgressBar subscribed as Experience bar by a given amount.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="decrement">The value decrement.</param>
		public static HUDProgressBar DecreaseExperienceBarValue (int decrement) {
			
			return Controller.DecreaseExperienceBarValue (decrement);
		}
		
		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed as Experience bar during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar SetExperienceBarValueOverTime (float targetValue, float secondsOfDuration) {
			
			return Controller.SetExperienceBarValueOverTime (targetValue, secondsOfDuration);
		}
		
		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed as Experience bar during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar SetExperienceBarValueOverTime (int targetValue, float secondsOfDuration) {
			
			return Controller.SetExperienceBarValueOverTime (targetValue, secondsOfDuration);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as Experience bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="increment">Value increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar IncreaseExperienceBarValueOverTime (float increment, float secondsOfDuration) {
			
			return Controller.IncreaseExperienceBarValueOverTime (increment, secondsOfDuration);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as Experience bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="increment">Value increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar IncreaseExperienceBarValueOverTime (int increment, float secondsOfDuration) {
			
			return Controller.IncreaseExperienceBarValueOverTime (increment, secondsOfDuration);
		}
		
		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed as Experience bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="decrement">Value decrement.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar DecreaseExperienceBarValueOverTime (float decrement, float secondsOfDuration) {
			
			return Controller.DecreaseExperienceBarValueOverTime (decrement, secondsOfDuration);
		}
		
		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed as Experience bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Experience bar.</returns>
		/// <param name="decrement">Value decrement.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar DecreaseExperienceBarValueOverTime (int decrement, float secondsOfDuration) {
			
			return Controller.DecreaseExperienceBarValueOverTime (decrement, secondsOfDuration);
		}


		
		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed as Level bar.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetLevelBarValue (float newValue) {
			
			return Controller.SetLevelBarValue (newValue);
		}
		
		/// <summary>
		/// Sets the current value of the HUDProgressBar subscribed as Level bar.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetLevelBarValue (int newValue) {
			
			return Controller.SetLevelBarValue (newValue);
		}
		
		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed as Level bar.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetLevelBarMaximumValue (float newValue) {
			
			return Controller.SetLevelBarMaximumValue (newValue);
		}
		
		/// <summary>
		/// Sets the maximum value of the HUDProgressBar subscribed as Level bar.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetLevelBarMaximumValue (int newValue) {
			
			return Controller.SetLevelBarMaximumValue (newValue);
		}
		
		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed as Level bar.
		/// </summary>
		/// <returns>The health bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetLevelBarMinimumValue (float newValue) {
			
			return Controller.SetLevelBarMinimumValue (newValue);
		}
		
		/// <summary>
		/// Sets the minimum value of the HUDProgressBar subscribed as Level bar.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="newValue">New value.</param>
		public static HUDProgressBar SetLevelBarMinimumValue (int newValue) {
			
			return Controller.SetLevelBarMinimumValue (newValue);
		}
		
		/// <summary>
		/// Shows the Level bar.
		/// </summary>
		/// <returns>The Level bar.</returns>
		public static HUDProgressBar ShowLevelBar () {
			
			return Controller.ShowLevelBar ();
		}
		
		/// <summary>
		/// Hides the Level bar.
		/// </summary>
		/// <returns>The Level bar.</returns>
		public static HUDProgressBar HideLevelBar () {
			
			return Controller.HideLevelBar ();
		}
		
		/// <summary>
		/// Shows and hides the Level bar after some seconds have passed.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="secondsToHide">Seconds to hide.</param>
		public static HUDProgressBar ShowAndHideLevelBar (float secondsToHide) {
			
			return Controller.ShowAndHideLevelBar (secondsToHide);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as Level bar by a given amount.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="increment">The value increment.</param>
		public static HUDProgressBar IncreaseLevelBarValue (float increment) {
			
			return Controller.IncreaseLevelBarValue (increment);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as Level bar by a given amount.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="increment">The value increment.</param>
		public static HUDProgressBar IncreaseLevelBarValue (int increment) {
			
			return Controller.IncreaseLevelBarValue (increment);
		}
		
		/// <summary>
		/// Decreass the current value of the HUDProgressBar subscribed as Level bar by a given amount.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="decrement">The value decrement.</param>
		public static HUDProgressBar DecreaseLevelBarValue (float decrement) {
			
			return Controller.DecreaseLevelBarValue (decrement);
		}
		
		/// <summary>
		/// Decreass the current value of the HUDProgressBar subscribed as Level bar by a given amount.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="decrement">The value decrement.</param>
		public static HUDProgressBar DecreaseLevelBarValue (int decrement) {
			
			return Controller.DecreaseLevelBarValue (decrement);
		}
		
		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed as Level bar during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar SetLevelBarValueOverTime (float targetValue, float secondsOfDuration) {
			
			return Controller.SetLevelBarValueOverTime (targetValue, secondsOfDuration);
		}
		
		/// <summary>
		/// Modifies the current value of the HUDProgressBar subscribed as Level bar during the specified time, until a given value is reached.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="targetValue">Target value.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar SetLevelBarValueOverTime (int targetValue, float secondsOfDuration) {
			
			return Controller.SetLevelBarValueOverTime (targetValue, secondsOfDuration);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as Level bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="increment">Value increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar IncreaseLevelBarValueOverTime (float increment, float secondsOfDuration) {
			
			return Controller.IncreaseLevelBarValueOverTime (increment, secondsOfDuration);
		}
		
		/// <summary>
		/// Increases the current value of the HUDProgressBar subscribed as Level bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="increment">Value increment.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar IncreaseLevelBarValueOverTime (int increment, float secondsOfDuration) {
			
			return Controller.IncreaseLevelBarValueOverTime (increment, secondsOfDuration);
		}
		
		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed as Level bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="decrement">Value decrement.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar DecreaseLevelBarValueOverTime (float decrement, float secondsOfDuration) {
			
			return Controller.DecreaseLevelBarValueOverTime (decrement, secondsOfDuration);
		}
		
		/// <summary>
		/// Decreases the current value of the HUDProgressBar subscribed as Level bar by a given amount during the specified time.
		/// </summary>
		/// <returns>The Level bar.</returns>
		/// <param name="decrement">Value decrement.</param>
		/// <param name="secondsOfDuration">Seconds of duration.</param>
		public static HUDProgressBar DecreaseLevelBarValueOverTime (int decrement, float secondsOfDuration) {
			
			return Controller.DecreaseLevelBarValueOverTime (decrement, secondsOfDuration);
		}


		# endregion
	}
}

