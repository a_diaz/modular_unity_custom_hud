﻿using UnityEngine;

namespace CustomHUD {

	public class HUDEvents {

		# region delegates

		public delegate void OnHUDInitializedDelegate ();

		public delegate void OnHUDControllerInitializedDelegate (HUDController initializedController);

		public delegate void OnHUDControllerDisabledDelegate ();

		public delegate void OnHUDDestroyedDelegate ();

		# endregion


		# region events

		// TODO generate summary for events

		public static OnHUDInitializedDelegate OnHUDInitializedHandler;

		public static OnHUDControllerInitializedDelegate OnHUDControllerInitializedHandler;

		public static OnHUDControllerDisabledDelegate OnHUDControllerDisabledHandler;

		public static OnHUDDestroyedDelegate OnHUDDestroyedHandler;
		
	
		# endregion

		# region methods



		# endregion

		# region notify events

		/// <summary>
		/// Throws the even that the HUD was Initialized.
		/// </summary>
		public void NotifyHUDInitialized () {

			if (OnHUDInitializedHandler != null) {
			
				OnHUDInitializedHandler ();
			}
		}

		/// <summary>
		/// Throws the even that the HUD was Destroyed.
		/// </summary>
		public void NotifyHUDDestroyed () {
			
			if (OnHUDDestroyedHandler != null) {
				
				OnHUDDestroyedHandler ();
			}
		}

		/// <summary>
		/// Throws the event that a HUDController has been initialized in the scene.
		/// </summary>
		/// <param name="initializedController">Initialized controller.</param>
		public void NotifyHUDControllerInitialized (HUDController initializedController) {

			if (OnHUDControllerInitializedHandler != null) {
			
				OnHUDControllerInitializedHandler (initializedController);
			}
		}

		/// <summary>
		/// Throws the event that the HUDController has been disabled in the hierarchy.
		/// </summary>
		public void NotifyHUDControllerDisabled () {
			
			if (OnHUDControllerDisabledHandler != null) {
				
				OnHUDControllerDisabledHandler ();
			}
		}

		# endregion
	}


}
