﻿using UnityEngine;
using System.Collections;

public static class ExtensionMethods {

	# region transform

	/// <summary>
	/// Reset the transform.
	/// </summary>
	/// <param name="trans">Trans.</param>
	public static void Reset (this Transform trans)
	{
		trans.position 		= Vector3.zero;
		trans.localRotation = Quaternion.identity;

		trans.localScale 	= new Vector3(1, 1, 1);
	}

	/// <summary>
	/// Resets the global rotation.
	/// </summary>
	/// <param name="trans">Trans.</param>
	public static void ResetRotation (this Transform trans) {

		trans.rotation = Quaternion.identity;
	}

	/// <summary>
	/// Resets the localRotation.
	/// </summary>
	/// <param name="trans">Trans.</param>
	public static void ResetLocalRotation (this Transform trans) {
		
		trans.localRotation = Quaternion.identity;
	}

	/// <summary>
	/// Sets the global position.
	/// </summary>
	/// <param name="trans">Trans.</param>
	/// <param name="newX">New x.</param>
	/// <param name="newY">New y.</param>
	/// <param name="newZ">New z.</param>
	public static void SetPos (this Transform trans, float newX, float newY, float newZ) {

		trans.position = new Vector3 (newX, newY, newZ);
	}

	/// <summary>
	/// Sets the position x.
	/// </summary>
	/// <param name="trans">Trans.</param>
	/// <param name="newX">New x.</param>
	public static void SetPosX (this Transform trans, float newX) {

		trans.position = new Vector3 (newX, trans.position.y, trans.position.z);
	}

	/// <summary>
	/// Sets the position y.
	/// </summary>
	/// <param name="trans">Trans.</param>
	/// <param name="newY">New y.</param>
	public static void SetPosY (this Transform trans, float newY) {
		
		trans.position = new Vector3 (trans.position.x, newY, trans.position.z);
	}

	/// <summary>
	/// Sets the position z.
	/// </summary>
	/// <param name="trans">Trans.</param>
	/// <param name="newZ">New z.</param>
	public static void SetPosZ (this Transform trans, float newZ) {
		
		trans.position = new Vector3 (trans.position.x, trans.position.y, newZ);
	}

	/// <summary>
	/// Resets the position.
	/// </summary>
	/// <param name="trans">Trans.</param>
	public static void ResetPos (this Transform trans) {
		
		trans.position = new Vector3 (0.0f, 0.0f, 0.0f);
	}

	/// <summary>
	/// Resets the position x.
	/// </summary>
	/// <param name="trans">Trans.</param>
	public static void ResetPosX (this Transform trans) {

		SetPosX (trans, 0.0f);
	}

	/// <summary>
	/// Resets the position y.
	/// </summary>
	/// <param name="trans">Trans.</param>
	public static void ResetPosY (this Transform trans) {
		
		SetPosY (trans, 0.0f);
	} 

	/// <summary>
	/// Resets the position z.
	/// </summary>
	/// <param name="trans">Trans.</param>
	public static void ResetPosZ (this Transform trans) {
		
		SetPosZ (trans, 0.0f);
	}

	/// <summary>
	/// Sets the local position.
	/// </summary>
	/// <param name="trans">Trans.</param>
	/// <param name="newX">New x.</param>
	/// <param name="newY">New y.</param>
	/// <param name="newZ">New z.</param>
	public static void SetLocalPos (this Transform trans, float newX, float newY, float newZ) {
		
		trans.localPosition = new Vector3 (newX, newY, newZ);
	}

	/// <summary>
	/// Sets the local position x.
	/// </summary>
	/// <param name="trans">Trans.</param>
	/// <param name="newX">New x.</param>
	public static void SetLocalPosX (this Transform trans, float newX) {
		
		trans.localPosition = new Vector3 (newX, trans.localPosition.y, trans.localPosition.z);
	}

	/// <summary>
	/// Sets the local position y.
	/// </summary>
	/// <param name="trans">Trans.</param>
	/// <param name="newY">New y.</param>
	public static void SetLocalPosY (this Transform trans, float newY) {
		
		trans.localPosition = new Vector3 (trans.localPosition.x, newY, trans.localPosition.z);
	}

	/// <summary>
	/// Sets the local position z.
	/// </summary>
	/// <param name="trans">Trans.</param>
	/// <param name="newZ">New z.</param>
	public static void SetLocalPosZ (this Transform trans, float newZ) {
		
		trans.localPosition = new Vector3 (trans.localPosition.x, trans.localPosition.y, newZ);
	}

	/// <summary>
	/// Resets the local position.
	/// </summary>
	/// <param name="trans">Trans.</param>
	public static void ResetLocalPos (this Transform trans) {

		trans.localPosition = new Vector3 (0.0f, 0.0f, 0.0f);
	}
	
	/// <summary>
	/// Resets the local position x.
	/// </summary>
	/// <param name="trans">Trans.</param>
	public static void ResetLocalPosX (this Transform trans) {
		
		SetLocalPosX (trans, 0.0f);
	}

	/// <summary>
	/// Resets the local position y.
	/// </summary>
	/// <param name="trans">Trans.</param>
	public static void ResetLocalPosY (this Transform trans) {
		
		SetLocalPosY (trans, 0.0f);
	} 

	/// <summary>
	/// Resets the local position z.
	/// </summary>
	/// <param name="trans">Trans.</param>
	public static void ResetLocalPosZ (this Transform trans) {
		
		SetLocalPosZ (trans, 0.0f);
	}

	/// <summary>
	/// Scales this transform proportionally in every axis by a given factor.
	/// </summary>
	/// <param name="trans">Trans.</param>
	/// <param name="factor">Factor.</param>
	public static void ScaleProportional (this Transform trans, float factor) {

		trans.localScale = trans.localScale * factor;
	}

	/// <summary>
	/// Scales each axis of this transform by a given factor. A factor of 1.0f has no effect on an axis.
	/// </summary>
	/// <param name="trans">Trans.</param>
	/// <param name="factorX">Factor x.</param>
	/// <param name="factorY">Factor y.</param>
	/// <param name="factorZ">Factor z.</param>
	public static void ScaleProportional (this Transform trans, float factorX, float factorY, float factorZ) {

		trans.localScale = new Vector3 (trans.localScale.x * factorX, trans.localScale.y * factorY, trans.localScale.z * factorZ);
	}

	/// <summary>
	/// Sets the scale to a new value in all axis.
	/// </summary>
	/// <param name="trans">Transform.</param>
	/// <param name="newScale">New scale.</param>
	public static void SetLocalScaleProportional (this Transform trans, float newScale) {

		trans.localScale = new Vector3 (newScale, newScale, newScale);
	}
	
	/// <summary>
	/// Sets the euler angles.
	/// </summary>
	/// <param name="trans">Trans.</param>
	/// <param name="newEulerX">New euler x.</param>
	/// <param name="newEulerY">New euler y.</param>
	/// <param name="newEulerZ">New euler z.</param>
	public static void SetEulerAngles (this Transform trans, float newEulerX, float newEulerY, float newEulerZ) {

		trans.eulerAngles = new Vector3 (newEulerX, newEulerY, newEulerZ);
	}

	/// <summary>
	/// Sets the local euler angles.
	/// </summary>
	/// <param name="trans">Trans.</param>
	/// <param name="newEulerX">New euler x.</param>
	/// <param name="newEulerY">New euler y.</param>
	/// <param name="newEulerZ">New euler z.</param>
	public static void SetLocalEulerAngles (this Transform trans, float newEulerX, float newEulerY, float newEulerZ) {
		
		trans.localEulerAngles = new Vector3 (newEulerX, newEulerY, newEulerZ);
	}


	# endregion

	# region debug




	# endregion

	# region camera

	/// <summary>
	/// Sets the Y coordinate of the rect of the camera.
	/// </summary>
	/// <param name="cam">The camera.</param>
	/// <param name="newY">New Y coordinate.</param>
	public static void SetYCoord (this Camera cam, float newY) {

//		cam.rect = new Rect (cam.rect.left, newY, cam.rect.width, cam.rect.height);
		cam.rect = new Rect (cam.rect.xMin, newY, cam.rect.width, cam.rect.height);
	}

	# endregion

	# region color

	/// <summary>
	/// Sets the alpha.
	/// </summary>
	/// <param name="c">C.</param>
	/// <param name="newAlpha">New alpha.</param>
	public static void SetAlpha (this Color c, float newAlpha) {

		newAlpha = Mathf.Clamp (newAlpha, 0.0f, 1.0f);
		c = new Color (c.r, c.g, c.b, newAlpha);
	}

	/// <summary>
	/// Sets the red.
	/// </summary>
	/// <param name="c">C.</param>
	/// <param name="newRed">New red.</param>
	public static void SetRed (this Color c, float newRed) {

		newRed = Mathf.Clamp (newRed, 0.0f, 1.0f);
		c = new Color (newRed, c.g, c.b, c.a);
	}

	/// <summary>
	/// Sets the green.
	/// </summary>
	/// <param name="c">C.</param>
	/// <param name="newGreen">New green.</param>
	public static void SetGreen (this Color c, float newGreen) {
		
		newGreen = Mathf.Clamp (newGreen, 0.0f, 1.0f);
		c = new Color (c.r, newGreen, c.b, c.a);
	}

	/// <summary>
	/// Sets the blue.
	/// </summary>
	/// <param name="c">C.</param>
	/// <param name="newBlue">New blue.</param>
	public static void SetBlue (this Color c, float newBlue) {
		
		newBlue = Mathf.Clamp (newBlue, 0.0f, 1.0f);
		c = new Color (c.r, c.g, newBlue, c.a);
	}

	# endregion

	# region vectors

	public static void SetX (this Vector2 v, float newX) {

		v.x = newX;
	}

	public static void SetY (this Vector2 v, float newY) {
		
		v.y = newY;
	}

	public static void SetX (this Vector3 v, float newX) {
		
		v.x = newX;
	}
	
	public static void SetY (this Vector3 v, float newY) {
		
		v.y = newY;
	}

	public static void SetZ (this Vector3 v, float newZ) {
		
		v.z = newZ;
	}

	# endregion
}
