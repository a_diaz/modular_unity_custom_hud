using UnityEngine;
using System.Collections;

namespace CustomHUD {

	/// <summary>
	/// A class that only holds constant values to be used in the Editor.
	/// </summary>
	public class HUDConst {

		/// <summary>
		/// The default size in X for popups.
		/// </summary>
		public const int EDITOR_POPUP_SIZE_X = 500;

		/// <summary>
		/// The default size in Y for popups.
		/// </summary>
		public const int EDITOR_POPUP_SIZE_Y = 200;

		/// <summary>
		/// The default reference resolution in X for the Canvases.
		/// </summary>
		public const int DEFAULT_REFERENCE_RESOLUTION_X = 1920;

		/// <summary>
		/// The default reference resolution in Y for the Canvases.
		/// </summary>
		public const int DEFAULT_REFERENCE_RESOLUTION_Y = 1080;

		/// <summary>
		/// The default window size in X for the Editor windows.
		/// </summary>
		public const int DEFAULT_WINDOW_SIZE_X = 650;

		/// <summary>
		/// The default window size in Y for the Editor windows.
		/// </summary>
		public const int DEFAULT_WINDOW_SIZE_Y = 620;

		/// <summary>
		/// The min width for all input fields of various types.
		/// </summary>
		public const int MIN_INPUT_WIDTH = 300;

		/// <summary>
		/// The min width for all labels
		/// </summary>
		public const int MIN_LABEL_WIDTH = 200;

		# region pivot values

		/// <summary>
		/// The pivot X value for the RectTransform of the HUDElements under the Top HUDAnchor.		
		/// </summary>
		public const float PIVOT_X_FOR_TOP_ANCHOR = 0.5f;
		
		/// <summary>
		/// The pivot Y value for the RectTransform of the HUDElements under the Top HUDAnchor.		
		/// </summary>
		public const float PIVOT_Y_FOR_TOP_ANCHOR = 1.0f;

		/// <summary>
		/// The pivot X value for the RectTransform of the HUDElements under the Left HUDAnchor.		
		/// </summary>
		public const float PIVOT_X_FOR_LEFT_ANCHOR = 0.0f;
		
		/// <summary>
		/// The pivot Y value for the RectTransform of the HUDElements under the Left HUDAnchor.		
		/// </summary>
		public const float PIVOT_Y_FOR_LEFT_ANCHOR = 0.5f;

		/// <summary>
		/// The pivot X value for the RectTransform of the HUDElements under the Right HUDAnchor.		
		/// </summary>
		public const float PIVOT_X_FOR_RIGHT_ANCHOR = 1.0f;
		
		/// <summary>
		/// The pivot Y value for the RectTransform of the HUDElements under the Right HUDAnchor.		
		/// </summary>
		public const float PIVOT_Y_FOR_RIGHT_ANCHOR = 0.5f;
		
		/// <summary>
		/// The pivot X value for the RectTransform of the HUDElements under the Bottom HUDAnchor.		
		/// </summary>
		public const float PIVOT_X_FOR_BOTTOM_ANCHOR = 0.5f;
		
		/// <summary>
		/// The pivot Y value for the RectTransform of the HUDElements under the Bottom HUDAnchor.		
		/// </summary>
		public const float PIVOT_Y_FOR_BOTTOM_ANCHOR = 0.0f;

		/// <summary>
		/// The pivot X value for the RectTransform of the HUDElements under the Center HUDAnchor.		
		/// </summary>
		public const float PIVOT_X_FOR_CENTER_ANCHOR = 0.5f;
		
		/// <summary>
		/// The pivot Y value for the RectTransform of the HUDElements under the Center HUDAnchor.		
		/// </summary>
		public const float PIVOT_Y_FOR_CENTER_ANCHOR = 0.5f;

		/// <summary>
		/// The minimum value for the anchors in the RectTranform.
		/// </summary>
		public const float ANCHOR_MIN_VALUE = 0.0f;

		/// <summary>
		/// The middle value for the anchors in the RectTranform.
		/// </summary>
		public const float ANCHOR_MID_VALUE = 0.5f;

		/// <summary>
		/// The maximum value for the anchors in the RectTranform.
		/// </summary>
		public const float ANCHOR_MAX_VALUE = 1.0f;

		# endregion

		# region animation triggers

		public const string SLIDE_OUT_TRIGGER = "slideOut";
		public const string SLIDE_LEFT_IN_TRIGGER = "slideInLeft";
		public const string SLIDE_RIGHT_IN_TRIGGER = "slideInRight";
		public const string SLIDE_TOP_IN_TRIGGER = "slideInTop";
		public const string SLIDE_BOTTOM_IN_TRIGGER = "slideInBottom";

		# endregion
	}
}