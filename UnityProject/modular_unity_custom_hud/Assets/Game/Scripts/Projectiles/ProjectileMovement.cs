﻿using UnityEngine;
using System.Collections;

public class ProjectileMovement : MonoBehaviour {
	
	public Transform projectileBody;
	public Transform spinningTransform;
	
	public bool spinAround;
	public bool flyForward;
	
	public float spinAroundSpeed;
	public float flySpeed;
	
	// Update is called once per frame
	void Update () {
		
		
		if (spinAround) {
			
			spinningTransform.GetComponent<Transform> ().Rotate (Vector3.forward, spinAroundSpeed * Time.deltaTime, Space.Self);
		}
		
		if (flyForward) {
			
			float movement = flySpeed * Time.deltaTime;
			projectileBody.Translate ( 0.0f, 0.0f, movement, Space.Self);
		}
	}
}
