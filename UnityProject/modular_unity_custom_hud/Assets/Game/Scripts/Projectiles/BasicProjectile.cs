﻿using UnityEngine;
using System.Collections;

public class BasicProjectile : Projectile {

	public Transform rear;
	public Transform front;

//	private float _projectileLength;

	void Start () {

		//_projectileLength = Vector3.Distance (rear.position, front.position);
	}

	void OnTriggerEnter (Collider other) {

		if (other.tag == "Enemy") {
		
			SceneController.Instance.ProjectileCollisonEnemy (this, other.GetComponent<Enemy> ());

			RecycleMe ();
		}

	}

//	void Update () {
//
//		// compute collisions
//			RaycastHit hit;
//		
//		if (Physics.Raycast (rear.position, rear.forward, out hit, GetComponent<ProjectileMovement> ().flySpeed * Time.deltaTime)) {
//			
//			if (hit.collider.tag == "Enemy") {
//				
//				SceneController.Instance.ProjectileCollisonEnemy (this, hit.collider.GetComponent<Enemy> ());
//			}
//		}


//
//	}
}
