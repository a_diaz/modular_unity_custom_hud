﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (ProjectileMovement))]
public abstract class Projectile : MonoBehaviour {

	public enum ProjectyleType {
		
		Basic = 0
	}

	private ProjectilePool _pool;

	public ProjectilePool pool {

		get { return _pool; } 
	}
	
	public int damage {

		get { return _pool.damage; }
	}


	public void Ready (Transform coordinates, ProjectilePool originalPool) {

		GetComponent<Transform> ().position = coordinates.position;
		GetComponent<Transform> ().rotation = coordinates.rotation;

		_pool = originalPool;
	}

	public void Launch () {

		gameObject.SetActive (true);
		GetComponent <ProjectileMovement> ().flyForward = true;
		
	}

	public void Stop () {

		gameObject.SetActive (false);
		GetComponent <ProjectileMovement> ().flyForward = false;
	}

	public void RecycleMe () {

		_pool.RecycleProjectile (this);
	}
}
