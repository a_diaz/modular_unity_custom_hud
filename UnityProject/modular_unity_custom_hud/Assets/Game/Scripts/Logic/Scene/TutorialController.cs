﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using CustomHUD;

public class TutorialController : MonoBehaviour {

	public Sprite leftStickSprite;
	public Sprite rightStickSprite;
	public Sprite rightTriggerSprite;

	public AudioClip[] speeches;

	private List<TutorialStep> _tutorialSteps;
	private PlayerController _playerOne;

	private void PlaySpeech (int index) {
	
		GetComponent<AudioSource> ().PlayOneShot (speeches [index]);
	}

	public void BeginTutorial ( List<TutorialStep> steps, PlayerController player ) {
	
		_tutorialSteps = steps;
		_playerOne = player;

		TutorialStep (0);
	}  

	private void TutorialStep (int step) {
		
		
		switch (step) {
		case 0: StartCoroutine (TutorialStep0 ()); break;
		case 1: StartCoroutine (TutorialStep1 ()); break;
		case 2: StartCoroutine (TutorialStep2 ()); break;
		case 3: StartCoroutine (TutorialStep3 ()); break;
		case 4: StartCoroutine (TutorialStep4 ()); break;
		}
	}
	
	private IEnumerator TutorialStep0 () {
		
		yield return new WaitForSeconds (0.8f);
		
		// show hey
		//GetComponent<TutorialGUIDialogs> ().ShowSentence (_tutorialSteps [0]._sentence, 1.0f);

		CustomHUD.HUD.ShowAndHidePrimarySubtitle (_tutorialSteps[0]._sentence, 1.0f);
		PlaySpeech (0);

		
		yield return new WaitForSeconds (2.0f);
		
		TutorialStep (1);
		
	}
	
	private IEnumerator TutorialStep1 () {
		
		yield return new WaitForSeconds (0.8f);
		
		//GetComponent<TutorialGUIDialogs> ().ShowSentence (_tutorialSteps [1]._sentence, 1.0f);
		CustomHUD.HUD.ShowAndHidePrimarySubtitle (_tutorialSteps[1]._sentence, 1.0f);
		PlaySpeech (1);
		
		yield return new WaitForSeconds (2.0f);
		
		// show the HINT
		HUD.ShowActionHint ("Move", leftStickSprite);
		
		float timeMoved = 0.0f;
		_playerOne.canMove = true;
		
		while (timeMoved < 2.0f) {
			
			if (Input.GetAxis("Horizontal") + Input.GetAxis ("Vertical") > 0.01f) {
				
				timeMoved += Time.deltaTime;
			}
			
			yield return new WaitForEndOfFrame ();
		}
		
		// he has moved enough
		_playerOne.canMove = false;
		
		// hide the hint
		HUD.HideActionHint ();
		
		TutorialStep (2);
	}
	
	private IEnumerator TutorialStep2 () {
		
		yield return new WaitForSeconds (0.8f);
		
		//GetComponent<TutorialGUIDialogs> ().ShowSentence (_tutorialSteps [2]._sentence, 1.0f);
		CustomHUD.HUD.ShowAndHidePrimarySubtitle (_tutorialSteps[2]._sentence, 1.0f);
		PlaySpeech (2);
		
		yield return new WaitForSeconds (2.0f);
		
		// show the HINT
		HUD.ShowActionHint ("Turn", rightStickSprite);
		
		float timeAimed = 0.0f;
		_playerOne.canMove = true;
		_playerOne.canAimTurn = true;
		
		while (timeAimed < 3.0f) {
			
			if (Mathf.Abs((Input.GetAxis ("RightH")) + (Input.GetButton("Fire1") ? 1 : 0 ) + (Input.GetButton("Fire2") ? 1 : 0 ) ) > 0.01f) {
				
				timeAimed += Time.deltaTime;
			}
			
			yield return new WaitForEndOfFrame ();
		}
		
		// he has moved enough
		_playerOne.canMove = false;
		_playerOne.canAimTurn = false;
		
		// hide the hint
		HUD.HideActionHint ();
		
		TutorialStep (3);
	}
	
	private IEnumerator TutorialStep3 () {
		
		yield return new WaitForSeconds (0.8f);
		
		//GetComponent<TutorialGUIDialogs> ().ShowSentence (_tutorialSteps [3]._sentence, 1.0f);
		CustomHUD.HUD.ShowAndHidePrimarySubtitle (_tutorialSteps[3]._sentence, 1.0f);
		PlaySpeech (3);
		
		yield return new WaitForSeconds (2.0f);
		
		// show the HINT
		HUD.ShowActionHint ("Shoot", rightTriggerSprite);
		
		float timeShoot = 0.0f;
		_playerOne.canMove = true;
		_playerOne.canAimTurn = true;
		_playerOne.canShoot = true;
		
		while (timeShoot < 3.0f) {
			
			if ((Input.GetAxis ("RightTrigger") > 0.9f || Input.GetButton ("Jump") )) {
				
				timeShoot += Time.deltaTime;
			}
			
			yield return new WaitForEndOfFrame ();
		}
		
		// he has moved enough
		_playerOne.canMove = false;
		_playerOne.canAimTurn = false;
		_playerOne.canShoot = false;
		
		// hide the hint
		HUD.HideActionHint ();
		
		TutorialStep (4);
	}
	
	private IEnumerator TutorialStep4 () {
		
		yield return new WaitForSeconds (0.8f);
		
		// show hey
		//GetComponent<TutorialGUIDialogs> ().ShowSentence (_tutorialSteps [4]._sentence, 1.0f);
		CustomHUD.HUD.ShowAndHidePrimarySubtitle (_tutorialSteps[4]._sentence, 1.0f);
		PlaySpeech (4);
		
		
		yield return new WaitForSeconds (2.0f);
		
		TutorialFinished ();
	}

	private void TutorialFinished () {

		SceneController.Instance.OnTutorialHasEnded ();
	}
}
