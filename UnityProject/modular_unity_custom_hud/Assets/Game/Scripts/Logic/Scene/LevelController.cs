﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelController : MonoBehaviour {

	public struct EnemyCount {

		public int basicEnemyCount;
		public int enforcerEnemyCount;
	}

	private EnemyCount _enemiesInstanced;

	public int beginLevel;

	[SerializeField]
	private int _currentLevel;

	private List<LevelItem> _levels;

	public bool continuousWaves;

	[Range (0.0f, 1.0f)]
	public float timeSpawnRandomMinimum;

	[Range (1.0f, 3.0f)]
	public float timeSpawnRandomMaximum;
	// Use this for initialization
	void Start () {
	
	}

	public void StartLevels (List<LevelItem> levels) {

		_levels = levels;
		StartFromLevel (beginLevel);
	}

	public void StartFromLevel (int level) {

		continuousWaves = true;
		LaunchLevel (level);
	}

	private void LaunchLevel (int level) {

		_currentLevel = level;
		_enemiesInstanced = new EnemyCount {

			basicEnemyCount = _levels[level]._basicEnemyCount,
			enforcerEnemyCount = _levels[level]._enforcerEnemyCount
		};
		StartCoroutine (CoroutineSetUpLevel (level)); 

	}

	private void LaunchNextLevel () {
	
		LaunchLevel (++_currentLevel);
	}

	private IEnumerator CoroutineSetUpLevel (int level) {

		yield return new WaitForSeconds (0.8f);
		CustomHUD.HUD.ShowAndHideWarning (_levels [level]._name, 2.0f);
		yield return new WaitForSeconds (2.5f);
	
		for (int i = 0; i < _levels[level]._basicEnemyCount; i ++) {
		
			Invoke ("RequestSpawnBasicEnemy", UnityEngine.Random.Range (timeSpawnRandomMinimum, timeSpawnRandomMaximum));
		}

		for (int i = 0; i < _levels[level]._enforcerEnemyCount; i ++) {
			
			Invoke ("RequestSpawnEnforcerEnemy", UnityEngine.Random.Range (timeSpawnRandomMinimum, timeSpawnRandomMaximum));
		}
	}

	private void RequestSpawnBasicEnemy () { SceneController.Instance.SpawnBasicEnemy (OnEnemyRecycled); }

	private void RequestSpawnEnforcerEnemy () { SceneController.Instance.SpawnEnforcerEnemy (OnEnemyRecycled); }

	public void OnEnemyRecycled (Enemy recycledEnemy) {

		switch (recycledEnemy.enemyType) {
		
		case Enemy.EnemyTypes.Basic: _enemiesInstanced.basicEnemyCount--; break;
		case Enemy.EnemyTypes.Enforcer: _enemiesInstanced.enforcerEnemyCount--; break;
		}

		CheckLevelBeaten ();
	}

	private void CheckLevelBeaten () {

		if (_enemiesInstanced.basicEnemyCount <= 0
			&& _enemiesInstanced.enforcerEnemyCount <= 0) {
		
			// the level has been completed
			if (_currentLevel >= _levels.Count - 1) {

				CustomHUD.HUD.ShowAndHideWarning ("Cleared", 3.0f);
				SceneController.Instance.GameWasCleared ();
			}
			else {

				LaunchNextLevel ();
			}

		}
	}
}
