﻿using UnityEngine;
using System.Collections;


public static class PrefController {


	public static bool hasSeenTutorial {

		get { return PlayerPrefs.GetInt (hasSeenTutorialPrefKey, 0) == 1; }
		set { PlayerPrefs.SetInt (hasSeenTutorialPrefKey, (value) ? 1 : 0); }
	}

	private static string hasSeenTutorialPrefKey = "hasSeenTutorialPrefKey_001";




}
