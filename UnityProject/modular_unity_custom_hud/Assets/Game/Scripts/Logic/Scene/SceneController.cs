﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml;
using CustomHUD;

public class SceneController : MonoBehaviour {
	
	public bool debugFPS;
	
	void OnGUI () { 
		
		if (debugFPS) {
			
			GUI.Label (new Rect (10, 10, 150, 40), (1.0f / Time.deltaTime).ToString ("00.00"));
		} 
	}

	private static SceneController _Instance;
	
	public static SceneController Instance {
		
		get { return _Instance; }  
	}

	[Tooltip ("If the player has not seen the tutorial, plays it automatically when the app starts.")]
	public bool playTutorialAutomatically;

	[Range(0.05f, 1.5f)]
	public float timeToDecreaseHealthBar;

	[SerializeField]
	private Vector2 _areaBoundaries;

	public Vector2 areaBoundaries {

		get { return _areaBoundaries; } 
	}

	public TextAsset _levelInfoXML;
	public TextAsset _tutorialInfoXML;

	public Transform[] dummySpawners;

	public List<LevelItem> levels;
	public List<TutorialStep> tutorialSteps;

	[SerializeField]
	private PlayerController playerOne;

	public Transform playerTransform {

		get { return playerOne.transform; }
	}

	public Camera playerCamera;

	public float cameraSizeWhenPlaying;
	public float cameraSizeWhenMainMenu;
	public float cameraSizeLerpSpeed;

	public float cameraPositionZForMenu;

	void Awake () {

		_Instance = this;
	}

	void Start () {

		//Invoke ("SpawnRandomEnemy", 0.54f);

		//_levelInfoXML = Resources.Load ("/XML/level_definition") as TextAsset;

		// InvokeRepeating ("SpawnRandomEnemy", 0.54f, 2.5f);

		LoadLevelItems ();
		LoadTutorialSteps ();

		if (playTutorialAutomatically && !PrefController.hasSeenTutorial) {
		
			HideMainMenu ();
			StartTutorial ();
		}
		else {

			//StartSpawningEnemies ();
			//StartLevels ();
			ShowMainMenu ();

		}
	}

	private void LoadLevelItems () {

		levels = new List<LevelItem> ();

		XmlDocument levelsXML = new XmlDocument ();
	
		levelsXML.LoadXml (_levelInfoXML.text);
		XmlNodeList levelsList = levelsXML.GetElementsByTagName("levelitem");

		foreach (XmlNode level in levelsList) {

			LevelItem item = new LevelItem ();

			foreach (XmlNode levelParam in level.ChildNodes) {

				if (levelParam.Name == "name") { item._name = levelParam.InnerText; }
				if (levelParam.Name == "number") { item._number = int.Parse(levelParam.InnerText); }
				if (levelParam.Name == "basicenemycount") { item._basicEnemyCount = int.Parse(levelParam.InnerText); }
				if (levelParam.Name == "enforcerenemycount") { item._enforcerEnemyCount = int.Parse(levelParam.InnerText); }
			}

			levels.Add (item);
		}
	}

	private void LoadTutorialSteps () {

		tutorialSteps = new List<TutorialStep> ();

		XmlDocument tutorialsXML = new XmlDocument ();
		tutorialsXML.LoadXml (_tutorialInfoXML.text);

		XmlNodeList tutorialsList = tutorialsXML.GetElementsByTagName ("tutorialstep");

		foreach (XmlNode tutorial in tutorialsList) {
		
			TutorialStep step = new TutorialStep ();

			foreach (XmlNode tutorialParam in tutorial.ChildNodes) {

				if (tutorialParam.Name == "number") { step._number = int.Parse (tutorialParam.InnerText); }
				if (tutorialParam.Name == "sentence") { step._sentence = tutorialParam.InnerText; }
			}

			tutorialSteps.Add (step);
		}
	}

	private void ShowMainMenu () {

		playerCamera.orthographicSize = cameraSizeWhenMainMenu;
		playerCamera.GetComponent<Transform> ().localPosition = new Vector3 (playerCamera.GetComponent<Transform> ().localPosition.x,
		                                                                     playerCamera.GetComponent<Transform> ().localPosition.y,
		                                                                     cameraPositionZForMenu);
		MainMenuController.Instance.ShowButtons ();
	}

	private void HideMainMenu () {

		MainMenuController.Instance.HideButtons ();
	}

	private void StartTutorial () {

		playerOne.canMove = false;
		playerOne.canAimTurn = false;
		playerOne.canShoot = false;

		playerCamera.orthographicSize = cameraSizeWhenPlaying;
		playerOne.GetComponent<PlayerMovement> ().lerpCameraToTarget = true;
		GetComponent<TutorialController> ().BeginTutorial (tutorialSteps, playerOne);
	}


	public void OnTutorialHasEnded () {

//		StartSpawningEnemies ();
		StartLevels ();
		PrefController.hasSeenTutorial = true;
	}

	public void StartLevels () {
	
		HUD.ShowHealthBar ();
		playerOne.canMove = true;
		playerOne.canAimTurn = true;
		playerOne.canShoot = true;
		GetComponent<LevelController> () .StartLevels (levels);

	}

	// TESTING
	private void StartSpawningEnemies () {

		HUD.ShowHealthBar ();
		playerOne.canMove = true;
		playerOne.canAimTurn = true;
		playerOne.canShoot = true;

		InvokeRepeating ("SpawnRandomEnemy", 0.5f, 2.5f);
	}

	// TESTING PURPOSES
	void SpawnRandomEnemy () {
	
		Transform t = dummySpawners [UnityEngine.Random.Range (0, dummySpawners.Length)];

		if (UnityEngine.Random.Range (0.0f, 1.0f) > 0.050f) {

			PoolController.Instance.enforcerEnemies.InitEnemy (t);
		}
		else {

			PoolController.Instance.basicEnemies.InitEnemy (t);
		}
	
	}

	public void SpawnBasicEnemy (Action<Enemy> callback) {
	
		Transform t = dummySpawners [UnityEngine.Random.Range (0, dummySpawners.Length)];
		PoolController.Instance.basicEnemies.InitEnemy (t).GetComponent<CallbackOnEnemyRecycled> ().callback = callback;
	}

	public void SpawnEnforcerEnemy (Action<Enemy> callback) {
		
		Transform t = dummySpawners [UnityEngine.Random.Range (0, dummySpawners.Length)];
		PoolController.Instance.enforcerEnemies.InitEnemy (t).GetComponent<CallbackOnEnemyRecycled> ().callback = callback;
	}

	public void ProjectileCollisonEnemy (Projectile projectile, Enemy enemy) {

		bool killed = enemy.TakeDamage(projectile.damage);

		if (killed) {
		
			enemy.Die ();
		}
	}

	public void EnemyCollisionPlayer (Enemy enemy, PlayerController player) {
	
		bool killed = enemy.TakeDamage (player.damageOnCollision);

		if (killed) {

			enemy.Die ();
		}

		killed = player.TakeDamage (enemy.damageOnCollision);

		HUD.SetHealthBarValueOverTime (player.remainingLife, timeToDecreaseHealthBar);

		if (killed) {
		
			player.Die ();
			PlayerHasDied ();
		}
	}

	public void EnforcerBurstPlayer (EnforcerEnemy enemy, PlayerController player) {

		bool killed = player.TakeDamage (enemy.baseBurstDamage);
		HUD.SetHealthBarValueOverTime (player.remainingLife, timeToDecreaseHealthBar);
		
		if (killed) {
			
			PlayerHasDied ();
			player.Die ();
		}
		else {

			player.Stun ();
		}
	}

	private void PlayerHasDied () {

		CancelInvoke ("SpawnRandomEnemy");
		PoolController.Instance.PlayerHasDied ();
		HUD.HideHealthBar ();
		HUD.ShowAndHideWarning ("Defeated", 3.0f);
		Invoke ("TESTINGReset", 4.0f);
	}

	public void TESTINGPlayTutorial () {

		PrefController.hasSeenTutorial = false;
		Application.LoadLevel (Application.loadedLevel);
	}

	public void TESTINGReset () {

		Application.LoadLevel (Application.loadedLevel);
	}

	public void TESTINGExit () {

	#if !UNITY_EDITOR
		Application.Quit ();
	#else
		UnityEditor.EditorApplication.isPlaying = false;
	#endif
	
	}

	public void StartPlaying () {

		StartCoroutine (CoroutineSetCameraToPlay ());
	}

	private IEnumerator CoroutineSetCameraToPlay () {

		playerOne.GetComponent<PlayerMovement> ().lerpCameraToTarget = true;

		while (playerCamera.orthographicSize < cameraSizeWhenPlaying - 0.05f) {
		
			playerCamera.orthographicSize = Mathf.Lerp (playerCamera.orthographicSize, cameraSizeWhenPlaying,cameraSizeLerpSpeed * Time.deltaTime);
			yield return new WaitForEndOfFrame ();
		}

		playerCamera.orthographicSize = cameraSizeWhenPlaying;
		StartLevels ();
	}

	public void GameWasCleared () {

		Invoke ("TESTINGReset", 4.0f);
	}
}
