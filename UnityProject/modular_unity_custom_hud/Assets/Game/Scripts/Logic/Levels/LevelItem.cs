﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class LevelItem {

	public string _name;
	public int _number;
	public int _basicEnemyCount; 
	public int _enforcerEnemyCount;
}
