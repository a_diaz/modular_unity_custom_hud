﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class TutorialStep {

	public string _sentence;
	public int _number;

}
