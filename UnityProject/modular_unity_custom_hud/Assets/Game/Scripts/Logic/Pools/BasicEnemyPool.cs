﻿using UnityEngine;
using System.Collections;

public class BasicEnemyPool : EnemyPool {
	
	public AudioClip soundOnHit;
	public AudioClip soundOnDeath;

	public float baseAccelerationFactor;
	public float baseTurnToTargetSpeed;
	
	public Color startColor;
	public Color finalColor;

	public bool randomizeStats;
	public float statRandomizerPercent;


	public override Enemy InitEnemy (Transform coordinates) {
		
		BasicEnemy enemy = ((BasicEnemy) _enemies [_nextEnemy]);
		
		enemy.Ready (coordinates, this);
		enemy.Spawn ();
		
		_nextEnemy = (_nextEnemy + 1) % count;
		
		return enemy;
	}

	public override void PlayerHasDied () {

		for (int i = 0; i < _enemies.Count; i++) {
		
			((BasicEnemy) _enemies[i]).StopMoving ();
		}
	}
}
