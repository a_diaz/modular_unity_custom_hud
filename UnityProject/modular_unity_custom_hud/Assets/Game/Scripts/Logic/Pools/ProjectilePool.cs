﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class ProjectilePool : MonoBehaviour {

	protected List<Projectile> _projectiles;
	protected int _nextProjectile;

	public AudioClip soundOnShoot;

	public float secondsBetweenShots;

	public int damage;

	public int count {

		get { return _projectiles.Count; }
	}

	// Use this for initialization
	void Start () {
	
		FillProjectileListWithChildren ();
	}

	protected void FillProjectileListWithChildren () {

		_projectiles = new List<Projectile> ();

		foreach (Transform t in GetComponent<Transform> ()) {
		
			if (t.GetComponent<Projectile> () != null) {

				_projectiles.Add (t.GetComponent<Projectile> ());
			}
		}

		_nextProjectile = 0;
	}

	public abstract Projectile InitProjectile (Transform coordinates);

	public void RecycleProjectile (Projectile projectile) {

		projectile.Stop ();
	}
}
