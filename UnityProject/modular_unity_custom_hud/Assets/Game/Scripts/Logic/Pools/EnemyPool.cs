﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class EnemyPool : MonoBehaviour {

	protected List<Enemy> _enemies;
	protected int _nextEnemy;
	
	public int count {
		
		get { return _enemies.Count; }
	}

	public int damageOnCollision;

	public int startingLife;

	public float baseMaximumMovementSpeed;

	// Use this for initialization
	void Start () {
		
		FillEnemyListWithChildren ();
	}
	
	protected void FillEnemyListWithChildren () {
		
		_enemies = new List<Enemy> ();
		
		foreach (Transform t in GetComponent<Transform> ()) {
			
			if (t.GetComponent<Enemy> () != null) {
				
				_enemies.Add (t.GetComponent<Enemy> ());
			}
		}
		
		_nextEnemy = 0;
		
	}
	
	public abstract Enemy InitEnemy (Transform coordinates);
	
	public void RecycleEnemy (Enemy enemy) {
		
		enemy.Stop ();
	}

	public abstract void PlayerHasDied ();
}
