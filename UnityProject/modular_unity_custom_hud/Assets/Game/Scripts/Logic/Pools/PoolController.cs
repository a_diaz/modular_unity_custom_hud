﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolController : MonoBehaviour {

	private static PoolController _Instance;

	public static PoolController Instance {

		get { return _Instance; }
	}


	public BasicProjectilePool basicProjectiles;

	public BasicEnemyPool basicEnemies;

	public EnforcerEnemyPool enforcerEnemies;


	void Awake () {
		
		_Instance = this;
	}

	public void PlayerHasDied () {

		basicEnemies.PlayerHasDied ();
		enforcerEnemies.PlayerHasDied ();
	}
}
