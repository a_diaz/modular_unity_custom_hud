﻿using UnityEngine;
using System.Collections;

public class BasicProjectilePool : ProjectilePool {

	public override Projectile InitProjectile (Transform coordinates) {

		BasicProjectile projectile = ((BasicProjectile)_projectiles [_nextProjectile]);

		projectile.Ready (coordinates, this);
		projectile.Launch ();

		_nextProjectile = (_nextProjectile + 1) % count;

		return projectile;
	}
}
