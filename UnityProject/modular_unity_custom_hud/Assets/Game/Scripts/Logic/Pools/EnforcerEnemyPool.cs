﻿using UnityEngine;
using System.Collections;

	public class EnforcerEnemyPool : EnemyPool {

	public AudioClip soundOnBurst;

	public float baseSecondsToAreaBurst;
	public int baseBurstDamage;
	public float baseBurstForce;

	public Color startEmissiveColor;
	public Color finalEmissiveColor;

	public override Enemy InitEnemy (Transform coordinates) {
		
		EnforcerEnemy enemy = ((EnforcerEnemy) _enemies [_nextEnemy]);
		
		enemy.Ready (coordinates, this);
		enemy.Spawn ();
		
		_nextEnemy = (_nextEnemy + 1) % count;
		
		return enemy;
	}

	
	public override void PlayerHasDied () {
		
		for (int i = 0; i < _enemies.Count; i++) {
			
			((EnforcerEnemy) _enemies[i]).StopMoving ();
		}
	}
}
