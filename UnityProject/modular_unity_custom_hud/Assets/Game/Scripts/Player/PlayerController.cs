﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {



	[SerializeField]
	private PlayerMovement _movement;

	public PlayerMovement movement {

		get { return _movement; }
	}

	[SerializeField]
	private UnityStandardAssets.ImageEffects.MotionBlur _blur;

	[Range (0.0f, 0.9f)]
	public float blurOnSteady;

	[Range (0.0f, 0.9f)]
	public float blurOnStun;

	public bool spinAround {

		get { return movement.spinAround; }
		set { movement.spinAround = value; }
	}
	public bool canMove {
		
		get { return movement.canMove; }
		set { movement.canMove = value; }
	}
	public bool canAimTurn {
		
		get { return movement.canAimTurn; }
		set { movement.canAimTurn = value; }
	}
	public bool canShoot {
		
		get { return movement.canShoot; }
		set { movement.canShoot = value; }
	}

	public AudioClip soundOnDeath;

	public AudioSource audioSource;

	public int maximumLife = 100;

	private int _remainignLife = 100;

	public int remainingLife {

		get { return _remainignLife; }
	}

	public int damageOnCollision;

	public ProjectilePool startingProjectilePool;

	private ProjectilePool _currentProjectilePool;

	public ProjectilePool currentProjectilePool {

		get { return _currentProjectilePool; }
	}

	public TrailRenderer[] playerTrails;

	void Start () {

		InitializePlayer ();
	}

	private void InitializePlayer () {
	
		_remainignLife = maximumLife;
		UpdateProjectilePool (startingProjectilePool);
	}

	public bool TakeDamage (int damage) {

		IncrementLife (-damage);
		CheckRemainingLifeEffects ();
		return _remainignLife == 0;
	}

	public void Heal (int amount) {
	
		IncrementLife (amount);
	}

	private void IncrementLife (int increment) {

		_remainignLife = Mathf.Clamp (_remainignLife + increment, 0, maximumLife);
	}



	public void Stun () {

		canMove = false;
		Invoke ("RecoverStun", 0.9f);

		_blur.blurAmount = blurOnStun;
		_blur.extraBlur = true;

	}

	private void RecoverStun () {

		canMove = true;
		_blur.blurAmount = blurOnSteady;
		_blur.extraBlur = true;
	}

	public void Die () {

		canMove = false;
		canShoot = false;
		CancelInvoke ("RecoverStun");
		audioSource.PlayOneShot (soundOnDeath);
	}

	public void UpdateProjectilePool (ProjectilePool newPool) {

		_currentProjectilePool = newPool;

		GetComponent<PlayerMovement> ().UpdateProjectilePool ();
	}

	public void CheckRemainingLifeEffects () {

		float percent = (((float)remainingLife) / maximumLife);

		Color targetColor = movement.insideCube.GetComponent<MeshRenderer> ().material.color;
		targetColor = new Color (targetColor.r, targetColor.g, targetColor.b, percent);

		movement.insideCube.GetComponent<MeshRenderer> ().material.color = targetColor;

		for (int i = 0; i < playerTrails.Length; i++) {
		
			playerTrails[i].material.color = targetColor;
		}

		for (int i = 0; i < movement.aimingGizmos.Length; i++) {
		
			movement.aimingGizmos[i].material.color = targetColor;
		}
	}
}
