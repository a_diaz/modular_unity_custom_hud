﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public GameObject playerCamera;
	public bool lerpCameraToTarget;

	public Transform floorQuadTransform;
	public Vector3 localFloorBounds;
	
	public Transform playerCubeTransform;
	public GameObject insideCube;
	public Transform shootDirection;
	public Transform shootPoint;

	public AudioSource shootingPointAudio;

	public MeshRenderer[] aimingGizmos;
	
	
	public bool spinAround;
	public bool canMove;
	public bool canAimTurn;
	public bool canShoot;
	
	public float spinAroundSpeed;
	public float moveSpeed;
	public float cameraLerpSpeed;
	public float aimTurnSpeed;
	
	public float cameraDistanceInZ;

	private ProjectilePool _currentProjectilePool;

	private float _secondsBetweenShots;
	private float _secondsSinceLastShot;


	void Start () {
	
	}

	void Update () {
		
		
		if (spinAround) {
			
			insideCube.GetComponent<Transform> ().Rotate (Vector3.up, spinAroundSpeed * Time.deltaTime, Space.World);
		}
		
		if (canMove) {
			
			float movement = moveSpeed * Time.deltaTime;
			float movementX = Input.GetAxis("Horizontal") * movement;
			float movementZ = Input.GetAxis("Vertical") * movement;


			Vector3 direction = new Vector3 ( Input.GetAxis("Horizontal") * movement, 0.0f, Input.GetAxis("Vertical") * movement);
			Debug.DrawRay (playerCubeTransform.position, direction * 2000.0f, Color.red, 0.02f);

			playerCubeTransform.Translate ( movementX, 0.0f, movementZ, Space.World);



		}

		// the clamping of the local bounds should still happen even if we can't move, like if we have been blasted by an enforcer
		playerCubeTransform.localPosition = new Vector3 (Mathf.Clamp (playerCubeTransform.localPosition.x, - localFloorBounds.x, localFloorBounds.x),
		                                                 playerCubeTransform.localPosition.y,
		                                                 Mathf.Clamp (playerCubeTransform.localPosition.z, - localFloorBounds.z, localFloorBounds.z));

		
		if (canAimTurn) {
			
			shootDirection.Rotate (Vector3.up, 

#if UNITY_ANDROID
				Input.GetAxis ("RightHAndroid")
#else

				(Input.GetAxis ("RightH") + (Input.GetButton("Fire1") ? -1 : 0 ) + (Input.GetButton("Fire2") ? 1 : 0 ) ) 

#endif
			                       * aimTurnSpeed * Time.deltaTime);
		}
		
		// basic shooting behaviour
		if (canShoot) {
			
			if (_secondsSinceLastShot >= _secondsBetweenShots &&

#if UNITY_ANDROID
				Input.GetAxis ("RightTriggerAndroid") > 0.9f
#else

				(Input.GetAxis ("RightTrigger") > 0.9f || Input.GetButton ("Jump"))
#endif
				) {
				_currentProjectilePool.InitProjectile (shootPoint);
				shootingPointAudio.Play ();
				_secondsSinceLastShot = 0.0f;
			}
			else {

				_secondsSinceLastShot += Time.deltaTime;
			}
		}

		// yolo things, okkk
		if (Input.GetKeyDown (KeyCode.R)) {
		
			Application.LoadLevel (Application.loadedLevel);
		}

		if (Input.GetKeyDown (KeyCode.Escape)) {
		
			Application.Quit ();
		}
		
		if (Input.GetKeyDown (KeyCode.F)) {
			
			// FRENZY
			if (_secondsBetweenShots == _currentProjectilePool.secondsBetweenShots) {

				_secondsBetweenShots = 0.000001f;
			}
			else {

				_secondsBetweenShots = _currentProjectilePool.secondsBetweenShots;
			}
		}
		
	}
	
	void LateUpdate () {

		if (lerpCameraToTarget) {
		
			
			Vector3 p = playerCamera.GetComponent<Transform> ().position;
			
			playerCamera.transform.position = Vector3.Lerp (new Vector3 (p.x, p.y, p.z), 
			                                                new Vector3 (playerCubeTransform.position.x, p.y, playerCubeTransform.position.z + cameraDistanceInZ),
			                                                cameraLerpSpeed * Time.deltaTime);
		}
	}

	public void UpdateProjectilePool () {
		
		_currentProjectilePool = GetComponent<PlayerController> ().currentProjectilePool;
		_secondsBetweenShots = _currentProjectilePool.secondsBetweenShots;
		shootingPointAudio.clip = _currentProjectilePool.soundOnShoot;
	}
}
