﻿using UnityEngine;
using System.Collections;

/*
	HOW TO:

	Make your camera free in the scene, with no parent.
	Attach this component to it.
	Place an empty GameObject, just a Transform, as a child of your player where the camera should be.
	Set the target Transform to that Transform.
	Play with the Lerp Speed to see which one fits you.

 */

public class LerpToTarget : MonoBehaviour {
	
	private Transform _thisTransform;

	[Tooltip("The Transform to follow.")]
	public Transform target;

	[Tooltip("How fast this Transform moves towards the target. Zero means no movement.")]
	public float lerpSpeed = 1.0f;


	void Start () {
	
		_thisTransform = GetComponent<Transform> ();
	}

	void LateUpdate () {

		// this smoothly interpolates a position between the current position and the target to follow
		_thisTransform.position = Vector3.Lerp (_thisTransform.position, target.position, lerpSpeed * Time.deltaTime);
	}
}
