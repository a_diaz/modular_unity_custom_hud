﻿using UnityEngine;
using System.Collections;

public class EnforcerEnemyMovement : EnemyMovement {

	private EnforcerEnemyPool _pool;
	
	public override EnemyPool pool {
		
		get { return _pool; }
	}
	
	public EnforcerEnemyPool enforcerPool {
		
		get { return _pool; }
	}

	void Start () {

		targetDirection.SetPosY (_target.position.y);
		currentMovementSpeed = maximumMovementSpeed = baseMaximumMovementSpeed;
	}

	void Update () {

		if (canMove) {
			
			// by now move forward

			if (Vector3.Distance (_target.position, GetComponent<Transform> ().position) >= 1.8f)
			
				enemyBody.Translate (Vector3.forward * currentMovementSpeed * Time.deltaTime);

		
		}

		targetDirection.LookAt (_target);
	}

	void LateUpdate () {
		
		//if (canMove) 
			enemyBody.rotation = targetDirection.rotation; 
			//Vector3.Lerp (enemyBody.eulerAngles, targetDirection.eulerAngles,  Time.deltaTime);
	}

	public override void SetPool (EnemyPool originalPool) {
		
		_pool = (EnforcerEnemyPool) originalPool;
	}
}
