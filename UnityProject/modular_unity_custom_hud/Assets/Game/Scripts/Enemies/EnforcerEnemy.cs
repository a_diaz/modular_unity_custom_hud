﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(EnforcerEnemyMovement))]
public class EnforcerEnemy : Enemy {

	public AudioSource audioSource;
	public GameObject insideCube;
	public GameObject cylinderGroup;
	public GameObject positionQuad;

	public NotifyPlayerEnterArea areaDetection;

	private EnforcerEnemyPool _pool;
	
	public override EnemyPool pool {
		
		get { return _pool; }
	}
	
	public EnforcerEnemyPool enforcerPool {
		
		get { return _pool; }
	}

	public EnforcerEnemyMovement movement {
		
		get { return GetComponent<EnforcerEnemyMovement> (); }
	}

	public AudioClip soundOnBurst {

		get { return _pool.soundOnBurst; } 
	}

	public bool canBurst;
	public float baseSecondsToAreaBurst {

		get { return _pool.baseSecondsToAreaBurst; }
	}

	public int baseBurstDamage {

		get { return _pool.baseBurstDamage; }
	}
	public float baseBurstForce {

		get { return _pool.baseBurstForce; }
	}

	private PlayerController _tempPlayerEntered;
	private float _tempBurstRadius;

	public ParticleSystem circularParticles;
	public ParticleSystem hitParticles;
	public ParticleSystem deathParticles;
	public ParticleSystem burstParticles;

	public Color circularParticlesRegularColor;
	public Color circularParticlesAlteredColor;

	void Awake () {
	
		_enemyType = EnemyTypes.Enforcer;
	}

	// Use this for initialization
	void Start () {
	
		GetComponentInChildren<NotifyPlayerEnterArea> ().SetCallback (OnPlayerEnteredArea);
	}

	public override void OnTriggerEnter (Collider other) {
	
		if (other.tag == "Player") {
		
			SceneController.Instance.EnemyCollisionPlayer (this, other.GetComponent<PlayerController> ());
		}
	}
	
	protected override void SetPool (EnemyPool originalPool) {
		
		_pool = (EnforcerEnemyPool) originalPool;
		movement.SetPool (originalPool);
	}

	public void StopMoving () {
		
		movement.canMove = false;
	}

	public void OnPlayerEnteredArea (PlayerController player, float burstRadius) {
	
		if (canBurst) {
		
			
			_tempPlayerEntered = player;
			_tempBurstRadius = burstRadius;
			
			AlterCircularParticlesOnPlayerEnter ();

			canBurst = false;
			Invoke ("AreaBurst", baseSecondsToAreaBurst);
		}
	}

	public void AreaBurst () {
	

		Vector3 lineDistance =  new Vector3 (this.GetComponent<Transform> ().position.x,
		                                     _tempPlayerEntered.GetComponent<Transform> ().position.y, 
		                                     this.GetComponent<Transform> ().position.z);

		lineDistance = _tempPlayerEntered.GetComponent<Transform> ().position - lineDistance;

		burstParticles.Play ();
		audioSource.PlayOneShot (soundOnBurst);

		ResetCircularParticles ();

		if (lineDistance.magnitude <= _tempBurstRadius) {
		
			// hit on burst
			_tempPlayerEntered.GetComponent<Rigidbody> ().AddExplosionForce (baseBurstForce, this.GetComponent <Transform> ().position, _tempBurstRadius);
			SceneController.Instance.EnforcerBurstPlayer (this, _tempPlayerEntered);

		}
	
		canBurst = true;
	}

	private void ResetCircularParticles () {

		circularParticles.startColor = circularParticlesRegularColor;
		circularParticles.gravityModifier = 0.0f;
	}

	private void AlterCircularParticlesOnPlayerEnter () {
	
		circularParticles.startColor = circularParticlesAlteredColor;
		circularParticles.gravityModifier = -2.0f;
	}
	
	public override void EffectOnReady () {
		
		GetComponent<Animation> ().Play ("Enforcer Enemy Start");

		ResetCircularParticles ();
		GetComponent<Collider> ().enabled = false;
		movement.canMove = false;
		canBurst = false;
		areaDetection.GetComponent<Collider> () .enabled = false;

		SetAllSpecificColors (_pool.startEmissiveColor);
	}

	public override void EffectOnDamage () {
		
		float percent = ((float)remainingLife) / startingLife;
		
		
		Color c = new Color ((percent * _pool.startEmissiveColor.r + (1.0f - percent) * _pool.finalEmissiveColor.r),
		                     (percent * _pool.startEmissiveColor.g + (1.0f - percent) * _pool.finalEmissiveColor.g),
		                     (percent * _pool.startEmissiveColor.b + (1.0f - percent) * _pool.finalEmissiveColor.b),
		                     1.0f);

		hitParticles.Play ();

		SetAllSpecificColors (c);
		
	}
	
	public override void EffectOnDeath () {
		
		movement.canMove = false;
		canBurst = false;
		areaDetection.GetComponent<Collider> () .enabled = false;
		
		GetComponent<Collider> ().enabled = false;
		deathParticles.Play ();
		CancelInvoke ("AreaBurst");
		//Invoke ("RecycleMe", 2.0f);
		hitParticles.Stop ();
		//		GetComponent<Animation> ().Rewind ();
		GetComponent<Animation> ().Play ("Enforcer Enemy Death");
		SetAllSpecificColors (_pool.finalEmissiveColor);
		
	}

	public void OnStartAnimationFinished () {

		movement.canMove = true;
		GetComponent<Collider> ().enabled = true;
		canBurst = true;
		areaDetection.GetComponent<Collider> () .enabled = true;
	}

	public void OnDeathAnimationFinished () {
	
		RecycleMe ();
	}

	private void SetAllSpecificColors (Color newColor) {



		insideCube.GetComponent<MeshRenderer> ().material.SetColor("_EmissionColor", newColor);
		
		foreach (Transform t in cylinderGroup.transform) {
			
			t.GetComponent<MeshRenderer> ().material.SetColor("_EmissionColor", newColor);
		}
		
		positionQuad.GetComponent<MeshRenderer> ().material.SetColor("_EmissionColor", newColor);
	}
}
