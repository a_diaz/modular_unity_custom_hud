﻿using UnityEngine;
using System.Collections;

public abstract class Enemy : MonoBehaviour {

	public enum EnemyTypes {

		Basic = 0,
		Enforcer = 1
	}

	public abstract EnemyPool pool {

		get;
	}

	protected EnemyTypes _enemyType;

	public EnemyTypes enemyType {

		get { return _enemyType; }
	}

	public int startingLife {

		get { return pool.startingLife; }
	}

	private int _remainingLife;

	public int remainingLife {

		get { return _remainingLife; } 
	}

	public int damageOnCollision {

		get { return pool.damageOnCollision; }
	}

	public void Ready (Transform coordinates, EnemyPool originalPool) {

		SetPool (originalPool);
		
		_remainingLife = startingLife;

		EffectOnReady ();

		float originalY = GetComponent<Transform> ().position.y;

		GetComponent<Transform> ().position = coordinates.position;
		GetComponent<Transform> ().SetPosY (originalY);
		GetComponent<Transform> ().rotation = coordinates.rotation;
		
	}

	protected abstract void SetPool (EnemyPool pool);

	public abstract void OnTriggerEnter (Collider other);
	
	public void Spawn () {

		gameObject.SetActive (true);
	}
	
	public void Stop () {
		
		gameObject.SetActive (false);
	}
	
	public void RecycleMe () {
		
		if (GetComponent<CallbackOnEnemyRecycled> () 
		    && GetComponent<CallbackOnEnemyRecycled> ().callback != null) { 

			GetComponent<CallbackOnEnemyRecycled> ().callback (this);
		}

		pool.RecycleEnemy (this);
	}

	public bool TakeDamage (int damage) {

		_remainingLife -= damage;

		bool dead = remainingLife <= 0;

		if (!dead) { EffectOnDamage (); }

		return dead;
	}

	public void Die () {

		EffectOnDeath ();
	}

	public abstract void EffectOnReady ();
	public abstract void EffectOnDamage ();
	public abstract void EffectOnDeath ();
}
