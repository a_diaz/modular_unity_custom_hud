﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(BasicEnemyMovement))]
public class BasicEnemy : Enemy {

	private BasicEnemyPool _pool;

	public override EnemyPool pool {

		get { return _pool; }
	}

	public BasicEnemyPool basicPool {

		get { return _pool; }
	}

	public BasicEnemyMovement movement {

		get { return GetComponent<BasicEnemyMovement> (); }
	}

	public AudioClip soundOnHit {

		get { return _pool.soundOnHit; }
	}

	public AudioClip soundOnDeath {

		get { return _pool.soundOnDeath; }
	}

	public AudioSource audioSource;
	public ParticleSystem hitParticles;
	public ParticleSystem deathParticles;

	public bool randomizeStats {

		get { return _pool.randomizeStats; }
	}

	public float statRandomizerPercent {

		get { return _pool.statRandomizerPercent; }
	}

	void Awake () {

		_enemyType = EnemyTypes.Basic;
	}

	void Start () {

		if (movement._target == null) {
		
			movement._target = SceneController.Instance.playerTransform;
		} 
	}

	protected override void SetPool (EnemyPool originalPool) {
	
		_pool = (BasicEnemyPool) originalPool;
		movement.SetPool (originalPool);
	}

	public override void OnTriggerEnter (Collider other) {

		if (other.tag == "Player") {
		
			SceneController.Instance.EnemyCollisionPlayer (this, other.GetComponent<PlayerController> ());
		}
	}

	public void EnableMovement () {
	
		movement.canMove = true;
	}

	public override void EffectOnReady () {
	
		GetComponent<Animation> ().Play ("Basic Enemy Start");

		GetComponent<Collider> ().enabled = true;

		if (randomizeStats) {
		
			RandomizeStats ();
		}
		movement.canMove = true;
		GetComponent<BasicEnemyMovement> ().insideCube.GetComponent<MeshRenderer> ().material.SetColor("_Color", _pool.startColor);
	}

	public override void EffectOnDamage () {


		movement.canMove = false;
		CancelInvoke ("EnableMovement");

		audioSource.PlayOneShot (soundOnHit);

		if (remainingLife > 0) { 

			Invoke ("EnableMovement", 0.3f); 
		}

		hitParticles.Play ();

		float percent = ((float)remainingLife) / startingLife;

		Color c = new Color ((percent * _pool.startColor.r + (1.0f - percent) * _pool.finalColor.r),
		                     (percent * _pool.startColor.g + (1.0f - percent) * _pool.finalColor.g),
		                     (percent * _pool.startColor.b + (1.0f - percent) * _pool.finalColor.b),
		                     1.0f);

		GetComponent<BasicEnemyMovement> ().insideCube.GetComponent<MeshRenderer> ().material.SetColor("_Color", c);
	}

	public override void EffectOnDeath () {

		movement.canMove = false;
		CancelInvoke ("EnableMovement");
		deathParticles.Play ();

		audioSource.PlayOneShot (soundOnDeath);
		//Invoke ("RecycleMe", 0.4f);
		GetComponent<Collider> ().enabled = false;
//		GetComponent<Animation> ().Rewind ();
		GetComponent<Animation> ().Play ("Basic Enemy Death");
		GetComponent<BasicEnemyMovement> ().insideCube.GetComponent<MeshRenderer> ().material.SetColor("_Color", _pool.finalColor);
	}

	public void OnDeathAnimationFinished () {
	
		RecycleMe ();
	}

	public void StopMoving () {
	
		movement.canMove = false;
	}

	public void RandomizeStats () {
	
		float random = UnityEngine.Random.Range (1.0f - statRandomizerPercent, 1.0f + statRandomizerPercent);
		movement.maximumMovementSpeed = movement.baseMaximumMovementSpeed * random;

		random = UnityEngine.Random.Range (1.0f - statRandomizerPercent, 1.0f + statRandomizerPercent);
		movement.accelerationFactor = movement.baseAccelerationFactor * random;

		random = UnityEngine.Random.Range (1.0f -  statRandomizerPercent, 1.0f + statRandomizerPercent);
		movement.turnToTargetSpeed =  movement.baseTurnToTargetSpeed * random;
	}

}
