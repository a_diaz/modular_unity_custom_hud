﻿using UnityEngine;
using System.Collections;

public abstract class EnemyMovement : MonoBehaviour {

	public abstract EnemyPool pool {

		get;
	}

	public Transform enemyBody;
	public Transform targetDirection;
	
	public Transform _target;

	public bool canMove;
	
	public float baseMaximumMovementSpeed {

		get { return pool.baseMaximumMovementSpeed; }
	}
	
	public float maximumMovementSpeed;
	public float currentMovementSpeed;

	
	void Start () {
		
		targetDirection.LookAt (_target);
	}

	public abstract void SetPool (EnemyPool originalPool);
}
