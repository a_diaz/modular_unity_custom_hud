﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class NotifyPlayerEnterArea : MonoBehaviour {

	public Action<PlayerController, float> CallbackOnPlayerEnterArea;

	public void SetCallback (Action<PlayerController, float> callback) {
	
		CallbackOnPlayerEnterArea = callback;
	}

	void OnTriggerEnter (Collider other) {

		if (other.tag == "Player") {
		
			CallbackOnPlayerEnterArea (other.GetComponent<PlayerController> (), this.GetComponent<CapsuleCollider> ().radius * this.GetComponent<Transform> ().lossyScale.z);
		}
	}
}
