﻿using UnityEngine;
using System.Collections;

public class BasicEnemyMovement : EnemyMovement {

	public Transform insideCube;
//	public Transform insideCylinder;

	private BasicEnemyPool _pool;

	public override EnemyPool pool {
		
		get { return _pool; }
	}
	
	public BasicEnemyPool basicPool {
		
		get { return _pool; }
	}

	public bool spinAround;

	public float baseAccelerationFactor {

		get { return _pool.baseAccelerationFactor; }
	}

	public float baseTurnToTargetSpeed {

		get { return _pool.baseTurnToTargetSpeed; }
	}
	
	public float spinAroundSpeed;
	public float turnToTargetSpeed;

	public float accelerationFactor;
	
	void Update () {
	
		if (spinAround) {
		
			insideCube.Rotate (Vector3.up, spinAroundSpeed * Time.deltaTime, Space.World);
		}

		if (canMove) {
		

			if (currentMovementSpeed < maximumMovementSpeed) {
			
				currentMovementSpeed = Mathf.Clamp (currentMovementSpeed + accelerationFactor, 0.0f, maximumMovementSpeed);
			}
			else {

				currentMovementSpeed = maximumMovementSpeed;
			}

			// by now move forward
			enemyBody.Translate (0.0f, 0.0f, currentMovementSpeed  * Time.deltaTime);
		}
		else {

			// reset the speed
			currentMovementSpeed = 0.0f;
		}

		// update the target
		targetDirection.LookAt (_target);

	}

	void LateUpdate () {

		enemyBody.rotation = Quaternion.Slerp (enemyBody.rotation, targetDirection.rotation, turnToTargetSpeed * Time.deltaTime);
	}

	public override void SetPool (EnemyPool originalPool) {

		_pool = (BasicEnemyPool) originalPool;
	}
}
