using UnityEngine;
using System.Collections;
using CustomHUD;

public class DummyShowAndHide : MonoBehaviour {

	public HUDElement obj;

	public float timeToHide;

	void Start () {

		Invoke ("DoIt", 1.0f);
	}

	void DoIt () {

		((IHUDShowable)obj).ShowAndHide (timeToHide);
	}
}
