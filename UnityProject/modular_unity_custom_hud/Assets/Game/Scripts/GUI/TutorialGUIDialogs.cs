﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TutorialGUIDialogs : MonoBehaviour {

	public Canvas baseCanvas;

	public float sentenceDuration;


	public void ShowSentence (string sentence, float duration) {

	
		CustomHUD.HUD.ShowAndHidePrimarySubtitle (sentence, duration);
	}
}
