﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenuController : MonoBehaviour {

	private static MainMenuController _Instance;

	public static MainMenuController Instance {

		get { return _Instance; }
	}

	public Button playButton;
	public Button tutorialButton;
	public Button optionsButton;
	public Button quitButton;

	public Text creditsText;

	[Range (0.0f, 1.0f)]
	public float fadeInTime;

	[Range (0.0f, 5.0f)]
	public float fadeOutTime;

	void Awake () {

		_Instance = this;
	}

	public void OnPlayButtonClicked () {

		FadeOutButtons ();
		SceneController.Instance.StartPlaying ();
	}

	public void OnTutorialButtonClicked () {

		SceneController.Instance.TESTINGPlayTutorial ();
	}

	public void OnOptionsButtonClicked () {


	}

	public void OnQuitButtonClicked () {

		SceneController.Instance.TESTINGExit ();
	}

	public void ShowButtons () {

		playButton		.gameObject.SetActive (true);
		tutorialButton	.gameObject.SetActive (true);
		optionsButton	.gameObject.SetActive (true);
		quitButton		.gameObject.SetActive (true);
		creditsText		.gameObject.SetActive (true);


		playButton		.GetComponent<Text> ().canvasRenderer.SetAlpha (0.0f);
		tutorialButton	.GetComponent<Text> ().canvasRenderer.SetAlpha (0.0f);
		optionsButton	.GetComponent<Text> ().canvasRenderer.SetAlpha (0.0f);
		quitButton		.GetComponent<Text> ().canvasRenderer.SetAlpha (0.0f);
		creditsText		.canvasRenderer.SetAlpha (0.0f);

		playButton		.GetComponent<Text> ().CrossFadeAlpha (1.0f, fadeInTime, true);
		tutorialButton	.GetComponent<Text> ().CrossFadeAlpha (1.0f, fadeInTime, true);
		optionsButton	.GetComponent<Text> ().CrossFadeAlpha (1.0f, fadeInTime, true);
		quitButton		.GetComponent<Text> ().CrossFadeAlpha (1.0f, fadeInTime, true);
		creditsText		.CrossFadeAlpha (1.0f, fadeInTime, true);

		playButton		.interactable = true;
		tutorialButton	.interactable = true;
		optionsButton	.interactable = true;
		quitButton		.interactable = true;
	}

	public void HideButtons () {

		playButton		.gameObject.SetActive (false);
		tutorialButton	.gameObject.SetActive (false);
		optionsButton	.gameObject.SetActive (false);
		quitButton		.gameObject.SetActive (false);
		creditsText		.gameObject.SetActive (false);

		playButton		.interactable = false;
		tutorialButton	.interactable = false;
		optionsButton	.interactable = false;
		quitButton		.interactable = false;
	}

	private void FadeOutButtons () {

		playButton		.GetComponent<Text> ().canvasRenderer.SetAlpha (1.0f);
		tutorialButton	.GetComponent<Text> ().canvasRenderer.SetAlpha (1.0f);
		optionsButton	.GetComponent<Text> ().canvasRenderer.SetAlpha (1.0f);
		quitButton		.GetComponent<Text> ().canvasRenderer.SetAlpha (1.0f);
		creditsText		.canvasRenderer.SetAlpha (1.0f);

		playButton		.GetComponent<Text> ().CrossFadeAlpha (0.0f, fadeOutTime, true);
		tutorialButton	.GetComponent<Text> ().CrossFadeAlpha (0.0f, fadeOutTime, true);
		optionsButton	.GetComponent<Text> ().CrossFadeAlpha (0.0f, fadeOutTime, true);
		quitButton		.GetComponent<Text> ().CrossFadeAlpha (0.0f, fadeOutTime, true);
		creditsText		.CrossFadeAlpha (0.0f, fadeOutTime, true);

		playButton		.interactable = false;
		tutorialButton	.interactable = false;
		optionsButton	.interactable = false;
		quitButton		.interactable = false;
	}

	private void DisableButtons () {


	}


}
